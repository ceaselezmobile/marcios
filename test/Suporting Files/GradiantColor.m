//
//  GradiantColor.m
//  test
//
//  Created by ceaselez on 02/02/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "GradiantColor.h"

@implementation GradiantColor


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Create the colors
    UIColor *topColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1.0];
    UIColor *bottomColor = [UIColor colorWithRed:56.0/255.0 green:56.0/255.0 blue:56.0/255.0 alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *theViewGradient = [CAGradientLayer layer];
    theViewGradient.colors = [NSArray arrayWithObjects: (id)topColor.CGColor, (id)bottomColor.CGColor, nil];
    theViewGradient.frame = self.bounds;
    
    //Add gradient to view
    [self.layer insertSublayer:theViewGradient atIndex:0];
    
}


@end
