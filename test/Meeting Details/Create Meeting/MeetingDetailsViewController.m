
#import "MeetingDetailsViewController.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "MeetingDetailsTableViewCell.h"
#import "THDatePickerViewController.h"
#import "SearchViewController.h"
#import "MySharedManager.h"

@interface MeetingDetailsViewController ()<UITextFieldDelegate,UITextViewDelegate,THDatePickerDelegate>

@end

@implementation MeetingDetailsViewController
{
    __weak IBOutlet UILabel *tittleLabel;
    __weak IBOutlet NSLayoutConstraint *memberTableViewHeight;
    __weak IBOutlet NSLayoutConstraint *agendeTableViewHeight;
    __weak IBOutlet UITableView *agendaTableView;
    __weak IBOutlet UITableView *membersPopoverTableView;
    __weak IBOutlet UIView *popOverView;
    __weak IBOutlet UITableView *dataTableView;
    __weak IBOutlet UIView *detailsBtnView;
    __weak IBOutlet UIView *membersBtnView;
    __weak IBOutlet UIView *agendaBtnView;
    __weak IBOutlet UIButton *submitBtn;
    
    __weak IBOutlet UIButton *addBtn;
    int meetingDetails;
    __weak IBOutlet UIButton *menuBtn;
    BOOL submitButtonClicked;
    THDatePickerViewController * datePicker;
    NSDate * curDate;
    NSDate *startTime;
    NSDate *endTime;
    NSDateFormatter * formatter;
    UIDatePicker *startTimePicker;
    UIDatePicker *endTimePicker;
    NSInteger timeDifference;
    BOOL endTimeSelected;
    NSString *textViewText;
    MySharedManager *sharedManager;
    NSString *StakeholderCategory;
    NSString *Company;
    NSString *AssignedTo;
    NSString *AssignedToEmail;
    NSString *Role;
    NSString *textViewTextAgenda;
    NSMutableArray *membersArray;
    NSMutableArray *agendaArray;
    NSMutableArray *membersDeletedArray;
    NSMutableArray *agendaDeletedArray;
    NSString *catogeryString;
    NSString *stakeHstring;
    NSString *comanyString;
    NSString *memberString;
    NSString *roleString;
    NSInteger oldDataIndex;
    BOOL companyIDBool;
    BOOL categoryIDBool;
    NSInteger sum;
    NSString *timeZone;
    BOOL roleNeed;
    BOOL olddata;
    NSString *modeString;
    NSString *roomString;
    NSDateFormatter *timeFormat;
    BOOL updatemember;
    NSString *MeetingTypeID;
    NSDate *MeetingDateOld;
    NSString *mainCategoryString;
    NSString *mainCategoryID;
    NSString *groupCategoryString;
    NSString *groupCategoryID;
    NSString *approveByString;
    NSString *approveByID;
    NSString *typeString;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    sharedManager = [MySharedManager sharedManager];
    dataTableView.rowHeight = UITableViewAutomaticDimension;
    agendaTableView.rowHeight = UITableViewAutomaticDimension;
    membersArray = [[NSMutableArray alloc] init];
    agendaArray = [[NSMutableArray alloc] init];
    membersDeletedArray = [[NSMutableArray alloc] init];
    agendaDeletedArray = [[NSMutableArray alloc] init];
    
    textViewText = @"";
    textViewTextAgenda = @"";
    curDate = nil;
    startTime = nil;
    endTime = nil;
    roomString = nil;
    modeString = nil;
    mainCategoryString= @"";
    groupCategoryString= @"";
    typeString = @"";
    approveByString = @"";
    tittleLabel.text = @"Meeting Details";
    
    membersBtnView.hidden = YES;
    agendaBtnView.hidden = YES;
    popOverView.hidden = YES;
    membersPopoverTableView.hidden = YES;
    agendaTableView.hidden = YES;
    detailsBtnView.hidden = NO;
    meetingDetails = 1;
    timeDifference = 0;
    sum = 0;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    timeFormat= [[NSDateFormatter alloc]init];
    [timeFormat setDateFormat:@"hh:mm a"];
    startTimePicker=[[UIDatePicker alloc]init];
    startTimePicker.datePickerMode=UIDatePickerModeTime;
    startTimePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    endTimePicker=[[UIDatePicker alloc]init];
    endTimePicker.datePickerMode=UIDatePickerModeTime;
    endTimePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [dataTableView addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self  action:@selector(didSwipe:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [dataTableView addGestureRecognizer:swipeRight];
    if(_update)
    {
        [self updateData];
        [menuBtn setImage:[UIImage imageNamed:@"BackBtn.png"] forState:UIControlStateNormal];
        [menuBtn addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
        [submitBtn setTitle:@"Update" forState:UIControlStateNormal];
    }
    else
    {
        [self getData];
        NSDictionary *dict = @{ @"name" : [NSString stringWithFormat:@"%@",[defaults objectForKey:@"EmployeeName"]], @"role" : @"Secretary",@"nameID" : [defaults objectForKey:@"UserID"],@"roleID" : @"3"};
        [membersArray addObject:dict];
    }
}
-(void)backBtn
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void) viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self fillTheTF];
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        [self viewDidLoad];
    }
    [dataTableView reloadData];
}
-(void)viewDidAppear:(BOOL)animated
{
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"])
    {
        sharedManager.slideMenuSlected = @"no";
    }
}
- (void)didSwipe:(UISwipeGestureRecognizer*)swipe
{
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        if (meetingDetails == 1) {
            [self membersClicked];
        }
        else if (meetingDetails == 2) {
            [self AgendaClicked];
        }
        
    }
    else if (swipe.direction == UISwipeGestureRecognizerDirectionRight)
    {
        if (meetingDetails == 3)
        {
            [self membersClicked];
        }
        else if (meetingDetails == 2)
        {
            [self DetailsClicked];
        }
        
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == dataTableView) {
        if(meetingDetails == 1){
            return 11;
        }
        else if(meetingDetails == 2){
            return membersArray.count;
        }
        else if(meetingDetails == 3){
            if (agendaArray.count == 0) {
                return  1;
            }
            return agendaArray.count;
        }
    }
    else if (tableView == membersPopoverTableView)
    {
        if (updatemember && [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"update"])
        {
            memberTableViewHeight.constant = 3*60;
            return 3;
        }
        else if (roleNeed)
        {
            if([stakeHstring isEqualToString:@"Internal"]||[stakeHstring isEqualToString:@""])
            {
                memberTableViewHeight.constant = 4*60;
                return 4;
            }
            else
                memberTableViewHeight.constant = 6*60;
            return 6;
        }
        else
        {
            if([stakeHstring isEqualToString:@"Internal"]||[stakeHstring isEqualToString:@""])
            {
                memberTableViewHeight.constant = 3*60;
                return 3;
            }
            else
                memberTableViewHeight.constant = 5*60;
            return 5;
        }
    }
    else if (tableView == agendaTableView)
    {
        return 3;
    }
    return 0;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MeetingDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Header"];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == dataTableView) {
        if (meetingDetails == 2 || meetingDetails == 3) {
            addBtn.hidden = NO;
            return 0;
        }
    }
    addBtn.hidden = YES;
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"cell";
    
    MeetingDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (tableView == dataTableView) {
        
        if (meetingDetails == 1) {
            if (indexPath.row == 0) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Meeting Description" forIndexPath:indexPath];
                cell.meetingDescriptionTV.layer.sublayerTransform = CATransform3DMakeTranslation(0.0f, 0.0f, 0.0f);
                if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
                    textViewText = @"";
                }
                else{
                    if (_update) {
                        cell.meetingDescriptionTV.editable = NO;
                    }
                    [cell.meetingDescriptionTV setText:textViewText];
                }
                if (submitButtonClicked) {
                    if ([cell.meetingDescriptionTV.text isEqualToString:@""]) {
                        cell.meetingDescriptionIV.hidden = NO;
                    }
                }
                else
                    cell.meetingDescriptionIV.hidden =YES;
            }
            else if (indexPath.row == 1) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Time Zone" forIndexPath:indexPath];
                cell.timeZoneTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                cell.timeZoneTF.text = timeZone;
                
            }
            else if (indexPath.row == 2) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Date" forIndexPath:indexPath];
                cell.dateTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                if (_update) {
                    cell.dateTF.text = [formatter stringFromDate:curDate];
                }
                if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
                    cell.dateTF.text = @"";
                }
                if (submitButtonClicked) {
                    if ([cell.dateTF.text isEqualToString:@""]) {
                        cell.dateIV.hidden = NO;
                    }
                }
                else
                    cell.dateIV.hidden =YES;
            }
            else if (indexPath.row == 3) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Start Time" forIndexPath:indexPath];
                cell.startTimeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                if (_update) {
                    cell.startTimeTF.text = [timeFormat stringFromDate:startTime];
                }
                UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
                [toolBar setTintColor:[UIColor grayColor]];
                UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(startTimeDone)];
                UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
                [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
                [cell.startTimeTF setInputAccessoryView:toolBar];
                cell.startTimeTF.inputView = startTimePicker;
                
                if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
                    cell.startTimeTF.text = @"";
                    startTime = nil;
                }
                if (submitButtonClicked) {
                    if ([cell.startTimeTF.text isEqualToString:@""]) {
                        cell.startTimeIV.hidden = NO;
                    }
                }
                else
                    cell.startTimeIV.hidden =YES;
            }
            else if (indexPath.row == 4) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"End Time" forIndexPath:indexPath];
                cell.endTimeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                
                cell.endTimeTF.inputView = endTimePicker;
                UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
                [toolBar setTintColor:[UIColor grayColor]];
                UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(endTimeDone)];
                UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
                [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
                [cell.endTimeTF setInputAccessoryView:toolBar];
                if (_update) {
                    cell.endTimeTF.text = [timeFormat stringFromDate:endTime];
                }
                if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
                    cell.endTimeTF.text = @"";
                    endTime = nil;
                }
                if (submitButtonClicked) {
                    if ([cell.endTimeTF.text isEqualToString:@""]) {
                        cell.endTimeIV.hidden = NO;
                    }
                }
                else
                    cell.endTimeIV.hidden =YES;
            }
            else if (indexPath.row == 5) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Category" forIndexPath:indexPath];
                cell.mainCategoryTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                cell.mainCategoryTF.text = mainCategoryString;
                if(_update){
                    cell.userInteractionEnabled = NO;
                }
                
            }
            else if (indexPath.row == 6) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"groupCategory" forIndexPath:indexPath];
                cell.groupCategoryTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                cell.groupCategoryTF.text = groupCategoryString;
                if(_update){
                    cell.userInteractionEnabled = NO;
                }
                
            }
            else if (indexPath.row == 7) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Meeting Room" forIndexPath:indexPath];
                cell.meetingRoomTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                if (_update) {
                    cell.meetingRoomTF.text = roomString;
                }
                if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
                    cell.meetingRoomTF.text = @"";
                }
                if (submitButtonClicked) {
                    if ([cell.meetingRoomTF.text isEqualToString:@""]) {
                        cell.meetingRoomIV.hidden = NO;
                    }
                }
                else
                    cell.meetingRoomIV.hidden =YES;
            }
            else if (indexPath.row == 8) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Meeting Mode" forIndexPath:indexPath];
                cell.meetingModeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                if (_update) {
                    cell.meetingModeTF.text = modeString;
                }
                if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
                    cell.meetingModeTF.text = @"";
                }
                if (submitButtonClicked) {
                    if ([cell.meetingModeTF.text isEqualToString:@""]) {
                        cell.meetingModeIV.hidden = NO;
                    }
                }
                else
                    cell.meetingModeIV.hidden =YES;
                
            }
            else if (indexPath.row == 9) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Type" forIndexPath:indexPath];
                cell.typeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                //                if (_update) {
                cell.typeTF.text = typeString;
                //                }
                if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
                    cell.typeTF.text = @"";
                }
            }
            else if (indexPath.row == 10) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Approve By" forIndexPath:indexPath];
                cell.approveByTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                //                if (_update) {
                cell.approveByTF.text = approveByString;
                //                }
                if ([typeString isEqualToString:@"NonCompliance"]) {
                    cell.approvalBtn.enabled = NO;
                }
                else{
                    cell.approvalBtn.enabled = YES;
                }
                if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
                    cell.approveByTF.text = @"";
                    sharedManager.slideMenuSlected = @"no";
                }
            }
        }
        else if(meetingDetails == 2)
        {
            if ( indexPath.row != 0) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"updatemembersCell" forIndexPath:indexPath];
            }
            else{
                cell = [tableView dequeueReusableCellWithIdentifier:@"membersCell" forIndexPath:indexPath];
            }
            
            cell.membersNameLabel.text = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"name"];
            cell.membersRoleLabel.text = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"role"];
        }
        else if(meetingDetails == 3){
            if (agendaArray.count == 0) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"no data" forIndexPath:indexPath];
            }
            
            else
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"updateagendaCell" forIndexPath:indexPath];
                cell.agendaDescriptionLabel.text = [[agendaArray objectAtIndex:indexPath.row] objectForKey:@"descripition"];
                cell.agendaRoleLabel.text =[NSString stringWithFormat:@"%@ (Mins)",[[agendaArray objectAtIndex:indexPath.row] objectForKey:@"duration"]];
            }
        }
    }
    if (tableView == membersPopoverTableView)
    {
        if (updatemember && [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"update"]) {
            if (indexPath.row == 0) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Members" forIndexPath:indexPath];
                cell.memberTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                cell.userInteractionEnabled = NO;
                cell.memberTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"name"];
            }
            if (indexPath.row == 1) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Role" forIndexPath:indexPath];
                cell.roleTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                if (olddata)
                {
                    cell.roleTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"role"];
                }
                else
                {
                    cell.roleTF.text = roleString;
                }
            }
            if (indexPath.row == 2)
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Buttons" forIndexPath:indexPath];
                if (oldDataIndex == 5000)
                {
                    [cell.memberDoneBtn setTitle:@"Done" forState:UIControlStateNormal];
                    cell.deleteMemberBtn.hidden = YES;
                }
                else
                {
                    [cell.memberDoneBtn setTitle:@"Update" forState:UIControlStateNormal];
                    cell.deleteMemberBtn.hidden = YES;
                }
            }
        }
        else if (roleNeed)
        {
            if (indexPath.row == 0)
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"StakeHolder" forIndexPath:indexPath];
                cell.StakeHTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                if (olddata)
                {
                    //                    cell.StakeHTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"category"];
                }
                else
                {
                    cell.StakeHTF.text = stakeHstring;
                }
            }
            if([stakeHstring isEqualToString:@"Internal"]||[stakeHstring isEqualToString:@""])
            {
                if (indexPath.row == 1)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Members" forIndexPath:indexPath];
                    cell.memberTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.userInteractionEnabled = YES;
                    
                    if (olddata)
                    {
                        cell.memberTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"name"];
                    }
                    else
                    {
                        cell.memberTF.text = memberString;
                    }
                }
                if (indexPath.row == 2)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Role" forIndexPath:indexPath];
                    cell.roleTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    if (olddata)
                    {
                        cell.roleTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"role"];
                    }
                    else
                    {
                        cell.roleTF.text = roleString;
                    }
                }
                if (indexPath.row == 3)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Buttons" forIndexPath:indexPath];
                    if (oldDataIndex == 5000)
                    {
                        [cell.memberDoneBtn setTitle:@"Done" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                    else
                    {
                        [cell.memberDoneBtn setTitle:@"Update" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                }
            }
            else
            {
                if (indexPath.row == 1)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Category" forIndexPath:indexPath];
                    cell.categoryTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    if (olddata) {
                        cell.categoryTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"category"];
                    }
                    else
                    {
                        cell.categoryTF.text = catogeryString;
                    }
                }
                if (indexPath.row == 2)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Company" forIndexPath:indexPath];
                    cell.companyTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    if (olddata)
                    {
                        cell.companyTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"company"];
                    }
                    else
                    {
                        cell.companyTF.text = comanyString;
                    }
                }
                if (indexPath.row == 3)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Members" forIndexPath:indexPath];
                    cell.memberTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.userInteractionEnabled = YES;
                    
                    if (olddata)
                    {
                        cell.memberTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"name"];
                    }
                    else
                    {
                        cell.memberTF.text = memberString;
                    }
                }
                if (indexPath.row == 4)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Role" forIndexPath:indexPath];
                    cell.roleTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    if (olddata)
                    {
                        cell.roleTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"role"];
                    }
                    else
                    {
                        cell.roleTF.text = roleString;
                    }
                }
                if (indexPath.row == 5)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Buttons" forIndexPath:indexPath];
                    if (oldDataIndex == 5000)
                    {
                        [cell.memberDoneBtn setTitle:@"Done" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                    else
                    {
                        [cell.memberDoneBtn setTitle:@"Update" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                }
            }
        }
        else
        {
            if (indexPath.row == 0)
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"StakeHolder" forIndexPath:indexPath];
                cell.StakeHTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                if (olddata)
                {
                    //                    cell.StakeHTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"category"];
                }
                else
                {
                    cell.StakeHTF.text = stakeHstring;
                }
            }
            if([stakeHstring isEqualToString:@"Internal"]||[stakeHstring isEqualToString:@""])
            {
                if (indexPath.row == 1)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Members" forIndexPath:indexPath];
                    cell.memberTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.userInteractionEnabled = YES;
                    
                    if (olddata) {
                        cell.memberTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"name"];
                    }
                    
                    else{
                        cell.memberTF.text = memberString;
                    }
                }
                if (indexPath.row == 2)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Buttons" forIndexPath:indexPath];
                    if (oldDataIndex == 5000) {
                        [cell.memberDoneBtn setTitle:@"Done" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                    else {
                        [cell.memberDoneBtn setTitle:@"Update" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                }
            }
            else
            {
                if (indexPath.row == 1)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Category" forIndexPath:indexPath];
                    cell.categoryTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    
                    if (olddata) {
                        cell.categoryTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"category"];
                    }
                    else{
                        cell.categoryTF.text = catogeryString;
                    }
                }
                if (indexPath.row == 2)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Company" forIndexPath:indexPath];
                    cell.companyTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    
                    
                    if (olddata) {
                        cell.companyTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"company"];
                    }
                    else{
                        cell.companyTF.text = comanyString;
                    }
                }
                if (indexPath.row == 3)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Members" forIndexPath:indexPath];
                    cell.memberTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.userInteractionEnabled = YES;
                    
                    if (olddata) {
                        cell.memberTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"name"];
                    }
                    
                    else{
                        cell.memberTF.text = memberString;
                    }
                }
                if (indexPath.row == 4)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Buttons" forIndexPath:indexPath];
                    if (oldDataIndex == 5000) {
                        [cell.memberDoneBtn setTitle:@"Done" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                    else {
                        [cell.memberDoneBtn setTitle:@"Update" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                }
            }
        }
    }
    if (tableView == agendaTableView)
    {
        if (indexPath.row == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Description" forIndexPath:indexPath];
            cell.descriptionTV.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            
            [cell.descriptionTV setText:textViewTextAgenda];
            if (olddata) {
                cell.descriptionTV.text = [[agendaArray objectAtIndex:oldDataIndex] objectForKey:@"descripition"];
            }
            else{
                cell.descriptionTV.text = @"";
            }
            
        }
        if (indexPath.row == 1) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Duration" forIndexPath:indexPath];
            cell.durationTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            
            cell.timeGapLabel.text = [NSString stringWithFormat:@"Remaining (%ld mins)",timeDifference-(long)sum];
            if (olddata) {
                cell.durationTF.text = [NSString stringWithFormat:@"%@",[[agendaArray objectAtIndex:oldDataIndex] objectForKey:@"duration"]];
            }
            else{
                cell.durationTF.text = @"";
            }
            
        }
        if (indexPath.row == 2) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Button" forIndexPath:indexPath];
            if (oldDataIndex == 5000) {
                [cell.agendaDoneBtn setTitle:@"Done" forState:UIControlStateNormal];
                cell.deleteAgendaBtn.hidden = YES;
            }
            else{
                [cell.agendaDoneBtn setTitle:@"Update" forState:UIControlStateNormal];
                cell.deleteAgendaBtn.hidden = YES;
            }
        }
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == dataTableView) {
        if (meetingDetails == 1)
        {
            if (indexPath.row == 0) {
                return [self heightForText:textViewText withFont:[UIFont systemFontOfSize:14] andWidth:self.view.bounds.size.width-50]+70;
            }
        }
        if (meetingDetails == 2 || meetingDetails == 3) {
            return 100;
        }
        return 80;
    }
    else if (tableView == agendaTableView)
    {
        if (meetingDetails == 3) {
            if (indexPath.row == 0)
            {
                if (180 + [self heightForText:textViewTextAgenda withFont:[UIFont systemFontOfSize:14] andWidth:190] < 350)
                {
                    agendeTableViewHeight.constant = 180 + [self heightForText:textViewTextAgenda withFont:[UIFont systemFontOfSize:14] andWidth:203];
                    return [self heightForText:textViewTextAgenda withFont:[UIFont systemFontOfSize:14] andWidth:203]+50;
                }
                else
                {
                    agendeTableViewHeight.constant = 350;
                    return 240;
                }
            }
        }
        return 60;
    }
    return 60;
}

- (IBAction)controlBtn:(id)sender
{
    [self DetailsClicked];
}
- (IBAction)MembersBtn:(id)sender {
    [self membersClicked];
}
- (IBAction)Agenda:(id)sender {
    [self AgendaClicked];
}
-(void)DetailsClicked{
    detailsBtnView.hidden = NO;
    membersBtnView.hidden = YES;
    agendaBtnView.hidden = YES;
    popOverView.hidden = YES;
    meetingDetails = 1;
    tittleLabel.text = @"Meeting Details";
    [dataTableView reloadData];
    [dataTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    
}
-(void)membersClicked{
    detailsBtnView.hidden = YES;
    membersBtnView.hidden = NO;
    agendaBtnView.hidden = YES;
    popOverView.hidden = YES;
    meetingDetails = 2;
    tittleLabel.text = @"Meeting Members";
    [dataTableView reloadData];
    [dataTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    
}
-(void)AgendaClicked{
    detailsBtnView.hidden = YES;
    membersBtnView.hidden = YES;
    agendaBtnView.hidden = NO;
    popOverView.hidden = YES;
    meetingDetails = 3;
    tittleLabel.text = @"Meeting Agenda";
    [dataTableView reloadData];
    [dataTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    
}
- (IBAction)dateClicked:(id)sender {
    if(!datePicker)
        datePicker = [THDatePickerViewController datePicker];
    datePicker.date = [NSDate date];
    datePicker.delegate = self;
    [datePicker setAllowClearDate:NO];
    [datePicker setClearAsToday:YES];
    [datePicker setAutoCloseOnSelectDate:NO];
    [datePicker setAllowSelectionOfSelectedDate:YES];
    [datePicker setDisableYearSwitch:YES];
    //    [datePicker setDaysInHistorySelection:1];
    //    [datePicker setDaysInFutureSelection:0];
    
    [datePicker setSelectedBackgroundColor:[UIColor colorWithRed:125/255.0 green:208/255.0 blue:0/255.0 alpha:1.0]];
    [datePicker setCurrentDateColor:[UIColor colorWithRed:242/255.0 green:121/255.0 blue:53/255.0 alpha:1.0]];
    [datePicker setCurrentDateColorSelected:[UIColor yellowColor]];
    
    [datePicker setDateHasItemsCallback:^BOOL(NSDate *date) {
        int tmp = (arc4random() % 30)+1;
        return (tmp % 5 == 0);
    }];
    [self presentSemiViewController:datePicker withOptions:@{
                                                             KNSemiModalOptionKeys.pushParentBack    : @(NO),
                                                             KNSemiModalOptionKeys.animationDuration : @(0.2),
                                                             KNSemiModalOptionKeys.shadowOpacity     : @(0.2),
                                                             }];
}
- (void)datePickerDonePressed:(THDatePickerViewController *)datePicker {
    curDate = datePicker.date;
    NSIndexPath *ip ;
    ip = [NSIndexPath indexPathForRow:2 inSection:0];
    MeetingDetailsTableViewCell *cell = [dataTableView cellForRowAtIndexPath:ip];
    
    cell.dateIV.hidden = YES;
    cell.dateTF.text = [formatter stringFromDate:curDate];
    [cell.dateTF resignFirstResponder];
    [self dismissSemiModalView];
    
}


- (void)datePickerCancelPressed:(THDatePickerViewController *)datePicker {
    [self dismissSemiModalView];
}

- (void)datePicker:(THDatePickerViewController *)datePicker selectedDate:(NSDate *)selectedDate {
    NSLog(@"Date selected: %@",[formatter stringFromDate:selectedDate]);
}

- (IBAction)startBtnClicked:(id)sender {
    
    NSIndexPath *ip;
    MeetingDetailsTableViewCell *cell;
    ip = [NSIndexPath indexPathForRow:2 inSection:0];
    cell = [dataTableView cellForRowAtIndexPath:ip];
    
    if ([cell.dateTF.text isEqualToString:@""]) {
        [self showMsgAlert:@"Please select the date"];
    }
    else{
        
        if ([cell.dateTF.text isEqualToString:[formatter stringFromDate:[NSDate date]]]) {
            //            startTimePicker.minimumDate = [NSDate date];
        }
        if (endTime != nil) {
            NSDateFormatter *dateFormatEnd = [[NSDateFormatter alloc] init];
            [dateFormatEnd setDateFormat:@"hh:mm a"];
            NSString *dateString1 = [dateFormatEnd stringFromDate:endTime];
            endTime = [dateFormatEnd dateFromString:dateString1];
            startTimePicker.maximumDate = endTime;
            startTimePicker.date = endTime;
        }
        ip = [NSIndexPath indexPathForRow:3 inSection:0];
        cell = [dataTableView cellForRowAtIndexPath:ip];
        [cell.startTimeTF becomeFirstResponder];
        
    }
    
}
- (IBAction)endBtnClicked:(id)sender {
    endTimeSelected = YES;
    NSIndexPath *ip;
    MeetingDetailsTableViewCell *cell;
    
    ip = [NSIndexPath indexPathForRow:3 inSection:0];
    cell = [dataTableView cellForRowAtIndexPath:ip];
    if ([cell.startTimeTF.text isEqualToString:@""]) {
        [self showMsgAlert:@"Please select the Start Time"];
    }
    else{
        if (_update) {
            NSDateFormatter *dateFormatEnd = [[NSDateFormatter alloc] init];
            [dateFormatEnd setDateFormat:@"hh:mm a"];
            NSString *dateString1 = [dateFormatEnd stringFromDate:startTime];
            startTime = [dateFormatEnd dateFromString:dateString1];
            endTimePicker.date = startTime;
            endTimePicker.minimumDate = startTime;
            
        }
        ip = [NSIndexPath indexPathForRow:4 inSection:0];
        cell = [dataTableView cellForRowAtIndexPath:ip];
        [cell.endTimeTF becomeFirstResponder];
        
    }
    
}


- (void)startTimeDone
{
    NSIndexPath *ip;
    MeetingDetailsTableViewCell *cell;
    //    startTime = nil;
    startTime = startTimePicker.date;
    endTimePicker.minimumDate = startTime;
    
    NSDateFormatter *dateFormatEnd = [[NSDateFormatter alloc] init];
    [dateFormatEnd setDateFormat:@"hh:mm a"];
    NSString *dateString = [dateFormatEnd stringFromDate:startTime];
    startTime = [dateFormatEnd dateFromString:dateString];
    NSString *dateString1 = [dateFormatEnd stringFromDate:endTime];
    endTime = [dateFormatEnd dateFromString:dateString1];
    
    NSLog(@"startTimePicker.date -----%@",startTime );
    NSTimeInterval diff = [endTime timeIntervalSinceDate:startTime];
    timeDifference = diff/60;
    NSLog(@"%ld",(long)timeDifference);
    ip = [NSIndexPath indexPathForRow:3 inSection:0];
    cell = [dataTableView cellForRowAtIndexPath:ip];
    cell.startTimeTF.text = [timeFormat stringFromDate:startTime];
    cell.startTimeIV.hidden = YES;
    [cell.startTimeTF resignFirstResponder];
}
- (void)endTimeDone
{
    //    endTime = nil;
    endTime = endTimePicker.date;
    startTimePicker.maximumDate = endTime;
    
    NSDateFormatter *dateFormatEnd = [[NSDateFormatter alloc] init];
    [dateFormatEnd setDateFormat:@"hh:mm a"];
    NSString *dateString = [dateFormatEnd stringFromDate:endTime];
    endTime = [dateFormatEnd dateFromString:dateString];
    NSString *dateString1 = [dateFormatEnd stringFromDate:startTime];
    startTime = [dateFormatEnd dateFromString:dateString1];
    NSLog(@"%ld",(long)timeDifference);
    
    NSTimeInterval diff = [endTime timeIntervalSinceDate:startTime];
    timeDifference = diff/60;
    NSLog(@"%ld",(long)timeDifference);
    
    NSIndexPath *ip = [NSIndexPath indexPathForRow:4 inSection:0];
    MeetingDetailsTableViewCell *cell = [dataTableView cellForRowAtIndexPath:ip];
    cell.endTimeTF.text = [timeFormat stringFromDate:endTime];
    cell.endTimeIV.hidden = YES;
    [cell.endTimeTF resignFirstResponder];
    
}


- (IBAction)MeetingModeClicked:(id)sender {
    NSIndexPath *ip = [NSIndexPath indexPathForRow:8 inSection:0];
    MeetingDetailsTableViewCell *cell = [dataTableView cellForRowAtIndexPath:ip];    // create an alert controller with action sheet appearance
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select" message:nil preferredStyle:UIAlertControllerStyleAlert];
    cell.meetingModeIV.hidden = YES;
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Remote" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        cell.meetingModeTF.text = @"Remote";
        modeString = @"Remote";
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"FaceToFace" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        cell.meetingModeTF.text = @"FaceToFace";
        modeString = @"FaceToFace";
        
        
    }];
    
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"Both" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        cell.meetingModeTF.text = @"Both";
        modeString = @"Both";
        
    }];
    
    // add actions to our sheet
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    [actionSheet addAction:action3];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        // Called when user taps outside
    }]];
    // bring up the action sheet
    [self presentViewController:actionSheet animated:YES completion:nil];
}
-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    [self updateTextLabelsWithText: newString];
    return YES;
}
-(void)updateTextLabelsWithText:(NSString *)string
{
    if (meetingDetails == 1) {
        if (![string isEqualToString:@""]) {
            NSIndexPath *ip ;
            ip = [NSIndexPath indexPathForRow:0 inSection:0];
            MeetingDetailsTableViewCell *cell = [dataTableView cellForRowAtIndexPath:ip];
            cell.meetingDescriptionIV.hidden = YES;
        }
        textViewText = string;
    }
    else{
        textViewTextAgenda = string;
    }
    
    
}
-(CGFloat)heightForText:(NSString*)text withFont:(UIFont *)font andWidth:(CGFloat)width
{
    CGSize constrainedSize = CGSizeMake(width, MAXFLOAT);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text attributes:attributesDictionary];
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    if (requiredHeight.size.width > width) {
        requiredHeight = CGRectMake(0,0,width, requiredHeight.size.height);
    }
    return requiredHeight.size.height;
}

- (void)textViewDidChange:(UITextView *)textView
{
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    if (newSize.height>200) {
        newSize.height = 200;
    }
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    if (meetingDetails == 1) {
        [dataTableView beginUpdates];
        textView.frame = newFrame;
        [dataTableView endUpdates];
    }
    else{
        if (180 + [self heightForText:textViewTextAgenda withFont:[UIFont systemFontOfSize:14] andWidth:190] < 350) {
            [agendaTableView beginUpdates];
            textView.frame = newFrame;
            [agendaTableView endUpdates];
        }
    }
    
}
- (IBAction)addBtn:(id)sender {
    roleNeed = NO;
    companyIDBool = NO;
    categoryIDBool = NO;
    olddata = NO;
    updatemember = NO;
    oldDataIndex = 5000;
    if (meetingDetails == 2) {
        comanyString = @"";
        catogeryString = @"";
        stakeHstring=@"";
        memberString = @"";
        
        popOverView.hidden = NO;
        membersPopoverTableView.hidden = NO;
        agendaTableView.hidden = YES;
        [membersPopoverTableView reloadData];
    }
    else if (meetingDetails == 3) {
        if (startTime != nil && endTime != nil){
            popOverView.hidden = NO;
            membersPopoverTableView.hidden = YES;
            agendaTableView.hidden = NO;
            textViewTextAgenda = @"";
            [agendaTableView reloadData];
        }
        else{
            [self showMsgAlert:@"Select start time and end time"];
        }
    }
}
- (IBAction)groupCategory:(id)sender {
    if([Utitlity isConnectedTointernet]){
        if ([mainCategoryString isEqualToString:@""]) {
            [self showMsgAlert:@"Please select Group"];
        }
        else{
            sharedManager.passingMode = @"groupCategory";
            sharedManager.passingId = mainCategoryID;
            //            sharedManager.passingString = companyString;
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
        }
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)mainCategoryBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        sharedManager.passingMode = @"mainCategory";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
        [self presentViewController:myNavController animated:YES completion:nil];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}

- (IBAction)membereCategoryBtn:(id)sender {
    olddata = NO;
    if([Utitlity isConnectedTointernet]){
        sharedManager.passingMode = @"category";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
        [self presentViewController:myNavController animated:YES completion:nil];
        categoryIDBool = NO;
        companyIDBool = NO;
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)membersCompanyBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        olddata = NO;
        NSIndexPath *ip ;
        MeetingDetailsTableViewCell *cell ;
        ip = [NSIndexPath indexPathForRow:0 inSection:0];
        cell = [membersPopoverTableView cellForRowAtIndexPath:ip];    // create an alert controller with action sheet appearance
        if ([cell.categoryTF.text isEqualToString:@""]) {
            [self showMsgAlert:@"Please select Catagory"];
        }
        else{
            sharedManager.passingMode = @"company";
            if (!categoryIDBool) {
                sharedManager.passingId = StakeholderCategory;
            }
            else{
                sharedManager.passingId = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"categoryID"];
            }
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
        }
        //    ip = [NSIndexPath indexPathForRow:2 inSection:0];
        //    cell = [membersPopoverTableView cellForRowAtIndexPath:ip];
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)membersFindBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        
        olddata = NO;
        
        NSIndexPath *ip ;
        ip = [NSIndexPath indexPathForRow:1 inSection:0];
        if([stakeHstring isEqualToString:@"Internal"]||[stakeHstring isEqualToString:@""])
        {
            sharedManager.passingMode = @"assignedToInternal";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
        }
        else
        {
            if ([comanyString isEqualToString:@""])
            {
                [self showMsgAlert:@"Please select Company"];
            }
            else
            {
                sharedManager.passingMode = @"assignedTo";
                if (!companyIDBool)
                {
                    sharedManager.passingId = Company;
                }
                else
                {
                    sharedManager.passingId = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"companyID"];
                }
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
                [self presentViewController:myNavController animated:YES completion:nil];
            }
        }
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)membersRoleBtn:(id)sender
{
    if([Utitlity isConnectedTointernet])
    {
        olddata = NO;
        NSIndexPath *ip ;
        ip = [NSIndexPath indexPathForRow:2 inSection:0];
        MeetingDetailsTableViewCell *cell = [membersPopoverTableView cellForRowAtIndexPath:ip];
        if ([cell.memberTF.text isEqualToString:@""])
        {
            [self showMsgAlert:@"Please select Member"];
        }
        else
        {
            sharedManager.passingMode = @"role";
            sharedManager.passingId = Company;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
        }
    }
    else
    {
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)typeBtn:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Compliance" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        typeString = @"Compliance";
        approveByString =@"";
        [dataTableView reloadData];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"NonCompliance" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        typeString = @"NonCompliance";
        approveByString = @"";
        [dataTableView reloadData];
    }];
    
    // add actions to our sheet
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        // Called when user taps outside
    }]];
    // bring up the action sheet
    [self presentViewController:actionSheet animated:YES completion:nil];
}
- (IBAction)approveByBtn:(id)sender {
    if([Utitlity isConnectedTointernet])
    {
        olddata = NO;
        
        if ([typeString isEqualToString:@""])
        {
            [self showMsgAlert:@"Please select Type"];
        }
        else
        {
            sharedManager.passingMode = @"approveBy";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
        }
    }
    else
    {
        [self showMsgAlert:NOInternetMessage];
    }
    
}
-(void)fillTheTF{
    
    if (sharedManager.passingString.length != 0) {
        if (sharedManager.passingString.length != 0) {
            if ([sharedManager.passingMode isEqualToString: @"mainCategory"]) {
                mainCategoryString= sharedManager.passingString;
                mainCategoryID = sharedManager.passingId;
                [dataTableView reloadData];
            }
        }
        if ([sharedManager.passingMode isEqualToString: @"groupCategory"]) {
            groupCategoryString= sharedManager.passingString;
            groupCategoryID = sharedManager.passingId;
        }
        if ([sharedManager.passingMode isEqualToString: @"category"]) {
            olddata = NO;
            comanyString = @"";
            memberString = @"";
            catogeryString = sharedManager.passingString;
            StakeholderCategory = sharedManager.passingId;
        }
        if ([sharedManager.passingMode isEqualToString: @"company"]) {
            memberString = @"";
            categoryIDBool = NO;
            companyIDBool = NO;
            comanyString = sharedManager.passingString;
            Company = sharedManager.passingId;
        }
        if ([sharedManager.passingMode isEqualToString: @"assignedTo"]) {
            roleString = @"";
            companyIDBool = NO;
            memberString = sharedManager.passingString;
            AssignedTo = sharedManager.passingId;
            AssignedToEmail = sharedManager.imageString1;
        }
        if ([sharedManager.passingMode isEqualToString: @"assignedToInternal"]) {
            roleString = @"";
            companyIDBool = NO;
            memberString = sharedManager.passingString;
            AssignedTo = sharedManager.passingId;
            AssignedToEmail = sharedManager.imageString1;
        }
        if ([sharedManager.passingMode isEqualToString: @"role"]) {
            roleString = sharedManager.passingString;
            Role = sharedManager.passingId;
        }
        if ([sharedManager.passingMode isEqualToString: @"approveBy"]) {
            approveByString = sharedManager.passingString;
            approveByID = sharedManager.passingId;
        }
        [self roleCell];
        [membersPopoverTableView reloadData];
        
    }
    
}
-(void)roleCell{
    if (updatemember && [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"update"]) {
    }
    else  if (memberString.length == 0) {
        if (roleNeed) {
            roleNeed = NO;
        }
    }
    else{
        roleNeed = YES;
    }
    
}
- (IBAction)membersDoneBtn:(id)sender {
    NSString *msg = @"";
    NSMutableArray *copyMemberArray= [membersArray mutableCopy];
    if (oldDataIndex !=5000) {
        [copyMemberArray removeObjectAtIndex:oldDataIndex];
    }
    if (   [[copyMemberArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(nameID contains[c] %@)", AssignedTo]] count] != 0) {
        msg = @"Member already exits";
    }
    
    if ([roleString isEqualToString:@""]) {
        msg = @"Please select role";
    }
    
    if ([memberString isEqualToString:@""]) {
        msg = @"Please select member";
    }
    if ([stakeHstring isEqualToString:@"External"]) {
        if ([comanyString isEqualToString:@""]) {
            msg = @"Please select Company";
        }
        
        if ([catogeryString isEqualToString:@""]) {
            msg = @"Please select Catagory";
        }
    }
    
    if ([stakeHstring isEqualToString:@""]) {
        msg = @"Please select stackholder type";
    }
    
    if (msg.length != 0) {
        [self showMsgAlert:msg];
    }
    else{
        NSDictionary *dict;
        if (updatemember && [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"update"])
        {
            dict= @{ @"name" : [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"name"],@"EmailID" : [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"EmailID"], @"role" : [NSString stringWithFormat:@"%@",roleString], @"roleID" : Role,@"nameID" : [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"nameID"],@"update":@"yes"};
        }
        else
        {
            if([stakeHstring isEqualToString:@"Internal"])
            {
                dict = @{ @"name" : [NSString stringWithFormat:@"%@",memberString],@"nameID" : AssignedTo, @"EmailID" : AssignedToEmail,@"role" : [NSString stringWithFormat:@"%@",roleString],@"roleID" : Role,@"company" : [NSString stringWithFormat:@"%@",@""],@"companyID" : @"" ,@"category" : [NSString stringWithFormat:@"%@",@""],@"categoryID" : @"",@"stakeHstring":stakeHstring};
            }
            else
            {
                dict = @{ @"name" : [NSString stringWithFormat:@"%@",memberString],@"nameID" : AssignedTo, @"EmailID" : AssignedToEmail,@"role" : [NSString stringWithFormat:@"%@",roleString],@"roleID" : Role,@"company" : [NSString stringWithFormat:@"%@",comanyString],@"companyID" : Company ,@"category" : [NSString stringWithFormat:@"%@",catogeryString],@"categoryID" : StakeholderCategory,@"stakeHstring":stakeHstring};
            }
        }
        if (oldDataIndex == 5000) {
            [membersArray addObject:dict];
        }
        else{
            [membersArray replaceObjectAtIndex:oldDataIndex withObject:dict];
        }
        [dataTableView reloadData];
        popOverView.hidden=YES;
    }
}
- (IBAction)cancelBtn:(id)sender {
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    membersPopoverTableView.hidden = YES;
    [dataTableView reloadData];
    popOverView.hidden = YES;
}
- (IBAction)agendaCancelBtn:(id)sender {
    agendaTableView.hidden = YES;
    popOverView.hidden = YES;
    [dataTableView reloadData];
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}
- (IBAction)agendaDoneBtn:(id)sender {
    NSIndexPath *ip ;
    MeetingDetailsTableViewCell *cell ;
    NSString *msg = @"";
    NSString *description;
    NSString *duration;
    ip = [NSIndexPath indexPathForRow:1 inSection:0];
    cell = [agendaTableView cellForRowAtIndexPath:ip];
    [cell.durationTF resignFirstResponder];
    if ([cell.durationTF.text isEqualToString:@""]) {
        msg = @"Please enter the duration";
    }
    else{
        duration = cell.durationTF.text;
    }
    ip = [NSIndexPath indexPathForRow:0 inSection:0];
    cell = [agendaTableView cellForRowAtIndexPath:ip];
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    if ([[cell.descriptionTV.text stringByTrimmingCharactersInSet: set] length] == 0){
        msg=[NSString stringWithFormat:@"Please enter the description"];
    }
    else{
        description = cell.descriptionTV.text;
    }
    sum= 0;
    for (int i =0 ; i<agendaArray.count; i++) {
        sum += [[[agendaArray objectAtIndex:i] objectForKey:@"duration"] intValue];
    }
    if (oldDataIndex == 5000) {
        if ([duration integerValue] > (timeDifference-sum)) {
            msg = @"Total Agenda duration is not equal to the meeting duration.Please increase/decrease Agenda duration...";
        }
    }
    else{
        if ([duration integerValue] > (timeDifference-sum+[[[agendaArray objectAtIndex:oldDataIndex] objectForKey:@"duration"] intValue])) {
            msg = @"Total Agenda duration is not equal to the meeting duration.Please increase/decrease Agenda duration...";
        }
    }
    
    if (msg.length != 0) {
        [self showMsgAlert:msg];
    }
    else{
        if (oldDataIndex == 5000) {
            NSDictionary *dict = @{ @"descripition" : description,@"duration" : duration};
            [agendaArray addObject:dict];
        }
        else{
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setObject:description forKey:@"descripition"];
            [dict setObject:duration forKey:@"duration"];
            if ([[agendaArray objectAtIndex:oldDataIndex] objectForKey:@"AgendaUniqueId"]) {
                [dict setObject:[[agendaArray objectAtIndex:oldDataIndex] objectForKey:@"AgendaUniqueId"] forKey:@"AgendaUniqueId"];
            }
            [agendaArray replaceObjectAtIndex:oldDataIndex withObject:dict];
        }
        sum= 0;
        for (int i =0 ; i<agendaArray.count; i++) {
            sum += [[[agendaArray objectAtIndex:i] objectForKey:@"duration"] intValue];
        }
        NSLog(@"agendaArray-----%@",agendaArray);
        [dataTableView reloadData];
        popOverView.hidden=YES;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == dataTableView) {
        
        if (meetingDetails == 2 || meetingDetails == 3) {
            if (meetingDetails == 2 && indexPath.row == 0) {
                NSLog(@"test");
            }
            else if( meetingDetails == 3 && agendaArray.count == 0){
                NSLog(@"test 1");
            }
            else{
                if (_update) {
                    updatemember = YES;
                }
                
                olddata = YES;
                oldDataIndex = indexPath.row;
                companyIDBool = YES;
                categoryIDBool = YES;
                popOverView.hidden = NO;
                if (meetingDetails == 2) {
                    
                    if (!updatemember && ![[membersArray objectAtIndex:oldDataIndex] objectForKey:@"update"]) {
                        catogeryString = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"category"];
                        StakeholderCategory = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"categoryID"];
                        comanyString = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"company"];
                        Company = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"companyID"];
                        stakeHstring = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"stakeHstring"];
                        
                    }
                    if (olddata)
                    {
                        if ([[membersArray objectAtIndex:indexPath.row] objectForKey:@"stakeHstring"]) {
                            stakeHstring = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"stakeHstring"];
                        }
                    }
                    memberString = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"name"];
                    AssignedTo = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"nameID"];
                    roleString = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"role"];
                    Role = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"roleID"];
                    AssignedToEmail = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"EmailID"];
                    membersPopoverTableView.hidden = NO;
                    agendaTableView.hidden = YES;
                    [membersPopoverTableView reloadData];
                }
                if (meetingDetails == 3 ) {
                    textViewTextAgenda = [[agendaArray objectAtIndex:oldDataIndex] objectForKey:@"descripition"];
                    membersPopoverTableView.hidden = YES;
                    agendaTableView.hidden = NO;
                    [agendaTableView reloadData];
                }
            }
        }
    }
}

-(void)getData{
    if([Utitlity isConnectedTointernet]){
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [self showProgress];
        NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@GetTimeZones?countryname=%@",BASEURL,[defaults objectForKey:@"CountryName"]]]];
        NSLog(@"-------%@",[NSString stringWithFormat:@"%@GetTimeZones?countryname=%@",BASEURL,[defaults objectForKey:@"CountryName"]]);
        //create the Method "GET"
        [urlRequest setHTTPMethod:@"GET"];
        
        NSURLSession *session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                              if(httpResponse.statusCode == 200)
                                              {
                                                  NSError *parseError = nil;
                                                  NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                                                  timeZone = [[dataArray objectAtIndex:0] objectForKey:@"TimeZone"];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [self hideProgress];
                                                      [dataTableView reloadData];
                                                  });
                                                  
                                              }
                                              else
                                              {
                                                  NSLog(@"Error");
                                              }
                                          }];
        [dataTask resume];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}

-(void)showProgress{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}

-(void)hideProgress{
    
    [MBProgressHUD hideHUDForView:self.view  animated:YES];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSIndexPath *ip;
    MeetingDetailsTableViewCell *cell;
    ip = [NSIndexPath indexPathForRow:7 inSection:0];
    cell = [dataTableView cellForRowAtIndexPath:ip];
    if (textField == cell.meetingRoomTF) {
        roomString  = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSLog(@"roomString---%@",roomString);
    }
    return YES;
}


- (IBAction)submitBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        
        NSString *msg;
        msg = @"";
        if (timeDifference != sum) {
            msg = @"Total Agenda duration is not equal to the meeting duration.Please increase/decrease Agenda duration...";
        }
        
        if (agendaArray.count == 0) {
            msg = @"Please enter agenda";
        }
        if ([typeString isEqualToString:@""]) {
            msg = @"Please select Type ";
        }
        if ([approveByString isEqualToString:@""]&& [typeString isEqualToString:@"Compliance"]) {
            msg = @"Please select Approve By ";
        }
        if ([mainCategoryString isEqualToString:@""]) {
            msg = @"Please select Group ";
        }
        if ( modeString == nil) {
            msg = @"Please select Meeting Mode ";
        }
        //        if ( roomString == nil) {
        //            msg = @"Please enter the Meeting Room";
        //        }
        if ( [timeFormat stringFromDate:endTime] == nil) {
            msg = @"Please select End Time";
        }
        if ( [timeFormat stringFromDate:startTime]  == nil) {
            msg = @"Please select Start Time ";
        }
        if ([formatter stringFromDate:curDate] == nil) {
            msg = @"Please select Date ";
        }
        if ([groupCategoryString isEqualToString:@""]) {
            msg = @"Please select Category ";
        }
        if ([mainCategoryString isEqualToString:@""]) {
            msg = @"Please select Group ";
        }
        
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        if ([[textViewText stringByTrimmingCharactersInSet: set] length] == 0){
            msg=[NSString stringWithFormat:@"Meeting Description is required"];
        }
        //    if ( [textViewText isEqualToString:@""]) {
        //        msg = @"Please enter Meeting Description";
        //    }
        if ([msg isEqualToString:@""]) {
            if (_update) {
                UIAlertController *alert;
                alert = [UIAlertController alertControllerWithTitle:nil message:@" Meeting calendar is saved successfully .Do you want to sent invitations?" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                     {
                                         [self updateSubmitApi:@1];
                                     }];
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                         {
                                             [self updateSubmitApi:@0];
                                         }];
                [alert addAction:ok];
                [alert addAction:cancel];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else{
                UIAlertController *alert;
                alert = [UIAlertController alertControllerWithTitle:nil message:@" Meeting calendar is saved successfully .Do you want to sent invitations?" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                     {
                                         [self submitApiCall:@1];
                                     }];
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                         {
                                             [self submitApiCall:@0];
                                         }];
                [alert addAction:ok];
                [alert addAction:cancel];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        else{
            [self showMsgAlert:msg];
        }
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
    
    
}
-(void)submitApiCall:(NSNumber *)InvitationFlag{
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:textViewText forKey:@"MeetingDescription"];
    [params setObject:[defaults objectForKey:@"CountryID"] forKey:@"CountryID"];
    [params setObject:[defaults objectForKey:@"LocationID"] forKey:@"LocationID"];
    [params setObject:[formatter stringFromDate:curDate] forKey:@"RecurringStartDate"];
    [params setObject:[formatter stringFromDate:curDate]  forKey:@"RecurringEndDate"];
    [params setObject:[formatter stringFromDate:curDate]  forKey:@"MeetingDate"];
    [params setObject:[defaults objectForKey:@"UserID"] forKey:@"CreatedBy"];
    [params setObject:[defaults objectForKey:@"UserID"] forKey:@"ModifiedBy"];
    [params setObject:modeString forKey:@"Mode"];
    if ( roomString == nil) {
        [params setObject:@"" forKey:@"MeetingPlace"];
    }
    else
        [params setObject:roomString forKey:@"MeetingPlace"];
    [params setObject:[timeFormat stringFromDate:startTime] forKey:@"FromTime"];
    [params setObject:[timeFormat stringFromDate:endTime] forKey:@"ToTime"];
    [params setObject:timeZone forKey:@"TimeZone"];
    [params setObject:[NSString stringWithFormat:@"%ld",(long)timeDifference] forKey:@"Duration"];
    [params setObject:mainCategoryID forKey:@"GroupMasterID"];
    [params setObject:groupCategoryID forKey:@"CategoryId"];
    [params setObject:typeString forKey:@"StatutoryType"];
    if ([typeString isEqualToString:@"Compliance"]) {
        [params setObject:approveByID forKey:@"ApproverId"];
    }
    else{
        [params setObject:@"0" forKey:@"ApproverId"];
    }
    NSMutableArray *agendaArrayDict = [[NSMutableArray alloc] init];
    for (int i = 0; i < agendaArray.count; i++) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        [dict setObject:[formatter stringFromDate:curDate] forKey:@"MeetingDate"];
        [dict setObject:[[agendaArray objectAtIndex:i] objectForKey:@"descripition"] forKey:@"AgendaDescription"];
        [dict setObject:[NSString stringWithFormat:@"%@",[[agendaArray objectAtIndex:i] objectForKey:@"duration"]] forKey:@"AgendaDuration"];
        [agendaArrayDict addObject:dict];
    }
    [params setObject:agendaArrayDict forKey:@"lstAdhocAgenda"];
    NSMutableArray *memberArrayDict = [[NSMutableArray alloc] init];
    for (int i = 0; i < membersArray.count; i++) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        [dict setObject:[formatter stringFromDate:curDate] forKey:@"MeetingDate"];
        [dict setObject:[[membersArray objectAtIndex:i] objectForKey:@"nameID"] forKey:@"UserID"];
        [dict setObject:[[membersArray objectAtIndex:i] objectForKey:@"roleID"] forKey:@"MeetingRoleID"];
        [memberArrayDict addObject:dict];
    }
    [params setObject:memberArrayDict forKey:@"lstAdhocMembers"];
    [params setObject:InvitationFlag forKey:@"InvitationFlag"];
    
    NSLog(@"post Diict ---->%@",params);
    
    [self showProgress];
    NSString* JsonString = [Utitlity JSONStringConv: params];
    NSLog(@"json----%@",JsonString);
    
    [[WebServices sharedInstance]apiAuthwithJSON:PostCreateAdhocMeeting HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
        
        NSString *error=[json valueForKey:@"error"];
        [self hideProgress];
        
        if(error.length>0){
            [self showMsgAlert:[json valueForKey:@"error_description"]];
            return ;
        }else{
            if(json.count==0){
                [self showMsgAlert:@"Error , Try Again"];
            }else{
                sharedManager.slideMenuSlected = @"yes";
                [self viewDidLoad];
                [dataTableView reloadData];
                if ([InvitationFlag isEqualToNumber:@1]) {
                    [self showMsgAlert:@"Invitations are sent"];
                }
                else{
                    [self showMsgAlert:@"Meeting created successfully."];
                }
            }
        }
        
    }];
    
}
-(void)updateSubmitApi:(NSNumber *)InvitationFlag{
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:MeetingTypeID forKey:@"MeetingTypeID"];
    [params setObject:textViewText forKey:@"MeetingDescription"];
    [params setObject:mainCategoryID forKey:@"GroupMasterID"];
    [params setObject:groupCategoryID forKey:@"CategoryId"];
    [params setObject:[formatter stringFromDate:curDate]  forKey:@"MeetingDate"];
    [params setObject:[defaults objectForKey:@"UserID"] forKey:@"ModifiedBy"];
    [params setObject:modeString forKey:@"Mode"];
    [params setObject:[formatter stringFromDate:MeetingDateOld]  forKey:@"MeetingDateOld"];
    [params setObject:@"" forKey:@"MeetingPwd"];
    [params setObject:@"" forKey:@"HostCode"];
    [params setObject:@"" forKey:@"MeetingNumber"];
    [params setObject:@"" forKey:@"ParticipantCode"];
    [params setObject:typeString forKey:@"StatutoryType"];
    if ([typeString isEqualToString:@"Compliance"]) {
        [params setObject:approveByID forKey:@"ApproverId"];
    }
    else{
        [params setObject:@"0" forKey:@"ApproverId"];
    }
    [params setObject:roomString forKey:@"MeetingPlace"];
    [params setObject:[timeFormat stringFromDate:startTime] forKey:@"FromTime"];
    [params setObject:[timeFormat stringFromDate:endTime] forKey:@"ToTime"];
    [params setObject:[NSString stringWithFormat:@"%ld",(long)timeDifference] forKey:@"Duration"];
    
    NSMutableArray *agendaArrayDict = [[NSMutableArray alloc] init];
    for (int i = 0; i < agendaArray.count+agendaDeletedArray.count; i++) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        //        if ([[formatter stringFromDate:curDate] isEqualToString:[formatter stringFromDate:MeetingDateOld]]) {
        [dict setObject:[formatter stringFromDate:curDate]  forKey:@"MeetingDate"];
        //        }
        //        else{
        //            [dict setObject:[formatter stringFromDate:curDate]  forKey:@"MeetingDate"];
        //        }
        [dict setObject:[formatter stringFromDate:MeetingDateOld] forKey:@"MeetingDateOld"];
        [dict setObject:MeetingTypeID forKey:@"MeetingTypeID"];
        
        if (i<agendaArray.count) {
            [dict setObject:[[agendaArray objectAtIndex:i] objectForKey:@"descripition"] forKey:@"AgendaDescription"];
            [dict setObject:[NSString stringWithFormat:@"%@",[[agendaArray objectAtIndex:i] objectForKey:@"duration"]] forKey:@"AgendaDuration"];
            if ([[agendaArray objectAtIndex:i] objectForKey:@"AgendaUniqueId"]) {
                [dict setObject:[NSString stringWithFormat:@"%@",[[agendaArray objectAtIndex:i] objectForKey:@"AgendaUniqueId"]] forKey:@"AgendaUniqueId"];
            }
            [dict setObject:@0 forKey:@"IsDeleted"];
        }
        else{
            if ([[agendaDeletedArray objectAtIndex:i - agendaArray.count] objectForKey:@"update"]) {
                [dict setObject:[[agendaDeletedArray objectAtIndex:i - agendaArray.count] objectForKey:@"descripition"] forKey:@"AgendaDescription"];
                [dict setObject:[NSString stringWithFormat:@"%@",[[agendaDeletedArray objectAtIndex:i - agendaArray.count] objectForKey:@"duration"]] forKey:@"AgendaDuration"];
                if ([[agendaDeletedArray objectAtIndex:i - agendaArray.count] objectForKey:@"AgendaUniqueId"]) {
                    [dict setObject:[NSString stringWithFormat:@"%@",[[agendaDeletedArray objectAtIndex:i - agendaArray.count] objectForKey:@"AgendaUniqueId"]] forKey:@"AgendaUniqueId"];
                }
                [dict setObject:@1 forKey:@"IsDeleted"];
            }
        }
        [agendaArrayDict addObject:dict];
    }
    
    [params setObject:agendaArrayDict forKey:@"lstAdhocAgenda"];
    NSMutableArray *memberArrayDict = [[NSMutableArray alloc] init];
    for (int i = 0; i < membersArray.count + membersDeletedArray.count; i++) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        //        if ([[formatter stringFromDate:curDate] isEqualToString:[formatter stringFromDate:MeetingDateOld]]) {
        [dict setObject:[formatter stringFromDate:curDate]  forKey:@"MeetingDate"];
        //        }
        //        else{
        //            [dict setObject:[formatter stringFromDate:curDate]  forKey:@"MeetingDate"];
        //        }
        [dict setObject:[formatter stringFromDate:MeetingDateOld] forKey:@"MeetingDateOld"];
        [dict setObject:MeetingTypeID forKey:@"MeetingTypeID"];
        [dict setObject:@0 forKey:@"ALevelID"];
        
        if ( membersArray.count > i) {
            [dict setObject:[[membersArray objectAtIndex:i] objectForKey:@"EmailID"] forKey:@"EmailID"];
            [dict setObject:[[membersArray objectAtIndex:i] objectForKey:@"name"] forKey:@"EmpName"];
            [dict setObject:[[membersArray objectAtIndex:i] objectForKey:@"nameID"] forKey:@"UserID"];
            [dict setObject:[[membersArray objectAtIndex:i] objectForKey:@"roleID"] forKey:@"MeetingRoleID"];
            [dict setObject:@0 forKey:@"IsDeleted"];
        }
        else{
            //            if ([[membersDeletedArray objectAtIndex:i - membersArray.count] objectForKey:@"update"] ) {
            [dict setObject:[[membersDeletedArray objectAtIndex:i - membersArray.count] objectForKey:@"EmailID"] forKey:@"EmailID"];
            [dict setObject:[[membersDeletedArray objectAtIndex:i - membersArray.count] objectForKey:@"name"] forKey:@"EmpName"];
            [dict setObject:[[membersDeletedArray objectAtIndex:i - membersArray.count] objectForKey:@"nameID"] forKey:@"UserID"];
            [dict setObject:[[membersDeletedArray objectAtIndex:i - membersArray.count] objectForKey:@"roleID"] forKey:@"MeetingRoleID"];
            [dict setObject:@1 forKey:@"IsDeleted"];
            //            }
        }
        [memberArrayDict addObject:dict];
    }
    [params setObject:memberArrayDict forKey:@"lstAdhocMembers"];
    [params setObject:InvitationFlag forKey:@"InvitationFlag"];
    
    NSLog(@"post Diict ---->%@",params);
    
    [self showProgress];
    NSString* JsonString = [Utitlity JSONStringConv: params];
    NSLog(@"json----%@",JsonString);
    
    [[WebServices sharedInstance]apiAuthwithJSON:POSTUpdateAdhocMeeting HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
        
        NSString *error=[json valueForKey:@"error"];
        [self hideProgress];
        
        if(error.length>0){
            [self showMsgAlert:[json valueForKey:@"error_description"]];
            return ;
        }else{
            //            if(json.count==0){
            //                [self showMsgAlert:@"Error , Try Again"];
            //            }else{
            NSString *msg;
            if ([InvitationFlag isEqualToNumber:@1]) {
                msg = @" Meeting calendar is saved successfully .Invitations are sent";
            }
            else{
                msg = @" Meeting calendar is saved successfully .";
            }
            UIAlertController *alert;
            alert = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                 {
                                     [self dismissViewControllerAnimated:YES completion:nil];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            //            }
        }
    }];
}
-(void)updateData
{
    if([Utitlity isConnectedTointernet]){
        [self showProgress];
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/mm/yyyy"];
        NSDate *meetingdate = [dateFormatter dateFromString:sharedManager.passingString];
        [dateFormatter setDateFormat:@"yyyy/mm/dd"];
        NSString *targetUrl = [NSString stringWithFormat:@"%@GETAdhocMeetingDetailsByMeetingtypeId?meetingtypeid=%@&date=%@", BASEURL,sharedManager.passingId,[dateFormatter stringFromDate:meetingdate]];
        NSLog(@"targetUrl=====%@",targetUrl);
        MeetingTypeID = sharedManager.passingId;
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:targetUrl]];
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
          ^(NSData * _Nullable data,
            NSURLResponse * _Nullable response,
            NSError * _Nullable error) {
              NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
              NSLog(@"%@",dataArray);
              textViewText = [[dataArray objectAtIndex:0] objectForKey:@"MeetingDescription"];
              [dateFormatter setDateFormat:@"dd MMM yyyy"];
              curDate = [dateFormatter dateFromString:[[dataArray objectAtIndex:0] objectForKey:@"MeetingDate"]];
              MeetingDateOld = [dateFormatter dateFromString:[[dataArray objectAtIndex:0] objectForKey:@"MeetingDate"]];
              modeString = [[dataArray objectAtIndex:0] objectForKey:@"Mode"];
              roomString = [[dataArray objectAtIndex:0] objectForKey:@"MeetingPlace"];
              [dateFormatter setDateFormat:@"mm/dd/yyyy hh:mm:ss a"];
              startTime = [dateFormatter dateFromString:[[dataArray objectAtIndex:0] objectForKey:@"FromTime"]];
              endTime = [dateFormatter dateFromString:[[dataArray objectAtIndex:0] objectForKey:@"ToTime"]];
              timeZone = [[dataArray objectAtIndex:0] objectForKey:@"TimeZone"];
              mainCategoryString = [[dataArray objectAtIndex:0] objectForKey:@"GroupMasterDescription"];
              mainCategoryID = [[dataArray objectAtIndex:0] objectForKey:@"GroupMasterID"];
              groupCategoryString = [[dataArray objectAtIndex:0] objectForKey:@"CategoryDescription"];
              groupCategoryID = [[dataArray objectAtIndex:0] objectForKey:@"CategoryId"];
              approveByID = [[dataArray objectAtIndex:0] objectForKey:@"ApproverId"];
              if ([[dataArray objectAtIndex:0] objectForKey:@"ApproverName"] !=[NSNull null]) {
                  approveByString = [[dataArray objectAtIndex:0] objectForKey:@"ApproverName"];
              }
              typeString = [[dataArray objectAtIndex:0] objectForKey:@"StatutoryType"];
              
              NSCalendar *gregorian = [[NSCalendar alloc]
                                       initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
              NSUInteger unitFlags = NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitMinute;
              
              NSDateComponents *components = [gregorian components:unitFlags
                                                          fromDate:startTime
                                                            toDate:endTime options:0];
              timeDifference = components.minute;
              NSLog(@"%ld is the time difference",timeDifference);
              for (int i = 0; i < [[[dataArray objectAtIndex:0] objectForKey:@"lstAdhocMembers"] count]; i++) {
                  NSDictionary *dict= @{ @"name" : [[[[dataArray objectAtIndex:0] objectForKey:@"lstAdhocMembers"] objectAtIndex:i] objectForKey:@"EmpName"],@"EmailID" : [[[[dataArray objectAtIndex:0] objectForKey:@"lstAdhocMembers"] objectAtIndex:i] objectForKey:@"EmailID"], @"role" : [[[[dataArray objectAtIndex:0] objectForKey:@"lstAdhocMembers"] objectAtIndex:i] objectForKey:@"RoleDescription"], @"roleID" : [[[[dataArray objectAtIndex:0] objectForKey:@"lstAdhocMembers"] objectAtIndex:i] objectForKey:@"MeetingRoleID"],@"nameID" : [[[[dataArray objectAtIndex:0] objectForKey:@"lstAdhocMembers"] objectAtIndex:i] objectForKey:@"UserID"],@"update":@"yes"};
                  [membersArray addObject:dict];
              }
              for (int i = 0; i < [[[dataArray objectAtIndex:0] objectForKey:@"lstAdhocAgenda"] count]; i++) {
                  NSLog(@"AgendaUniqueId--------%@",[[[[dataArray objectAtIndex:0] objectForKey:@"lstAdhocAgenda"] objectAtIndex:i] objectForKey:@"AgendaUniqueId"]);
                  NSDictionary *dict = @{ @"descripition" : [[[[dataArray objectAtIndex:0] objectForKey:@"lstAdhocAgenda"] objectAtIndex:i] objectForKey:@"AgendaDescription"],@"AgendaUniqueId" : [[[[dataArray objectAtIndex:0] objectForKey:@"lstAdhocAgenda"] objectAtIndex:i] objectForKey:@"AgendaUniqueId"],@"duration" : [[[[dataArray objectAtIndex:0] objectForKey:@"lstAdhocAgenda"] objectAtIndex:i] objectForKey:@"AgendaDuration"],@"update":@"yes"};
                  [agendaArray addObject:dict];
              }
              NSLog(@"agendaArray------%@",agendaArray);
              sum= 0;
              for (int i =0 ; i<agendaArray.count; i++) {
                  sum += [[[agendaArray objectAtIndex:i] objectForKey:@"duration"] intValue];
              }
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self hideProgress];
                  [dataTableView reloadData];
              });
          }] resume];
    }
    else
    {
        [self showMsgAlert:NOInternetMessage];
    }
}
-(IBAction)UpdateDeleteMemberBtn:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure do you want to delete the Member" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                         {
                             CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:dataTableView];
                             NSIndexPath *indexPath = [dataTableView indexPathForRowAtPoint:buttonPosition];
                             [membersDeletedArray addObject:membersArray[indexPath.row]];
                             NSLog(@"membersDeletedArray---------%@",membersDeletedArray);
                             [membersArray removeObjectAtIndex:indexPath.row];
                             NSLog(@"membersArray---------%@",membersArray);
                             [dataTableView reloadData];
                         }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)updateDeleteAgendaBtn:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure do you want to delete the agenda" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                         {
                             CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:dataTableView];
                             NSIndexPath *indexPath = [dataTableView indexPathForRowAtPoint:buttonPosition];
                             [agendaDeletedArray addObject:agendaArray[indexPath.row]];
                             [agendaArray removeObjectAtIndex:indexPath.row];
                             sum= 0;
                             for (int i =0 ; i<agendaArray.count; i++) {
                                 sum += [[[agendaArray objectAtIndex:i] objectForKey:@"duration"] intValue];
                             }
                             [dataTableView reloadData];
                         }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)deleteMemberBtn:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure do you want to delete the Member" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                         {
                             membersPopoverTableView.hidden = YES;
                             popOverView.hidden = YES;
                             if (olddata) {
                                 [membersDeletedArray addObject:membersArray[oldDataIndex]];
                                 NSLog(@"membersDeletedArray-------%@",membersDeletedArray);
                                 [membersArray removeObjectAtIndex:oldDataIndex];
                                 [dataTableView reloadData];
                                 [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
                                 
                             }
                         }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)deleteAgendaBtn:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure do you want to delete the Agenda" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                         {
                             agendaTableView.hidden = YES;
                             popOverView.hidden = YES;
                             if (oldDataIndex != 5000) {
                                 [agendaDeletedArray addObject:agendaArray[oldDataIndex]];
                                 [agendaArray removeObjectAtIndex:oldDataIndex];
                                 sum= 0;
                                 for (int i =0 ; i<agendaArray.count; i++) {
                                     sum += [[[agendaArray objectAtIndex:i] objectForKey:@"duration"] intValue];
                                 }
                                 [dataTableView reloadData];
                                 [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
                             }
                         }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)StakeHbtn:(UIButton *)sender {
    NSIndexPath *ip = [NSIndexPath indexPathForRow:0 inSection:0];
    MeetingDetailsTableViewCell *cell = [membersPopoverTableView cellForRowAtIndexPath:ip];    // create an alert controller with action sheet appearance
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select" message:nil preferredStyle:UIAlertControllerStyleAlert];
    //    cell.StakeHIV.hidden = YES;
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Internal" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        cell.StakeHTF.text = @"Internal";
        stakeHstring = @"Internal";
        [self clearMember];
        [membersPopoverTableView reloadData];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"External" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        cell.StakeHTF.text = @"External";
        stakeHstring = @"External";
        [self clearMember];
        [membersPopoverTableView reloadData];
    }];
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        // Called when user taps outside
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}
-(void)clearMember{
    comanyString = @"";
    catogeryString = @"";
    memberString = @"";
    roleString = @"";
}
@end

