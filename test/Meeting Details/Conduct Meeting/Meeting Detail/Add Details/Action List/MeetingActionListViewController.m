//
//  MeetingActionListViewController.m
//  test
//
//  Created by ceaselez on 18/01/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "MeetingActionListViewController.h"
#import "CustomTableViewCell.h"
#import "MySharedManager.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "AddActionViewController.h"
#import "PreviewMOMViewController.h"
#import "MySharedManager.h"

@interface MeetingActionListViewController ()
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeLabelConstrain;

@end

@implementation MeetingActionListViewController
{
    NSArray *actionList;
    UIRefreshControl *refreshControl;
    NSTimer *timer;
    long currMinute;
    long currSeconds;
    long currHours;
    BOOL decrement;
    BOOL mins;
    MySharedManager *sharedManager;
    __weak IBOutlet UILabel *timeLabel;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    sharedManager = [MySharedManager sharedManager];
    if (sharedManager.realTime) {
        decrement = sharedManager.decrement;
        currHours = sharedManager.currHour;
        currSeconds = sharedManager.currSecs;
        currMinute =  sharedManager.currmins ;
        NSString *countDownTimer=[NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds];
        NSDate *now = [NSDate date];
        
        static NSDateFormatter *dateFormatter;
        if (!dateFormatter) {
            dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"E MMM dd yyyy hh:mm:ss a";  // very simple format  "8:47:22 AM"
        }
        timeLabel.text = [NSString stringWithFormat:@" %@ \nGMT+05:30(India Standard Time) %@",[dateFormatter stringFromDate:now],countDownTimer];

        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
    }
    else{
        timeLabel.hidden = YES;
        _timeLabelConstrain.constant = 20;
    }
    [self loadDataFromApi];
    refreshControl = [[UIRefreshControl alloc]init];
    [self.dataTableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(loadDataFromApi) forControlEvents:UIControlEventValueChanged];
    
}

-(void) viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self loadDataFromApi];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        if (actionList.count == 0) {
            return 1;
        }
        else{
            return actionList.count;
        }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 160;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomTableViewCell *cell ;

        if (actionList.count == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        }
        else{
            cell = [tableView dequeueReusableCellWithIdentifier:@"dataCell" forIndexPath:indexPath];
            cell.actionListType.text = [[actionList objectAtIndex:indexPath.row] objectForKey:@"ActionType"];
            cell.actionListTargetDate.text = [[actionList objectAtIndex:indexPath.row] objectForKey:@"TargetDate"];
            cell.actionListAssignee.text = [[actionList objectAtIndex:indexPath.row] objectForKey:@"EmpName"];
            cell.actionListDescripition.text = [[actionList objectAtIndex:indexPath.row] objectForKey:@"ActionDescription"];
        }
    return  cell;
}
- (IBAction)addBtn:(id)sender {
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    AddActionViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"AddActionViewController"];
    //    myNavController.update = NO;
    //    [self presentViewController:myNavController animated:YES completion:nil];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddActionViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"AddActionViewController"];
    myNavController.update = NO;
    myNavController.meetingAction = YES;
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/mm/yyyy"];
    NSDate *meetingDate =[dateFormatter dateFromString:[_meetingDetails objectForKey:@"meetingDate"]];
    [dateFormatter setDateFormat:@"yyyy/mm/dd"];
    myNavController.meetingID = [_meetingDetails objectForKey:@"meetingID"];
    myNavController.meetingDate = [dateFormatter stringFromDate:meetingDate];
    //    sharedManager.passingId = [[actionList objectAtIndex:indexPath.row] objectForKey:@"ActionID"];
    //    myNavController.meetingActionDict = [actionList objectAtIndex:indexPath.row];
    [self presentViewController:myNavController animated:YES completion:nil];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    sharedManager.passingId = [[actionList objectAtIndex:indexPath.row] objectForKey:@"ActionPlannerID"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddActionViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"AddActionViewController"];
    myNavController.update = YES;
    myNavController.meetingAction = YES;
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/mm/yyyy"];
    NSDate *meetingDate =[dateFormatter dateFromString:[_meetingDetails objectForKey:@"meetingDate"]];
    [dateFormatter setDateFormat:@"yyyy/mm/dd"];
    myNavController.meetingID = [_meetingDetails objectForKey:@"meetingID"];
    myNavController.meetingDate = [dateFormatter stringFromDate:meetingDate];
//    sharedManager.passingId = [[actionList objectAtIndex:indexPath.row] objectForKey:@"ActionID"];
    myNavController.meetingActionDict = [actionList objectAtIndex:indexPath.row];

    [self presentViewController:myNavController animated:YES completion:nil];
}
-(void)loadDataFromApi{
    if([Utitlity isConnectedTointernet]){
        [self showProgress];
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/mm/yyyy"];
        NSDate *meetingDate =[dateFormatter dateFromString:[_meetingDetails objectForKey:@"meetingDate"]];
        [dateFormatter setDateFormat:@"yyyy/mm/dd"];
        NSString *targetUrl = [NSString stringWithFormat:@"%@GetAdhocMeetingActions?MeetingDate=%@&MeetingID=%@", BASEURL,[dateFormatter stringFromDate:meetingDate],[_meetingDetails objectForKey:@"meetingID"]];
        NSLog(@"targetUrl-----%@",targetUrl);
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:targetUrl]];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
          ^(NSData * _Nullable data,
            NSURLResponse * _Nullable response,
            NSError * _Nullable error) {
              actionList = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
              NSLog(@"%@",actionList);
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self hideProgress];
                  
                  [refreshControl endRefreshing];
                  
                  [_dataTableView reloadData];
              });
              
          }] resume];
        
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)showProgress{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:_dataTableView animated:YES];
    
}

-(void)hideProgress{
    [MBProgressHUD hideHUDForView:_dataTableView animated:YES];
}
-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)backBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)nextBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
        [params setObject:[_meetingDetails objectForKey:@"meetingID"] forKey:@"MeetingTypeId"];
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/mm/yyyy"];
        NSDate *meetingDate =[dateFormatter dateFromString:[_meetingDetails objectForKey:@"meetingDate"]];
        [dateFormatter setDateFormat:@"yyyy/mm/dd"];
        [params setObject:[dateFormatter stringFromDate:meetingDate] forKey:@"MeetingDate"];
        [params setObject:[defaults objectForKey:@"EmployeeName"] forKey:@"MinutesTakenBy"];
        [dateFormatter setDateFormat:@"hh:mm a"];
        NSDate *endDate =[NSDate date];

        if (sharedManager.realTime) {
            NSDate *startDate =[dateFormatter dateFromString:[_meetingDetails objectForKey:@"MeetingStartTime"]];
            NSLog(@"%@",endDate);
            NSString *todayDate =[dateFormatter stringFromDate:endDate];
            endDate = [dateFormatter dateFromString:todayDate];
            NSTimeInterval diff = [endDate timeIntervalSinceDate:startDate];
            int difference = diff/60;
            [params setObject:[NSString stringWithFormat:@"%@ to %@ (%d Mins)",[_meetingDetails objectForKey:@"MeetingStartTime"],[dateFormatter stringFromDate:endDate],difference] forKey:@"ActualTime"];
        }
        else{
            [params setObject:[_meetingDetails objectForKey:@"meetingActualTime"] forKey:@"ActualTime"];
        }
        [dateFormatter setDateFormat:@"dd-mm-yyyy"];
        [params setObject:[NSString stringWithFormat:@"%@-%@(Adhoc)",[_meetingDetails objectForKey:@"PDFName"],[dateFormatter stringFromDate:meetingDate]] forKey:@"PDFName"];
        [params setObject:[_meetingDetails objectForKey:@"Mode"] forKey:@"Mode"];

        [self showProgress];
        NSString* JsonString = [Utitlity JSONStringConv: params];
        NSLog(@"json----%@",JsonString);
        
        [[WebServices sharedInstance]apiAuthwithJSON:POSTSavePDF HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
            
            NSString *error=[json valueForKey:@"error"];
            [self hideProgress];
            
            if(error.length>0){
                [self showMsgAlert:[json valueForKey:@"error_description"]];
                return ;
            }else{
                if(json.count==0){
                    [self showMsgAlert:@"Error , Try Again"];
                }else{
                    NSLog(@"json-----%@",json);
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    PreviewMOMViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"PreviewMOMViewController"];
                    myNavController.pdfUrl = [json valueForKey:@"SavePDFResult"];
                    myNavController.meetingDetails = _meetingDetails;
                    sharedManager.decrement = decrement;
                    sharedManager.currmins = currMinute;
                    sharedManager.currHour = currHours;
                    sharedManager.currSecs = currSeconds;
                    if (sharedManager.realTime) {
                        [dateFormatter setDateFormat:@"hh:mm a"];

                        myNavController.ActualStartTime = [_meetingDetails objectForKey:@"MeetingStartTime"];
                        myNavController.ActualEndTime = [dateFormatter stringFromDate:endDate];
                    }
                    [self presentViewController:myNavController animated:YES completion:nil];

                }
            }
            
        }];
    }
    else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (void)timerTick:(NSTimer *)timer {
    NSDate *now = [NSDate date];
    
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"E MMM dd yyyy hh:mm:ss a";  // very simple format  "8:47:22 AM"
    }
    NSString *countDownTimer ;
    if (decrement) {
        countDownTimer =  [self  timerFired];
    }
    else{
        countDownTimer = [self timerincrementer];
    }
    timeLabel.text = [NSString stringWithFormat:@" %@ \nGMT+05:30(India Standard Time) %@",[dateFormatter stringFromDate:now],countDownTimer];
}
-(NSString*)timerFired
{
    if(currMinute == 0 && currSeconds == 0 && currHours == 0)
    {
        decrement = NO;
    }
    else{
        if(currMinute == 0 && currHours>0 && currSeconds==0)
        {
            currHours-=1;
            currMinute=59;
            mins = NO;
        }
        else if(currSeconds==0 && currMinute>0 )
        {
            if (mins) {
                currMinute-=1;
            }
            currSeconds=59;
        }
        else if(currSeconds>0 )
        {
            currSeconds-=1;
            mins = YES;
        }
    }
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds];
}
-(NSString*)timerincrementer
{
    if(currSeconds<59)
    {
        currSeconds+=1;
    }
    else if(currSeconds == 59 && currMinute < 59)
    {
        currMinute+=1;
        currSeconds=0;
    }
    else  if(currMinute == 59)
    {
        currHours+=1;
        currMinute=0;
        currSeconds=0;
    }
    
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds];
}
- (IBAction)deleteBtn:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.dataTableView];
    NSIndexPath *indexPath = [self.dataTableView indexPathForRowAtPoint:buttonPosition];
    NSArray *ActionList;
    //    if (searchEnabled) {
    //        ActionList = _searchResult;
    //    }
    //    else{
    ActionList = actionList;
    //    }
    NSLog(@"[ActionList objectAtIndex:indexPath.row]---%@",[ActionList objectAtIndex:indexPath.row]);
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure do you want to delete the action" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction*  _Nonnull action)
                        {
                                if([Utitlity isConnectedTointernet]){
                                    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
                                    NSString *dateString =  [[ActionList objectAtIndex:indexPath.row] objectForKey:@"MeetingDate"];
                                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                                    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
                                    NSDate *date = [dateFormatter dateFromString:dateString];
                                    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                                    [params setObject:[dateFormatter stringFromDate:date] forKey:@"MeetingDate"];
                                    [params setObject:[[ActionList objectAtIndex:indexPath.row] objectForKey:@"MeetingTypeID"]  forKey:@"MeetingID"];
                                    [params setObject:[[ActionList objectAtIndex:indexPath.row] objectForKey:@"ActionID"]  forKey:@"actionid"];
                                    [self showProgress];
                                    NSString* JsonString = [Utitlity JSONStringConv: params];
                                    NSLog(@"jsonstring-----%@",JsonString);
                                    [[WebServices sharedInstance]apiAuthwithJSON:PostDeleteAdhocMeetingActions HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary* json, NSURLResponse*  headerResponse) {
                                        
                                        NSString *error=[json valueForKey:@"error"];
                                        [self hideProgress];
                                        
                                        if(error.length>0){
                                            [self showMsgAlert:[json valueForKey:@"error_description"]];
                                            return ;
                                        }else{
                                            [self showMsgAlert:@"Deleted successfully"];
                                            NSLog(@"json-----%@",json);
                                            [self loadDataFromApi];
                                        }
                                    }];
                                }else{
                                    [self showMsgAlert:NOInternetMessage];
                                }
                        }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
