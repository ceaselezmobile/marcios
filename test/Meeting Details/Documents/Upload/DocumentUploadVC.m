//
//  DocumentUploadVC.m
//  test
//
//  Created by ceaselez on 17/05/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "DocumentUploadVC.h"
#import "MySharedManager.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SearchViewController.h"
#import "MyMeetingTableViewCell.h"
#import "SWRevealViewController.h"
#import "UploadFilesVC.h"

@interface DocumentUploadVC ()<UIDocumentPickerDelegate,UINavigationControllerDelegate,UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *revealButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;

@end

@implementation DocumentUploadVC
{
    MySharedManager *sharedManager;
    NSString *meetingID;
    //    __weak IBOutlet UIButton *documentBtn;
    __weak IBOutlet UIButton *dateBtn;
    __weak IBOutlet UIButton *meetingBtn;
    __weak IBOutlet UITableView *dataTableView;
    NSMutableArray *dataArray;
    __weak IBOutlet UIButton *momBtn;
    __weak IBOutlet UIView *agendaDocumentView;
    __weak IBOutlet UIView *MOMView;
    __weak IBOutlet UIView *popOverView;
    __weak IBOutlet UIWebView *webView;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    sharedManager = [MySharedManager sharedManager];
    dataArray = [[NSMutableArray alloc] init];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    meetingID = @"";
    SWRevealViewController *revealViewController = self.revealViewController;
    popOverView.hidden = YES;

    if ( revealViewController )
    {
        [self.revealButtonItem addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        sharedManager.slideMenuSlected = @"no";
        agendaDocumentView.hidden = YES;
        MOMView.hidden = YES;
        meetingID = @"";
        [meetingBtn setTitle:@"Select"forState:UIControlStateNormal];
        [dateBtn setTitle:@"Select" forState:UIControlStateNormal];
    }
    else{
        [self fillTheTF];
    }

}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (dataArray.count != 0) {
        return [[[dataArray objectAtIndex:0] objectForKey:@"lstdoc"] count];
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
        MyMeetingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"header"];
    cell.documentUploadHeaderLabel.text = [self convertHTML:[[[[dataArray objectAtIndex:0] objectForKey:@"lstdoc"] objectAtIndex: section] objectForKey:@"AgendaDescription"]];
    cell.backgroundColor = [UIColor whiteColor];
    if ([[[dataArray objectAtIndex:0] objectForKey:@"MeetingType"] isEqualToString:@"Planned"]) {
        if (section < 3) {
            cell.documentUploadHeaderBtn.hidden = YES;
        }
    }

    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[[[[dataArray objectAtIndex:0] objectForKey:@"lstdoc"] objectAtIndex: section] objectForKey:@"lstdoc"] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyMeetingTableViewCell *cell1= [tableView dequeueReusableCellWithIdentifier:@"files" forIndexPath:indexPath];;
    
    
    if ([[[[[[dataArray objectAtIndex:0] objectForKey:@"lstdoc"] objectAtIndex: indexPath.section] objectForKey:@"lstdoc"] objectAtIndex:indexPath.row] objectForKey:@"AgendaFile"] != [NSNull null]) {
        [cell1.documentRetrievalFileDownloadBtn setTitle:[[[[[[dataArray objectAtIndex:0] objectForKey:@"lstdoc"] objectAtIndex: indexPath.section] objectForKey:@"lstdoc"] objectAtIndex:indexPath.row] objectForKey:@"AgendaFile"] forState:UIControlStateNormal];
        cell1.documentRetrievalFileDownloadBtn.hidden = NO;
        cell1.textLabel.text = @"";
        cell1.userInteractionEnabled = YES;
    }
    else{
        cell1.documentRetrievalFileDownloadBtn.hidden = YES;
        cell1.textLabel.text = @"      -      ";
        cell1.userInteractionEnabled = NO;
    }
    cell1.backgroundColor = [UIColor clearColor];
    cell1.textLabel.textColor = [UIColor blackColor];
    return cell1;
}
- (IBAction)selectMeetingBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        sharedManager.passingMode = @"meetingListUpload";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
        [self presentViewController:myNavController animated:YES completion:nil];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)sectDateBtn:(id)sender {
    if (![meetingID isEqualToString:@""]) {
        if([Utitlity isConnectedTointernet]){
            sharedManager.passingMode = @"dateListUpload";
            sharedManager.passingId = meetingID;
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
        }else{
            [self showMsgAlert:NOInternetMessage];
        }
    }
    else{
        [self showMsgAlert:@"select the meeting"];
    }
}
-(void)showProgress{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}

-(void)hideProgress{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)fillTheTF{
    
    if ([sharedManager.passingMode isEqualToString: @"meetingListUpload"]) {
        //        sharedManager.passingMode = @"Category wise companies";
        if (sharedManager.passingString.length != 0) {
            [meetingBtn setTitle:sharedManager.passingString forState:UIControlStateNormal];
            [dateBtn setTitle:@"Select" forState:UIControlStateNormal];
            meetingID = sharedManager.passingId;
            agendaDocumentView.hidden = YES;
            MOMView.hidden = YES;
        }
    }
    
    if ([sharedManager.passingMode isEqualToString: @"dateListUpload"]) {
        //        sharedManager.passingMode = @"Industry wise companies";
        if (![meetingID isEqualToString:@""]) {
            if (sharedManager.passingString.length != 0) {
                [dateBtn setTitle:sharedManager.passingString forState:UIControlStateNormal];
                agendaDocumentView.hidden = NO;
                MOMView.hidden = NO;
                [self apiCall];
            }
        }
        else{
            [self showMsgAlert:@"Please select the meeting"];
        }
        
    }
    
}
-(void)apiCall{
    if([Utitlity isConnectedTointernet]){
        
        [self showProgress];
        NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@GetAgendaForallAdhocById?meetingid=%@&meetingdate=%@",BASEURL,meetingID,sharedManager.passingString]]];
        NSLog(@"urlRequest-----%@",urlRequest);
        
        //create the Method "GET"
        [urlRequest setHTTPMethod:@"GET"];
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                              if(httpResponse.statusCode == 200)
                                              {
                                                  NSError *parseError = nil;
                                                  dataArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                                                  NSLog(@"dataArray---%@",dataArray);
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [self hideProgress];
                                                      [dataTableView reloadData];
                                                      if ([[[dataArray objectAtIndex:0] objectForKey:@"IsAobUploaded"] isEqualToString:@"Yes"]) {
                                                          _addBtn.hidden = YES;
                                                          _tableViewHeightConstraint.constant = 10;
                                                      }
                                                      else{
                                                          _addBtn.hidden = NO;
                                                          _tableViewHeightConstraint.constant = 50;
                                                      }
                                                  });
                                              }
                                              else
                                              {
                                                  NSLog(@"Error");
                                              }
                                          }];
        [dataTask resume];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
    
}
- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
{
    [self hideProgress];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"nsurl===%@",url);
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Message"
                                              message:[NSString stringWithFormat:@"File downloaded successfully"]
                                              preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alertController animated:YES completion:nil];
    });
}
- (IBAction)documentsDownloadBtn:(UIButton *)sender {
    NSIndexPath *indexPath = [dataTableView indexPathForCell:(UITableViewCell *)sender.superview.superview];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Do you want to dowload or view the file" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *Download = [UIAlertAction actionWithTitle:@"Download" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                               {
                                   [self downloadFiles:indexPath];
                               }];
    UIAlertAction *viewFile = [UIAlertAction actionWithTitle:@"View" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                               {
                                   [self viewFile:indexPath];
                               }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:Download];
    [alert addAction:viewFile];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];

  
}
-(void)viewFile:(NSIndexPath*)indexPath{
    if([Utitlity isConnectedTointernet]){
        popOverView.hidden = NO;
        
        [MBProgressHUD showHUDAddedTo:webView animated:YES];
        NSString *pdfUrl ;
        pdfUrl =[[[[[[dataArray objectAtIndex:0] objectForKey:@"lstdoc"] objectAtIndex: indexPath.section] objectForKey:@"lstdoc"] objectAtIndex:indexPath.row] objectForKey:@"VirtualPath"];
    
        pdfUrl = [pdfUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:pdfUrl]];
        [webView loadRequest:request];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
    [MBProgressHUD hideHUDForView:webView animated:YES];
}
-(void)downloadFiles :(NSIndexPath*)indexPath{
    [self showProgress];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlToDownload ;
        NSString *type;
        urlToDownload = [NSString stringWithFormat:@"%@DownloadAllFile?mode=Agenda&fileName=%@&meetingid=%@",BASEURL,[[[[[[dataArray objectAtIndex:0] objectForKey:@"lstdoc"] objectAtIndex: indexPath.section] objectForKey:@"lstdoc"] objectAtIndex:indexPath.row] objectForKey:@"FileName"],meetingID];
        type=[[[[[[dataArray objectAtIndex:0] objectForKey:@"lstdoc"] objectAtIndex: indexPath.section] objectForKey:@"lstdoc"] objectAtIndex:indexPath.row] objectForKey:@"AgendaFile"];
        urlToDownload = [urlToDownload stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSLog(@"urlToDownload--------%@",urlToDownload);
        NSURL  *url = [NSURL URLWithString:urlToDownload];
        NSData *urlData = [NSData dataWithContentsOfURL:url];
        if ( urlData )
        {
            NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,type];
            
            //saving is done on main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                [self hideProgress];
                [urlData writeToFile:filePath atomically:YES];
                NSLog(@"filepath test n---------%@",filePath);
                
                NSLog(@"File Saved !");
                
                //Create the file path of the document to upload
                NSURL *filePathToUpload = [NSURL fileURLWithPath:filePath]  ;
                UIDocumentPickerViewController *docPicker = [[UIDocumentPickerViewController alloc] initWithURL:filePathToUpload inMode:UIDocumentPickerModeExportToService];
                NSLog(@"filepath---------%@",filePathToUpload);
                docPicker.delegate = self;
                [self presentViewController:docPicker animated:YES completion:nil];
            });
        }
    });
    
}

- (IBAction)UploadBtn:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:dataTableView];
    NSIndexPath *indexPath = [dataTableView indexPathForRowAtPoint:buttonPosition];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UploadFilesVC *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"UploadFilesVC"];
    myNavController.filesArray = [[[dataArray objectAtIndex:0] objectForKey:@"lstdoc"] objectAtIndex:indexPath.section];
    myNavController.meetingId = meetingID;
    myNavController.meetingDate = sharedManager.passingString;
    [self presentViewController:myNavController animated:YES completion:nil];
}
- (IBAction)addBtn:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UploadFilesVC *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"UploadFilesVC"];
    myNavController.meetingId = meetingID;
    myNavController.meetingDate = sharedManager.passingString;
    myNavController.filesArray = nil;
    [self presentViewController:myNavController animated:YES completion:nil];
}
-(NSString *)convertHTML:(NSString *)html {
    
    NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:html];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return html;
}
- (IBAction)popOverCloseBtn:(id)sender {
    popOverView.hidden = YES;
}

@end
