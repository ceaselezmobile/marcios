//
//  UploadFilesVC.m
//  test
//
//  Created by ceaselez on 18/05/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "UploadFilesVC.h"
#import "MySharedManager.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SearchViewController.h"
#import "MyMeetingTableViewCell.h"
#import "SWRevealViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/ALAssetRepresentation.h>
#import "AFHTTPSessionManager.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFHTTPRequestOperation.h"
@interface UploadFilesVC ()<UIDocumentPickerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;

@end

@implementation UploadFilesVC
{
    NSString *alertMessage;
    NSURL *filepath;
    NSData *fileData;
    UIImagePickerController *idPicker;
    NSMutableArray *filesArrays;
    NSMutableArray *deleteFilesArrays;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    deleteFilesArrays = [[NSMutableArray alloc] init];
    filesArrays = [[NSMutableArray alloc] init];
    if (_filesArray != nil) {
    filesArrays = [[_filesArray objectForKey:@"lstdoc"] mutableCopy];
        NSLog(@"filesArrays------%@",filesArrays);
    for (NSDictionary * person in filesArrays){
        if ([[person objectForKey:@"DocumentUploadId"] isEqualToString: @"00000000-0000-0000-0000-000000000000"])
            [filesArrays removeObject: person];
            }
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4+[filesArrays count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        if (indexPath.row == 0) {
            if (_filesArray != nil) {
            return [self heightForText:[_filesArray objectForKey:@"AgendaDescription"] withFont:[UIFont systemFontOfSize:14] andWidth:self.view.bounds.size.width-10]+60;
            }
            else{
                return 70;
            }
        }
    if (indexPath.row == 1) {
        return 70;
    }
    return 60;
}- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyMeetingTableViewCell *cell1;
    if (indexPath.row == 0) {
        cell1= [tableView dequeueReusableCellWithIdentifier:@"descripition" forIndexPath:indexPath];
        if (_filesArray == nil) {
            cell1.agendaDescripition.text = @"AOB";
        }
        else{
            cell1.agendaDescripition.text = [_filesArray objectForKey:@"AgendaDescription"];
        }
    }
    else if (indexPath.row == 1) {
        cell1= [tableView dequeueReusableCellWithIdentifier:@"duration" forIndexPath:indexPath];
        if (_filesArray == nil) {
            cell1.agendaDuration.text = @"0";
        }
        else{
            cell1.agendaDuration.text = [NSString stringWithFormat:@"%@",[_filesArray objectForKey:@"AgendaDuration"]];
        }

    }
    else if (indexPath.row == 2) {
        cell1= [tableView dequeueReusableCellWithIdentifier:@"uploadBtn" forIndexPath:indexPath];
    }
    else if ([filesArrays count] != 0 && indexPath.row < [filesArrays count]+3) {
        cell1= [tableView dequeueReusableCellWithIdentifier:@"files" forIndexPath:indexPath];
        cell1.agendaFiles.text = [[filesArrays objectAtIndex:indexPath.row-3] objectForKey:@"AgendaFile"];
    }
    else{
        cell1= [tableView dequeueReusableCellWithIdentifier:@"button" forIndexPath:indexPath];
    }
    return cell1;

}
- (IBAction)uploadBtn:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Take a photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        idPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:idPicker animated:YES completion:nil];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Choose from Libary" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSArray* types = @[(NSString*)kUTTypeImage,(NSString*)kUTTypeSpreadsheet,(NSString*)kUTTypePresentation,(NSString*)kUTTypeDatabase,(NSString*)kUTTypeFolder,(NSString*)kUTTypeZipArchive,(NSString*)kUTTypeVideo,(NSString*)kUTTypePDF,(__bridge NSString* ) kUTTypeContent,(__bridge NSString *) kUTTypeData,(__bridge NSString* ) kUTTypePackage,(__bridge NSString *) kUTTypeDiskImage,(NSString* )kUTTypeCompositeContent,@"com.apple.iwork.pages.pages",@"com.apple.iwork.numbers.numbers",@"com.apple.iwork.keynote.key"];
        UIDocumentPickerViewController *docPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:types inMode:UIDocumentPickerModeImport];
        docPicker.delegate = self;
        [self presentViewController:docPicker animated:YES completion:nil];
        
    }];
    
    
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}
- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
{
    if (controller.documentPickerMode == UIDocumentPickerModeImport)
    {
        fileData = [NSData dataWithContentsOfURL:url];
        filepath=url;
        alertMessage = [NSString stringWithFormat:@"%@", [url lastPathComponent]];
        NSDictionary *dic1 = @{@"FileName":alertMessage,@"fileData":fileData,@"filepath":filepath,@"AgendaFile":alertMessage};
        [filesArrays addObject:dic1];
        [_dataTableView reloadData];
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        
        UIImage* cameraImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
//        fileData = UIImageJPEGRepresentation(cameraImage, 0);
        NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];
        
        ALAssetsLibrary* assetsLibrary = [[ALAssetsLibrary alloc] init];
        
        [assetsLibrary writeImageToSavedPhotosAlbum:cameraImage.CGImage
                                           metadata:[info objectForKey:UIImagePickerControllerMediaMetadata]
                                    completionBlock:^(NSURL *assetURL, NSError *error) {
                                        
                                        if (!error) {
                                            
                                            NSLog(@"checkkk %@",[[NSString stringWithFormat:@"%@",assetURL] substringWithRange:NSMakeRange(77,3)]);
                                            NSLog(@"File======%@",assetURL.path);
                                            
                                            NSURL *Checkurl = [NSURL URLWithString:[[NSString stringWithFormat:@"file:///private/var/mobile/Containers/Data/Application/04E12545-4E21-47C3-ABE1-4CFE577CE6F1/tmp/%@.jpeg",[[NSString stringWithFormat:@"%@",assetURL] substringWithRange:NSMakeRange(36, 36)]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                                            NSLog(@"assetURL---------%@",assetURL);
                                            NSLog(@"assetURL test-------------%@",Checkurl);
                                            
                                            
                                            filepath=assetURL;
                                            alertMessage = [NSString stringWithFormat:@"%@", [Checkurl lastPathComponent]];
                                            NSDictionary *dic1 = @{@"FileName":alertMessage,@"fileData":fileData,@"filepath":filepath};
                                            [filesArrays addObject:dic1];
                                            [_dataTableView reloadData];
                                        }
                                    }];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)deleteBtn:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_dataTableView];
    NSIndexPath *indexPath = [_dataTableView indexPathForRowAtPoint:buttonPosition];
    if ([[filesArrays objectAtIndex:indexPath.row-3] objectForKey:@"AgendaUniqueId"]) {
        [deleteFilesArrays addObject:filesArrays[indexPath.row-3]];
    }
    [filesArrays removeObjectAtIndex:indexPath.row-3];
    [_dataTableView reloadData];
}

-(void)showProgress
{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:_dataTableView animated:YES];
}
-(void)hideProgress
{
    [MBProgressHUD hideHUDForView:_dataTableView animated:YES];
}
-(void)showMsgAlert:(NSString *)msg
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Action planner"
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)backBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil]; 
}
- (IBAction)saveBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        [self showProgress];
        [self imageupload];
    }
    else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)cancelBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(CGFloat)heightForText:(NSString*)text withFont:(UIFont *)font andWidth:(CGFloat)width
{
    CGSize constrainedSize = CGSizeMake(width, MAXFLOAT);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text attributes:attributesDictionary];
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    if (requiredHeight.size.width > width) {
        requiredHeight = CGRectMake(0,0,width, requiredHeight.size.height);
    }
    return requiredHeight.size.height;
}
-(void)imageupload
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"] forKey:@"CreatedBy"];
    [params setObject:_meetingId forKey:@"MeetingTypeID"];
    [params setObject:_meetingDate forKey:@"MeetingDate"];

    if (_filesArray != nil) {
        [params setObject:@"NO" forKey:@"isAob"];
        [params setObject:[_filesArray objectForKey:@"AgendaUniqueId"] forKey:@"ReferenceId"];
    }
//    else if([[_filesArray objectForKey:@"AgendaUniqueId"] isEqualToString: @"00000000-0000-0000-0000-000000000000"]){
//        [params setObject:@"YES" forKey:@"isAob"];
//    }
    else{
        [params setObject:@"YES" forKey:@"isAob"];
    }

    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (int i = 0 ; i<deleteFilesArrays.count; i++) {
        NSDictionary*dits;
            dits = @{@"ID":[[deleteFilesArrays objectAtIndex:i] objectForKey:@"DocumentUploadId"],@"IsDeleted":@"True"};
        [arr addObject:dits];
    }
    NSError * errortest;
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:arr options:0 error:&errortest];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    [params setObject:jsonString forKey:@"lstDocuments"];

    NSLog(@"dict-------%@",params);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"contentType"];
    AFHTTPRequestOperation *op =[manager POST:[NSString stringWithFormat:@"%@POSTUploadMeetingAgendaDocuments",BASEURL] parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (int i = 0; i<filesArrays.count; i++) {
            if (![[filesArrays objectAtIndex:i] objectForKey:@"AgendaUniqueId"]) {
                [formData appendPartWithFileData:[[filesArrays objectAtIndex:i] objectForKey:@"fileData"] name:@"files" fileName:[[filesArrays objectAtIndex:i] objectForKey:@"FileName"] mimeType:[[[filesArrays objectAtIndex:i] objectForKey:@"FileName"] pathExtension]];
            }
        }
        
    }
                                      success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
                                          NSLog(@"%@",responseObject);
                                          [self hideProgress];
                                          [self dismissViewControllerAnimated:YES completion:nil];
                                      } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
                                          NSString *ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
                                          
                                          NSData *data1 = [ErrorResponse dataUsingEncoding:NSUTF8StringEncoding];
                                          id json = [NSJSONSerialization JSONObjectWithData:data1 options:0 error:nil];
                                          [self hideProgress];
                                          [self dismissViewControllerAnimated:YES completion:nil];
                                          NSLog(@"%@",json);
                                      }];
    [op start];
}
@end
