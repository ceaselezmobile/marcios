//
//  MOMApprovalVC.m
//  test
//
//  Created by ceaselez on 30/05/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "MOMApprovalVC.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "MeetingDetailsTableViewCell.h"
#import "MySharedManager.h"
#import "MeetingDetailsViewController.h"

@interface MOMApprovalVC ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *popOverView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSArray *searchResult;
@end

@implementation MOMApprovalVC
{
    UIRefreshControl *refreshControl;
    MySharedManager *sharedManager;
    NSArray *meetingList;
    BOOL searchEnabled;
    NSString *_pdfUrl;
    NSIndexPath* selectedIndex;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _popOverView.hidden = YES;
    sharedManager = [MySharedManager sharedManager];
    meetingList = [[NSArray alloc] init];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [_menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    refreshControl = [[UIRefreshControl alloc]init];
    [self.dataTableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(loadDataFromApi) forControlEvents:UIControlEventValueChanged];
}
-(void) viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self searchBarCancelButtonClicked:_searchBar];
    [self loadDataFromApi];
}
-(void)viewDidAppear:(BOOL)animated{
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        sharedManager.slideMenuSlected = @"no";
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (meetingList.count == 0) {
        return 1;
    }
    if (searchEnabled) {
        if (_searchResult.count == 0) {
            return 1;
        }
        return [self.searchResult count];
    }
    else{
        return meetingList.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MeetingDetailsTableViewCell *cell;
    if (meetingList.count == 0) {
        UITableViewCell *cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SimpleTableItem"];
        cell1.textLabel.text = @"No Meetings Found";
        cell1.backgroundColor = [UIColor clearColor];
        cell1.textLabel.textColor = [UIColor blackColor];
        return cell1;
    }
    else if (searchEnabled) {
        if (_searchResult.count == 0) {
            UITableViewCell *cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SimpleTableItem"];
            cell1.textLabel.text = @"No Meetings Found";
            cell1.backgroundColor = [UIColor clearColor];
            cell1.textLabel.textColor = [UIColor blackColor];
            return cell1;
        }
        else
        {
            cell= [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            cell.calenderCountryLabel.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"MeetingPlace"];
            cell.calenderFrequencyLabel.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"StartTime"];
            cell.calenderMeetingDescrictionTV.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"MeetingDescription"];
            cell.calenderMeetingDateLabel.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"MeetingDate"];
            cell.calenderGroupLabel.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"Group"];
            cell.calenderGroupCategoryLabel.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"Category"];
            return cell;
            
        }
    }
    else{
        cell= [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.calenderCountryLabel.text = [[meetingList objectAtIndex: indexPath.row] objectForKey:@"MeetingPlace"];
        cell.calenderFrequencyLabel.text = [[meetingList objectAtIndex: indexPath.row] objectForKey:@"StartTime"];
        cell.calenderMeetingDescrictionTV.text = [[meetingList objectAtIndex: indexPath.row] objectForKey:@"MeetingDescription"];
        cell.calenderMeetingDateLabel.text = [[meetingList objectAtIndex: indexPath.row] objectForKey:@"MeetingDate"];
        cell.calenderGroupLabel.text = [[meetingList objectAtIndex: indexPath.row] objectForKey:@"Group"];
        cell.calenderGroupCategoryLabel.text = [[meetingList objectAtIndex: indexPath.row] objectForKey:@"Category"];
        return cell;
        
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 205;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    MeetingDetailsViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"MeetingDetailsViewController"];
//    if (searchEnabled) {
//        sharedManager.passingId = [[_searchResult objectAtIndex:indexPath.row] objectForKey:@"MeetingTypeID"];
//         sharedManager.passingString = [[_searchResult objectAtIndex:indexPath.row] objectForKey:@"MeetingDate"];
//    }
//    else{
//        sharedManager.passingId = [[meetingList objectAtIndex:indexPath.row] objectForKey:@"MeetingTypeID"];
//         sharedManager.passingString= [[meetingList objectAtIndex:indexPath.row] objectForKey:@"MeetingDate"];
//    }
//  
//    myNavController.update = YES;
//    [self presentViewController:myNavController animated:YES completion:nil];
}
-(void)loadDataFromApi{
    if([Utitlity isConnectedTointernet]){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [self showProgress];
        NSString *targetUrl = [NSString stringWithFormat:@"%@GetPublishMeetingsForMOMApproveList?userid=%@", BASEURL,[defaults objectForKey:@"UserID"]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:targetUrl]];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
          ^(NSData * _Nullable data,
            NSURLResponse * _Nullable response,
            NSError * _Nullable error) {
              meetingList = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
              NSLog(@"%@",meetingList);
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self hideProgress];
                  [refreshControl endRefreshing];
                  [_dataTableView reloadData];
              });
              
          }] resume];
        
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)showProgress{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:_dataTableView animated:YES];
    
}

-(void)hideProgress{
    [MBProgressHUD hideHUDForView:_dataTableView animated:YES];
}
-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)search:(id)sender {
    _searchView.hidden = NO;
    [_searchBar  becomeFirstResponder];
}
- (IBAction)searchBackBtn:(id)sender {
    [self searchBarCancelButtonClicked:_searchBar];
}
- (void)filterContentForSearchText:(NSString*)searchText
{
    _searchResult = [meetingList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(MeetingDescription contains[c] %@) OR (CountryName contains[c] %@) OR (FrequencyDescription contains[c] %@) OR (MeetingDate contains[c] %@)", searchText, searchText, searchText, searchText]];
    [_dataTableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[searchBar.text stringByTrimmingCharactersInSet: set] length] == 0) {
        searchEnabled = NO;
        [_dataTableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[searchBar.text stringByTrimmingCharactersInSet: set] length] == 0) {
        searchEnabled = NO;
        [self.dataTableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    searchEnabled = NO;
    [_dataTableView reloadData];
    _searchView.hidden = YES;
    
}
- (IBAction)deleteBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        [self showProgress];
        NSLog(@"%@",_pdfUrl);
        _popOverView.hidden = NO;

        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.dataTableView];
        NSIndexPath *indexPath = [self.dataTableView indexPathForRowAtPoint:buttonPosition];
        selectedIndex = indexPath;
        if (searchEnabled) {
            _pdfUrl = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"PDFName"];
        }
        else{
            _pdfUrl = [[meetingList objectAtIndex: indexPath.row] objectForKey:@"PDFName"];

        }
        _pdfUrl = [_pdfUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:_pdfUrl]];
        [_webView loadRequest:request];
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
    [self hideProgress];
}
- (IBAction)approveBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
        [params setObject:[[meetingList objectAtIndex:selectedIndex.row] objectForKey:@"MeetingTypeID"]  forKey:@"MeetingID"];
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"dd/MM/yyy"];
//        NSDate *date = [formatter dateFromString:[[meetingList objectAtIndex:selectedIndex.row] objectForKey:@"MeetingDate"]];
//        [formatter setDateFormat:@"yyyy-MM-dd"];
//        NSString *output = [formatter stringFromDate:date];
        [params setObject:[[meetingList objectAtIndex:selectedIndex.row] objectForKey:@"MeetingDate"]  forKey:@"MeetingDate"];
        [params setObject:@"YES"  forKey:@"Approval"];
        
        [self showProgress];
        NSString* JsonString = [Utitlity JSONStringConv: params];
        NSLog(@"jsonstring-----%@",JsonString);
        [[WebServices sharedInstance]apiAuthwithJSON:@"POSTApproveMOMofCompliance" HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
            
            NSString *error=[json valueForKey:@"error"];
            [self hideProgress];
            
            if(error.length>0){
                [self showMsgAlert:[json valueForKey:@"error_description"]];
                return ;
            }else{
                [self showMsgAlert:@"MOM Approved successfully"];
                NSLog(@"json-----%@",json);
                _popOverView.hidden = YES;
                [self loadDataFromApi];
            }
        }];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }

}
- (IBAction)cancelBtn:(id)sender {
    _popOverView.hidden = YES;
}
@end
