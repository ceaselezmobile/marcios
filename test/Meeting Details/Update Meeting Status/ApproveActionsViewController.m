
#import "ApproveActionsViewController.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "MeetingDetailsTableViewCell.h"
#import "MySharedManager.h"
#import "CustomTableViewCell.h"
#import "MeetingActionDetailsViewController.h"
@interface ApproveActionsViewController ()
@property (weak, nonatomic) IBOutlet UIButton *internalBtn;
@property (weak, nonatomic) IBOutlet UIButton *externalBtn;
@property (weak, nonatomic) IBOutlet UIView *internalBtnView;
@property (weak, nonatomic) IBOutlet UIView *externalBtnView;
@property (weak, nonatomic) IBOutlet UILabel *tittleLabel;
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;
@property (weak, nonatomic) IBOutlet UIView *searchBarView;
@property (nonatomic, strong) NSArray *searchResult;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *popOverView;
@property (weak, nonatomic) IBOutlet UITableView *popOverTableView;


@end

@implementation ApproveActionsViewController
{
    BOOL type;
    NSMutableArray *actionProgressDict;
    MySharedManager *sharedManager;
    UIRefreshControl *refreshControl;
    NSString *actionStatus;
    
    __weak IBOutlet UIButton *menuBtn;
    BOOL searchEnabled;
    __weak IBOutlet UIButton *actionStatusBtn;
    NSDate * curDate;
    NSDateFormatter * formatter;
    NSString *textViewText;
    __weak IBOutlet NSLayoutConstraint *popOverTableViewHeight;
    long editButtonIndex;
    NSString *approveString;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    textViewText = @"";
    _popOverView.hidden = YES;
    sharedManager = [MySharedManager sharedManager];
    _popOverTableView.rowHeight = UITableViewAutomaticDimension;
    _popOverTableView.estimatedRowHeight=74;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    actionProgressDict = [[NSMutableArray alloc] init];
    
    
    _internalBtnView.hidden = NO;
    _externalBtnView.hidden = YES;
    _searchBarView.hidden = YES;
    _tittleLabel.text = @"Critical Approve Actions";
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [_dataTableView addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self  action:@selector(didSwipe:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [_dataTableView addGestureRecognizer:swipeRight];
    actionStatus = @"dft";
    [self loadDataFromApi];
    refreshControl = [[UIRefreshControl alloc]init];
    [self.dataTableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(loadDataFromApi) forControlEvents:UIControlEventValueChanged];
    curDate = [NSDate date];
    formatter = [[NSDateFormatter alloc] init];
}
-(void) viewWillAppear:(BOOL)animated{
    
    [formatter setDateFormat:@"dd-MM-yyyy"];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        actionStatus = @"dft";
    }
    [self loadDataFromApi];
}
-(void)viewDidAppear:(BOOL)animated{
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        sharedManager.slideMenuSlected = @"no";
    }
}
- (void)didSwipe:(UISwipeGestureRecognizer*)swipe{
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        [self externalBtnClicked];
        
    } else if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
        [self internalBtnClicked];
        
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _popOverTableView) {
        return 3;
    }
    else{
        if (searchEnabled) {
            if (_searchResult.count == 0) {
                return 1;
            }
            return [self.searchResult count];
        }
        return actionProgressDict.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _popOverTableView) {
        CustomTableViewCell *cell;
        if (indexPath.row == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Completion Date" forIndexPath:indexPath];
            cell.updateCompletionDateTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            cell.updateCompletionDateTF.text = [formatter stringFromDate:curDate];
            if ([approveString isEqualToString:@"Disapprove"]) {
                cell.popOverTopicLabel.text = @"Disapprove Action";
                cell.popOverContentLabel.text = @"Do you really want to disapprove this action ?";
            }
            else{
                cell.popOverTopicLabel.text = @"Approve Action";
                cell.popOverContentLabel.text = @"Do you really want to approve this action ?";
            }
        }
        if (indexPath.row == 1) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Remarks" forIndexPath:indexPath];
            cell.updateRemaksTV.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            cell.updateRemaksTV.text = textViewText;
        }
        else if (indexPath.row == 2) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Button" forIndexPath:indexPath];
            
        }
        return cell;
        
    }
    else{
        MeetingDetailsTableViewCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:@"updateList"];
        if (searchEnabled) {
            if (_searchResult.count == 0) {
                UITableViewCell *cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SimpleTableItem"];
                cell1.textLabel.text = @"Approved Actions are not found";
                cell1.backgroundColor = [UIColor clearColor];
                cell1.textLabel.textColor = [UIColor blackColor];
                cell1.userInteractionEnabled  = NO;
                return cell1;
            }
            cell.updateMeetingDateLabel.text = [[_searchResult objectAtIndex:indexPath.row] objectForKey:@"TargetDate"];
            cell.updateMeetingDescriptionLabel.text = [[_searchResult objectAtIndex:indexPath.row] objectForKey:@"MeetingDescription"];
            cell.updateResponsibleLabel.text = [[_searchResult objectAtIndex:indexPath.row] objectForKey:@"EmpName"];
            cell.updateActionTypeLabel.text = [[_searchResult objectAtIndex:indexPath.row] objectForKey:@"ActionDescription"];
        }
        else{
            cell.updateMeetingDateLabel.text = [[actionProgressDict objectAtIndex:indexPath.row] objectForKey:@"TargetDate"];
            cell.updateMeetingDescriptionLabel.text = [[actionProgressDict objectAtIndex:indexPath.row] objectForKey:@"MeetingDescription"];
            cell.updateResponsibleLabel.text = [[actionProgressDict objectAtIndex:indexPath.row] objectForKey:@"EmpName"];
            cell.updateActionTypeLabel.text = [[actionProgressDict objectAtIndex:indexPath.row] objectForKey:@"ActionDescription"];
            
            
        }
        return cell;
        
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _popOverTableView) {
        if (indexPath.row == 1) {
            if (185 + [self heightForText:textViewText withFont:[UIFont systemFontOfSize:14] andWidth:190] < 350) {
                popOverTableViewHeight.constant = 185 + [self heightForText:textViewText withFont:[UIFont systemFontOfSize:14] andWidth:203];
                return [self heightForText:textViewText withFont:[UIFont systemFontOfSize:14] andWidth:203]+55;
            }
            else{
                popOverTableViewHeight.constant = 350;
                return 240;
            }        }
        if (indexPath.row == 0) {
            return 74;
        }
        return 44;
    }
    return 145;
}-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _dataTableView) {
        if (searchEnabled) {
            sharedManager.contactArray = [_searchResult objectAtIndex:indexPath.row];
        }
        else{
            sharedManager.contactArray = [actionProgressDict objectAtIndex:indexPath.row];
        }
        sharedManager.passingId=@"Approve Actions";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MeetingActionDetailsViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"MeetingActionDetailsViewController"];
        [self presentViewController:myNavController animated:YES completion:nil];
    }
    
    
}

- (IBAction)internalBtn:(id)sender {
    [self internalBtnClicked];
}
- (IBAction)externalBtn:(id)sender {
    [self externalBtnClicked];
    
}
-(void)internalBtnClicked{
    if (type) {
        [self searchBarCancelButtonClicked:_searchBar];
        
        type = NO;
        _tittleLabel.text = @"Critical Approve Actions";
        _internalBtnView.hidden = NO;
        _externalBtnView.hidden = YES;
        [self loadDataFromApi];
    }
    
}
-(void)externalBtnClicked{
    if (!type) {
        [self searchBarCancelButtonClicked:_searchBar];
        
        _tittleLabel.text = @"Normal Approve Actions";
        _internalBtnView.hidden = YES;
        _externalBtnView.hidden = NO;
        type = YES;
        [self loadDataFromApi];
    }
    
}
-(void)loadDataFromApi{
    if([Utitlity isConnectedTointernet]){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [self showProgress];
        NSString *actionType;
        if (type) {
            actionType = @"Normal";
        }
        else{
            actionType = @"Critical";
        }
        NSString *targetUrl = [NSString stringWithFormat:@"%@GetMeetingActionProgressVerificationByType?ActionTyp=%@&userid=%@", BASEURL,actionType,[defaults objectForKey:@"UserID"]];
        NSLog(@"targetUrl------%@",targetUrl);

        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:targetUrl]];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
          ^(NSData * _Nullable data,
            NSURLResponse * _Nullable response,
            NSError * _Nullable error) {
              actionProgressDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
              NSLog(@"%@",actionProgressDict);
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self hideProgress];
                  if (actionProgressDict.count == 0) {
                      _dataTableView.hidden = YES;
                  }
                  else{
                      _dataTableView.hidden = NO;
                  }
                  [refreshControl endRefreshing];
                  [_dataTableView reloadData];
              });
              
          }] resume];
        
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)showProgress{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:_dataTableView animated:YES];
    
}

-(void)hideProgress{
    [MBProgressHUD hideHUDForView:_dataTableView animated:YES];
}
-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)searchBtn:(id)sender {
    _searchBarView.hidden = NO;
    [_searchBar  becomeFirstResponder];
}
- (IBAction)hideSearchBarView:(id)sender {
    [self searchBarCancelButtonClicked:_searchBar];
    _searchBarView.hidden = YES;
}

- (void)filterContentForSearchText:(NSString*)searchText
{
    
    _searchResult = [actionProgressDict filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(ActionDescription contains[c] %@) OR (TargetDate contains[c] %@) OR (EmpName contains[c] %@) OR (MeetingDate contains[c] %@)", searchText, searchText, searchText, searchText]];
    
    [_dataTableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[searchBar.text stringByTrimmingCharactersInSet: set] length] == 0) {
        searchEnabled = NO;
        [_dataTableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[searchBar.text stringByTrimmingCharactersInSet: set] length] == 0) {
        searchEnabled = NO;
        [self.dataTableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    searchEnabled = NO;
    [_dataTableView reloadData];
    _searchBarView.hidden = YES;
    
}
- (IBAction)editBtn:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.popOverTableView];
    NSIndexPath *indexPath = [self.popOverTableView indexPathForRowAtPoint:buttonPosition];
    editButtonIndex = indexPath.row;
    approveString = @"Approve";
    [self editAction];
}
- (IBAction)editDisapproveBtn:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.popOverTableView];
    NSIndexPath *indexPath = [self.popOverTableView indexPathForRowAtPoint:buttonPosition];
    editButtonIndex = indexPath.row;
    approveString = @"Disapprove";
    [self editAction];
}
-(void)editAction{
    _popOverView.hidden = NO;
    _popOverTableView.scrollEnabled=YES;
    textViewText = @"";
    curDate = nil;
    [_popOverTableView reloadData];
}
- (IBAction)popCancelBtn:(id)sender {
    _popOverView.hidden = YES;
}
- (IBAction)popDoneBtn:(id)sender {
    
        
//        if(curDate == nil){
//            msg = @"Please select the complition date";
//        }
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        if ([[textViewText stringByTrimmingCharactersInSet: set] length] == 0){
            [self showMsgAlert:@"Please enter the Comments."];
        }
        else{
            if([Utitlity isConnectedTointernet]){
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
                [params setObject:[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"MeetingTypeID"] forKey:@"MeetingTypeID"];
                [params setObject:[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"ActionID"] forKey:@"ActionID"];
                NSString *dateString =  [[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"MeetingDate"];
                [formatter setDateFormat:@"dd/MM/yyyy"];
                NSDate *date = [formatter dateFromString:dateString];
                [formatter setDateFormat:@"yyyy-MM-dd"];
                [params setObject:[formatter stringFromDate:date] forKey:@"MeetingDate"];
                [params setObject:[defaults objectForKey:@"UserID"] forKey:@"VerifiedBy"];
                [params setObject:textViewText forKey:@"VerifiedRemarks"];
                [params setObject:approveString forKey:@"Mode"];

                [self showProgress];
                NSString* JsonString = [Utitlity JSONStringConv: params];
                NSLog(@"json----%@",JsonString);
                
                [[WebServices sharedInstance]apiAuthwithJSON:PostDisapproveActionProgress HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
                    
                    NSString *error=[json valueForKey:@"error"];
                    [self hideProgress];
                    
                    if(error.length>0){
                        [self showMsgAlert:[json valueForKey:@"error_description"]];
                        return ;
                    }else{
                        if(json.count==0){
                            [self showMsgAlert:@"Error , Try Again"];
                        }else{
                            NSLog(@"json-----%@",json);
                            _popOverView.hidden = YES;
                            [self showMsgAlert:@"Done! saved successfully"];
                            [self loadDataFromApi];
                            
                        }
                    }
                    
                }];
            }
            else{
                [self showMsgAlert:NOInternetMessage];
            }
        }
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    [self updateTextLabelsWithText: newString];
    return YES;
}
-(void)updateTextLabelsWithText:(NSString *)string
{
    if (![string isEqualToString:@""]) {
        textViewText = string;
    }
    
}
-(CGFloat)heightForText:(NSString*)text withFont:(UIFont *)font andWidth:(CGFloat)width
{
    CGSize constrainedSize = CGSizeMake(width, MAXFLOAT);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text attributes:attributesDictionary];
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    if (requiredHeight.size.width > width) {
        requiredHeight = CGRectMake(0,0,width, requiredHeight.size.height);
    }
    return requiredHeight.size.height;
}
- (void)textViewDidChange:(UITextView *)textView
{
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    if (185 + [self heightForText:textViewText withFont:[UIFont systemFontOfSize:14] andWidth:190] < 300) {
        [_popOverTableView beginUpdates];
        textView.frame = newFrame;
        [_popOverTableView endUpdates];
    }
    
}

@end
