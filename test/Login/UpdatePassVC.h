//
//  UpdatePassVC.h
//  test
//
//  Created by ceazeles on 27/02/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"
@interface UpdatePassVC : UIViewController
@property (weak, nonatomic) IBOutlet ACFloatingTextField *currentpwtxtfld;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *newpwtxtfld;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *cnfrmpwtxtfld;

@end
