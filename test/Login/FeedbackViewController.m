//
//  FeedbackViewController.m
//  test
//
//  Created by ceaselez on 08/03/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "FeedbackViewController.h"
#import "ProfileTableViewCell.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"

@interface FeedbackViewController ()
@property (weak, nonatomic) IBOutlet UITableView *dataTableview;

@end

@implementation FeedbackViewController
{
    NSString *nameString;
    NSString *phoneString;
    NSString *textViewText;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    textViewText = @"";
}
- (IBAction)Backbtn:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 || indexPath.row == 1) {
        return 80;
    }
    else if(indexPath.row == 2){
        if ([self heightForText:textViewText withFont:[UIFont systemFontOfSize:14] andWidth:345] >76) {
            return [self heightForText:textViewText withFont:[UIFont systemFontOfSize:14] andWidth:345]+76;
        }
        else{
            return 120;
        }
    }
    else
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProfileTableViewCell *cell;
    if (indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"name" forIndexPath:indexPath];
        cell.feedbackNameTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        cell.feedbackNameTF.text = nameString;
    }
    else if (indexPath.row == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"phone" forIndexPath:indexPath];
        cell.feedbackPhoneTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        cell.feedbackPhoneTF.text = phoneString;
    }
    else if (indexPath.row == 2) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"feedback" forIndexPath:indexPath];
        cell.feedbackDescriptionTV.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        [cell.feedbackDescriptionTV setText:textViewText];
    }
    else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"button" forIndexPath:indexPath];
    }
    return cell;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    textViewText = textView.text;
    return YES;
}

-(CGFloat)heightForText:(NSString*)text withFont:(UIFont *)font andWidth:(CGFloat)width
{
    CGSize constrainedSize = CGSizeMake(width, MAXFLOAT);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text attributes:attributesDictionary];
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    if (requiredHeight.size.width > width) {
        requiredHeight = CGRectMake(0,0,width, requiredHeight.size.height);
    }
    return requiredHeight.size.height;
}
- (void)textViewDidChange:(UITextView *)textView
{
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    NSLog(@"newSize.height-------%f",newSize.height);
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    NSLog(@"new===========%f",[self heightForText:textViewText withFont:[UIFont systemFontOfSize:14] andWidth:self.view.bounds.size.width-20]);

        if ([self heightForText:textViewText withFont:[UIFont systemFontOfSize:14] andWidth:345] >76) {
    [_dataTableview beginUpdates];
    textView.frame=newFrame;
    [_dataTableview endUpdates];
        }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSIndexPath *ip;
    ProfileTableViewCell *cell;
    ip = [NSIndexPath indexPathForRow:0 inSection:0];
    cell = [_dataTableview cellForRowAtIndexPath:ip];
    if (textField == cell.feedbackNameTF) {
        nameString  = [textField.text stringByReplacingCharactersInRange:range withString:string];
    }
    ip = [NSIndexPath indexPathForRow:1 inSection:0];
    cell = [_dataTableview cellForRowAtIndexPath:ip];
    if (textField == cell.feedbackPhoneTF) {
        phoneString  = [textField.text stringByReplacingCharactersInRange:range withString:string];
    }
return YES;
}

- (IBAction)submitBtn:(id)sender {
    if([Utitlity isConnectedTointernet])
    {
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        if(nameString.length == 0 )
        {
            [self showMsgAlert:@"Please enter your name"];
        }
        else if ( phoneString.length < 10 || phoneString.length >15 ) {
            [self showMsgAlert:@"Please enter your phone number"];
        }
        else if ([[textViewText stringByTrimmingCharactersInSet: set] length] == 0){
            [self showMsgAlert:@"Please enter your feedback"];
        }
        else
        {
            NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
            [params setObject:[[NSUserDefaults standardUserDefaults]valueForKey:@"UserID"] forKey:@"UserId"];
            [params setObject:nameString forKey:@"Name"];
            [params setObject:phoneString forKey:@"PhoneNumber"];
            [params setObject:textViewText forKey:@"Comment"];
            [self showProgress];
            NSString* JsonString = [Utitlity JSONStringConv: params];
            NSLog(@"json----%@",JsonString);
            [[WebServices sharedInstance]apiAuthwithJSON:@"PostSaveFeedBack" HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
                NSString *error=[json valueForKey:@"error"];
                [self hideProgress];
                if(error.length>0)
                {
                    [self showMsgAlert:[json valueForKey:@"error_description"]];
                    return ;
                }
                else
                {
                            NSLog(@"json-----%@",json);
                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                                          message:[json objectForKey:@"SaveFeedBackFromMobileResult"]preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK"
                                                                                style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction * action)
                                                        {
                                                            [self dismissViewControllerAnimated:YES completion:nil];
                                                        }];
                            [alert addAction:yesButton];
                            [self presentViewController:alert animated:YES completion:nil];
                    }
                
            }];
        }
    }
    else
    {
        [self showMsgAlert:NOInternetMessage];
    }
    
}
-(void)showProgress
{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)hideProgress
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
-(void)showMsgAlert:(NSString *)msg
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@""
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
