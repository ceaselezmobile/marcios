
#import "UpdatePassVC.h"
#import "SWRevealViewController.h"
#import "UpdatePassVC.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
@interface UpdatePassVC ()

@end

@implementation UpdatePassVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    self.newpwtxtfld.text = @"";
    self.cnfrmpwtxtfld.text = @"";
    self.currentpwtxtfld.text = @"";
}
- (IBAction)backbtn:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)Changepassbtn:(UIButton *)sender {
    if([Utitlity isConnectedTointernet])
    {
        if(self.currentpwtxtfld.text.length == 0 )
        {
            [self showMsgAlert:@"Please enter the current password"];
        }
        else if(self.newpwtxtfld.text.length == 0 )
        {
            [self showMsgAlert:@"Please enter the new password"];
        }
        else if(self.cnfrmpwtxtfld.text.length == 0 )
        {
            [self showMsgAlert:@"Please enter the confirm password"];
        }
        
        else if(![self.newpwtxtfld.text isEqualToString:self.cnfrmpwtxtfld.text])
        {
            [self showMsgAlert:@"New Password not matching with Confirm Password"];
        }
        if(![self.newpwtxtfld.text isEqualToString:self.cnfrmpwtxtfld.text])
        {
            [self showMsgAlert:@"New Password not matching with Confirm Password"];
        }
        else
        {
            NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
            [params setObject:[[NSUserDefaults standardUserDefaults]valueForKey:@"EmailID"] forKey:@"EmailID"];
            [params setObject:self.currentpwtxtfld.text forKey:@"CurrPassword"];
            [params setObject:self.cnfrmpwtxtfld.text forKey:@"NewPassword"];
            [self showProgress];
            NSString* JsonString = [Utitlity JSONStringConv: params];
            NSLog(@"json----%@",JsonString);
            [[WebServices sharedInstance]apiAuthwithJSON:PostChangePassword HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
                NSString *error=[json valueForKey:@"error"];
                [self hideProgress];
                if(error.length>0)
                {
                    [self showMsgAlert:[json valueForKey:@"error_description"]];
                    return ;
                }
                else
                {
                    if(json.count==0)
                    {
                        [self showMsgAlert:@"Error , Try Again"];
                    }else
                    {
                        if ([[json objectForKey:@"ChangePasswordResult"] isEqualToString:@"Password Changed Successfully."])
                        {
                            NSLog(@"json-----%@",json);
                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                                          message:[json objectForKey:@"ChangePasswordResult"]preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK"
                                                                                style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction * action)
                                                        {
                                                            SWRevealViewController *purchaseContr = (SWRevealViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                                                            purchaseContr.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                                                            [self presentViewController:purchaseContr animated:YES completion:nil];
                                                        }];
                            [alert addAction:yesButton];
                            [self presentViewController:alert animated:YES completion:nil];
                        }
                        else
                        {
                            [self showMsgAlert:[json objectForKey:@"ChangePasswordResult"]];
                        }
                    }
                }
                
            }];
        }
    }
    else
    {
        [self showMsgAlert:NOInternetMessage];
    }
    
}
-(void)showMsgAlert:(NSString *)msg
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@""
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)showProgress
{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)hideProgress
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
- (IBAction)Securetxtbtn:(UIButton *)sender {
    if(sender.tag==1)
    {
        if (self.currentpwtxtfld.secureTextEntry == YES) {
            self.currentpwtxtfld.secureTextEntry = NO;
            [sender setSelected:YES];
        }
        else
        {
            self.currentpwtxtfld.secureTextEntry = YES;
            [sender setSelected:NO];
        }
    }
    else if (sender.tag==2)
    {
        if (self.newpwtxtfld.secureTextEntry == YES) {
            self.newpwtxtfld.secureTextEntry = NO;
            [sender setSelected:YES];
        }
        else
        {
            self.newpwtxtfld.secureTextEntry = YES;
            [sender setSelected:NO];
        }
    }
    else if (sender.tag==3)
    {
        if (self.cnfrmpwtxtfld.secureTextEntry == YES) {
            self.cnfrmpwtxtfld.secureTextEntry = NO;
            [sender setSelected:YES];
        }
        else
        {
            self.cnfrmpwtxtfld.secureTextEntry = YES;
            [sender setSelected:NO];
        }
    }
}
@end

