//
//  SettingsVC.h
//  test
//
//  Created by ceazeles on 02/03/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AboutVC.h"
#import "UpdatePassVC.h"
#import "FeedbackViewController.h"
@interface SettingsVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *SettingsTable;
@property (nonatomic, strong) AboutVC *AboutVC;
@property (nonatomic, strong) UpdatePassVC *UpdatePassVC;
@property (nonatomic, strong) FeedbackViewController *FeedbackViewController;



@end
