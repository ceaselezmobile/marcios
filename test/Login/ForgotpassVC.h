//
//  ForgotpassVC.h
//  test
//
//  Created by ceazeles on 27/02/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"

@interface ForgotpassVC : UIViewController
@property (weak, nonatomic) IBOutlet ACFloatingTextField *emailtxtfld;
@property (weak, nonatomic) IBOutlet UIView *headView;

@end

