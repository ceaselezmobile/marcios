//
//  AboutVC.h
//  test
//
//  Created by ceazeles on 02/03/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutVC : UIViewController
@property (strong ,nonatomic)NSString *str;
@property (weak, nonatomic) IBOutlet UILabel *headertxtlbl;
@property (weak, nonatomic) IBOutlet UIWebView *webview;

@end
