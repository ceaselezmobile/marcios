
#import "ViewController.h"
#import "ACFloatingTextField.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "ForgotpassVC.h"

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *pastemails,*autocompletemail;
}

@property (weak, nonatomic) IBOutlet ACFloatingTextField *emailTF;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *passwordTF;
@property (weak, nonatomic) IBOutlet UITableView *autocompleteTableView;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    autocompletemail=[NSMutableArray new];
    //    pastemails=[NSMutableArray new];
    pastemails=[[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:@"autocomplete"]];
    //    [pastemails addObject:@"srilakshmi.k@ceaselez.com"];
    //    autocompleteTableView = [[UITableView alloc] initWithFrame:
    //                             CGRectMake(0, self.emailTF.frame.origin.y+self.emailTF.frame.size.height+5, self.view.frame.size.width, 120) style:UITableViewStylePlain];
    _autocompleteTableView.delegate = self;
    _autocompleteTableView.dataSource = self;
    _autocompleteTableView.scrollEnabled = YES;
    _autocompleteTableView.hidden = YES;
    [self.view addSubview:_autocompleteTableView];
   
}
-(void)viewWillAppear:(BOOL)animated
{
    _autocompleteTableView.hidden = YES;
}
- (IBAction)loginBtn:(id)sender
{
    if (_emailTF.text.length != 0) {
        [pastemails addObject:_emailTF.text];
        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:pastemails];
        NSArray *arrayWithoutDuplicates = [orderedSet array];
        NSLog(@"AutoComplet Arr : %@",arrayWithoutDuplicates);
        [[NSUserDefaults standardUserDefaults] setObject:arrayWithoutDuplicates forKey:@"autocomplete"];
        //        _emailTF.text = @"";
    }
    if([Utitlity isConnectedTointernet])
    {
        NSString *msg=@"";
        if(![self isValidEmail:_emailTF.text])
        {
            msg=[NSString stringWithFormat:@"Enter a valid mail id"];
        }
        //        if(msg.length==0)
        //        {
        if([Utitlity isConnectedTointernet])
        {
            NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
            [params setObject:_emailTF.text forKey:@"EmailID"];
            [params setObject:_passwordTF.text forKey:@"Password"];
            [self showProgress];
            NSString* JsonString = [Utitlity JSONStringConv: params];
            NSLog(@"json----%@",JsonString);
            [[WebServices sharedInstance]apiAuthwithJSON:Login HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
                NSString *error=[json valueForKey:@"error"];
                [self hideProgress];
                NSLog(@"json-----%@",json);
                if(error.length>0)
                {
                    [self showMsgAlert:[json valueForKey:@"error_description"]];
                    return ;
                }
                else
                {
                    if([[json valueForKey:@"LoginMITRResult"] count]==0)
                    {
                        [self showMsgAlert:@"Error , Try Again"];
                    }else
                    {
                        if ([[[json valueForKey:@"LoginMITRResult"] objectForKey:@"IsExists"] intValue]==1)
                        {
                            [self showProgress];
                            NSMutableDictionary *params1 = [[NSMutableDictionary alloc]init];
                            [params1 setObject:[[json valueForKey:@"LoginMITRResult"] objectForKey:@"UserID"] forKey:@"UserId"];
                            [params1 setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceId"]  forKey:@"DeviceId"];
                            [params1 setObject:@"IOS" forKey:@"DeviceDescription"];
                            NSString* JsonString1 = [Utitlity JSONStringConv: params1];
                            NSLog(@"json1----%@",JsonString1);
                            [[WebServices sharedInstance]apiAuthwithJSON:@"POSTSaveDeviceInfoOnLogin" HTTPmethod:@"POST" forparameters:JsonString1 ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
                                [self hideProgress];
                                
                                NSLog(@"json1----%@",json);
                            }];
                            
                            NSLog(@"json-----%@",[json valueForKey:@"LoginMITRResult"]);
                            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                            if ([[json valueForKey:@"LoginMITRResult"] objectForKey:@"CountryName"] != [NSNull null]) {
                                [defaults setObject:[[json valueForKey:@"LoginMITRResult"] objectForKey:@"CountryName"] forKey:@"CountryName"];
                            }
                            [defaults setObject:[[json valueForKey:@"LoginMITRResult"] objectForKey:@"EmailID"] forKey:@"EmailID"];
                            [defaults setObject:[[json valueForKey:@"LoginMITRResult"] objectForKey:@"EmployeeName"] forKey:@"EmployeeName"];
                            //                            [defaults setObject:[[json valueForKey:@"LoginMITRResult"] objectForKey:@"EmpID"] forKey:@"EmpID"];
                            [defaults setObject:[[json valueForKey:@"LoginMITRResult"] objectForKey:@"UserID"] forKey:@"UserID"];
                            [defaults setObject:[[json valueForKey:@"LoginMITRResult"] objectForKey:@"RoleDescription"] forKey:@"RoleDescription"];
                            [defaults setObject:[[json valueForKey:@"LoginMITRResult"] objectForKey:@"CountryID"] forKey:@"CountryID"];
                            [defaults setObject:[[json valueForKey:@"LoginMITRResult"] objectForKey:@"LocationID"] forKey:@"LocationID"];
                            [defaults setObject:[[json valueForKey:@"LoginMITRResult"] objectForKey:@"IsExternal"] forKey:@"IsExternal"];
                            [defaults setObject:[[json valueForKey:@"LoginMITRResult"] objectForKey:@"IsMOMApprover"] forKey:@"IsMOMApprover"];
                            [defaults setObject:[[json valueForKey:@"LoginMITRResult"] objectForKey:@"IsPasswordChangeNeeded"] forKey:@"IsPasswordChangeNeeded"];
                            [defaults setObject:[[json valueForKey:@"LoginMITRResult"] objectForKey:@"MemberType"] forKey:@"MemberType"];
                            if ( [[json valueForKey:@"LoginMITRResult"] objectForKey:@"AccessModule"] != [NSNull null]) {
                                [defaults setObject:[[json valueForKey:@"LoginMITRResult"] objectForKey:@"AccessModule"] forKey:@"AccessModule"];
                                if ([[[json valueForKey:@"LoginMITRResult"] objectForKey:@"IsPasswordChangeNeeded"] intValue]==0)
                                {
                                    SWRevealViewController *purchaseContr = (SWRevealViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                                    purchaseContr.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                                    [self presentViewController:purchaseContr animated:YES completion:nil];
                                }
                                else
                                {
                                    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                    if (!_UpdatePassVC) {
                                        _UpdatePassVC = [storyBoard instantiateViewControllerWithIdentifier:@"UpdatePassVC"];
                                    }
                                    [defaults setObject:@0 forKey:@"IsPasswordChangeNeeded"];
                                    [self presentViewController:_UpdatePassVC animated:YES completion:nil];
                                }
                            }
                            else{
                                [defaults setObject:nil forKey:@"EmailID"];
                                [self showMsgAlert:@"There is no Access Module "];
                            }
                        }
                        
                        else
                        {
                            [self showMsgAlert:@"Invalid Credentials"];
                        }
                    }
                }
                
            }];
        }else
        {
            [self showMsgAlert:NOInternetMessage];
        }
    }
    else
    {
        [self showMsgAlert:NOInternetMessage];
    }
}

-(BOOL) isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)showProgress
{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)hideProgress
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _emailTF) {
        _autocompleteTableView.hidden = YES;
    }
    [textField resignFirstResponder];
    return YES;
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    _autocompleteTableView.hidden = YES;
}
- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    if (textField == _emailTF) {
        
        if(pastemails.count==0)
        {
            _autocompleteTableView.hidden = YES;
        }
        else
        {
            _autocompleteTableView.hidden = NO;
        }
        NSString *substring = [NSString stringWithString:textField.text];
        substring = [substring
                     stringByReplacingCharactersInRange:range withString:string];
        [self searchAutocompleteEntriesWithSubstring:substring];
    }
    return YES;
}
- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring
{
    [autocompletemail removeAllObjects];
    for(NSString *curString in pastemails) {
        NSRange substringRange = [curString rangeOfString:substring];
        if (substringRange.location == 0) {
            [autocompletemail addObject:curString];
            NSArray *copy = [autocompletemail copy];
            NSInteger index = [copy count] - 1;
            for (id object in [copy reverseObjectEnumerator]) {
                if ([autocompletemail indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound) {
                    [autocompletemail removeObjectAtIndex:index];
                }
                index--;
            }
        }
    }
    [_autocompleteTableView reloadData];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(autocompletemail.count>0)
    {
        return autocompletemail.count;
    }
    else
        return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell
                 alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    cell.textLabel.text=[autocompletemail objectAtIndex:indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _emailTF.text=[autocompletemail objectAtIndex:indexPath.row];
    _autocompleteTableView.hidden = YES;
    [_emailTF resignFirstResponder];
}
-(void)showMsgAlert:(NSString *)msg
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@""
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)showBtn:(id)sender
{
    if (self.passwordTF.secureTextEntry == YES) {
        self.passwordTF.secureTextEntry = NO;
        [sender setSelected:YES];
    }
    else
    {
        self.passwordTF.secureTextEntry = YES;
        [sender setSelected:NO];
    }
}
- (IBAction)Fpassword:(UIButton *)sender {
    ForgotpassVC *ForgorPW = (ForgotpassVC *)[self.storyboard instantiateViewControllerWithIdentifier:@"ForgotpassVC"];
    //menu is only an example
    ForgorPW.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:ForgorPW animated:YES completion:nil];
}

@end

