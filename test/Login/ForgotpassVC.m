//
//  ForgotpassVC.m
//  test
//
//  Created by ceazeles on 27/02/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "ForgotpassVC.h"
#import "UpdatePassVC.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
@interface ForgotpassVC ()

@end

@implementation ForgotpassVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.headView.layer insertSublayer:[Utitlity getTheGradientColorCode:self.headView] atIndex:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)Backbtn:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)Restbtn:(UIButton *)sender
{
    if([Utitlity isConnectedTointernet])
    {
        NSString *msg=@"";
        if(![self isValidEmail:self.emailtxtfld.text])
        {
            msg=[NSString stringWithFormat:@"Enter a valid mail id"];
        }
        if([Utitlity isConnectedTointernet])
        {
            NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
            [params setObject:self.emailtxtfld.text forKey:@"EmailId"];
            [self showProgress];
            NSString* JsonString = [Utitlity JSONStringConv: params];
            [[WebServices sharedInstance]apiAuthwithJSON:PostForgotPasswordResetAndSend HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
                NSString *error=[json valueForKey:@"error"];
                [self hideProgress];
                if(error.length>0)
                {
                    [self showMsgAlert:[json valueForKey:@"error_description"]];
                    return ;
                }
                else
                {
                    if(json.count==0)
                    {
                        [self showMsgAlert:@"Error , Try Again"];
                    }else
                    {
                        if ([[json objectForKey:@"ForgotPasswordResetAndSendResult"] isEqualToString:@"Mail has been sent with password."])
                        {
                            NSLog(@"json-----%@",json);
                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                                          message:[json objectForKey:@"ForgotPasswordResetAndSendResult"]
                                                                                   preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK"
                                                                                style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction * action)
                                                        {
                                                            [self dismissViewControllerAnimated:YES completion:nil];
                                                        }];
                            [alert addAction:yesButton];
                            [self presentViewController:alert animated:YES completion:nil];
                        }
                        else
                        {
                            [self showMsgAlert:[json objectForKey:@"ForgotPasswordResetAndSendResult"]];
                        }
                    }
                }
                
            }];
        }else
        {
            [self showMsgAlert:NOInternetMessage];
        }
        
    }
}
-(BOOL) isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
-(void)showMsgAlert:(NSString *)msg
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@""
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)showProgress
{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)hideProgress
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
@end

