//
//  ConatctHeaderTableViewCell.h
//  test
//
//  Created by ceazeles on 16/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConatctHeaderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *headerName;
@property (weak, nonatomic) IBOutlet UIButton *btnShowHide;

@end
