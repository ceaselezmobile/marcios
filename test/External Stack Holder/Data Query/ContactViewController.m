//
//  ContactViewController.m
//  test
//
//  Created by ceazeles on 16/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "ContactViewController.h"
#define counts 10

@interface ContactViewController ()<MFMailComposeViewControllerDelegate,UIPopoverPresentationControllerDelegate,UIPopoverControllerDelegate>

@end

@implementation ContactViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Set isMultipleExpansionAllowed = true is multiple expanded sections to be allowed at a time. Default is NO.
    headerArray = [NSMutableArray array];
    contactArray = [NSMutableArray array];
    noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height/2,self.view.frame.size.width ,50)];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    isMultipleExpansionAllowed = NO;
    
    arrSelectedSectionIndex = [[NSMutableArray alloc] init];
    
    if (!isMultipleExpansionAllowed) {
        [arrSelectedSectionIndex addObject:[NSNumber numberWithInt:counts+2]];
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    [self loadDataFromApi:[NSString stringWithFormat:@"ARCExtStakeholderMainDetailsByUserId?userid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]] :@"header"];
}

#pragma mark - TableView methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return headerArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([arrSelectedSectionIndex containsObject:[NSNumber numberWithInteger:section]])
    {
        return contactArray.count;
    }else{
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contactCell"];
    
    if (cell ==nil)
    {
        [self.tableView registerClass:[ContactTableViewCell class] forCellReuseIdentifier:@"contactCell"];
        
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"contactCell"];
    }
    
    cell.callButton.tag = indexPath.row;
    [cell.callButton addTarget:self action:@selector(call:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.mailButton.tag = indexPath.row;
    [cell.mailButton addTarget:self action:@selector(mail:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.whatsAppButton.tag = indexPath.row;
    [cell.whatsAppButton addTarget:self action:@selector(message:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDictionary *dict = contactArray[indexPath.row];
    cell.contactName.text = dict[@"ContactName"];
    cell.contactNumber.text = dict[@"MobileNo"];
    if(![dict[@"Location"] isEqual:[NSNull null]])
    {
        cell.locationLabel.text = dict[@"Location"];
    }
    else{
        cell.locationLabel.text = @"N/A";
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    ConatctHeaderTableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:@"contactCellHeader"];
    
    if (headerView ==nil)
    {
        [self.tableView registerClass:[ConatctHeaderTableViewCell class] forCellReuseIdentifier:@"contactCellHeader"];
        
        headerView = [tableView dequeueReusableCellWithIdentifier:@"contactCellHeader"];
    }
    if(headerArray.count>0)
    {
        headerView.headerName.text = [headerArray[section]valueForKey:@"ExternalStakeholderName"];
    }
    
    if ([arrSelectedSectionIndex containsObject:[NSNumber numberWithInteger:section]])
    {
        headerView.btnShowHide.selected = YES;
    }
    
    [[headerView btnShowHide] setTag:section];
    
    [[headerView btnShowHide] addTarget:self action:@selector(btnTapShowHideSection:) forControlEvents:UIControlEventTouchUpInside];
    
    [headerView.contentView setBackgroundColor:section%2==0?[UIColor groupTableViewBackgroundColor]:[[UIColor groupTableViewBackgroundColor] colorWithAlphaComponent:0.5f]];
    return headerView.contentView;
}

-(IBAction)btnTapShowHideSection:(UIButton*)sender
{
    
    contactArray = [[headerArray objectAtIndex:sender.tag]objectForKey:@"lstContact"];
    if (!sender.selected)
    {
        if (!isMultipleExpansionAllowed) {
            if(arrSelectedSectionIndex.count>0)
            {
                [arrSelectedSectionIndex replaceObjectAtIndex:0 withObject:[NSNumber numberWithInteger:sender.tag]];
            }
            else{
                [arrSelectedSectionIndex addObject:[NSNumber numberWithInteger:sender.tag]];
            }
        }else {
            [arrSelectedSectionIndex addObject:[NSNumber numberWithInteger:sender.tag]];
        }
        
        sender.selected = YES;
    }else{
        sender.selected = NO;
        
        if ([arrSelectedSectionIndex containsObject:[NSNumber numberWithInteger:sender.tag]])
        {
            [arrSelectedSectionIndex removeObject:[NSNumber numberWithInteger:sender.tag]];
        }
    }
    
    if (!isMultipleExpansionAllowed) {
        if(contactArray.count>0)
        {
            [self.tableView reloadData];
        }
        else{
            [[Utitlity sharedInstance] showAlertViewWithMessage:@"No contacts found" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
             {
                 
             }];
        }
    }else {
        if(contactArray.count>0)
        {
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:sender.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        else{
            [[Utitlity sharedInstance] showAlertViewWithMessage:@"No contacts found" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
             {
                 
             }];
        }
        
    }
}

-(void)call:(UIButton*)sender
{
    
    [self makeCall:[contactArray[sender.tag]valueForKey:@"MobileNo"]];
}

-(void)mail:(UIButton*)sender
{
   
    [self sendMail:[contactArray[sender.tag]valueForKey:@"EmailId"]];
}

-(void)message:(UIButton *)sender
{
    [self sendMessage :[contactArray[sender.tag]valueForKey:@"MobileNo"]];
}

-(void)makeCall :(NSString *)number
{
    number = [@"telprompt://" stringByAppendingString:number];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:number]];
}

-(void)sendMail :(NSString *)mailId
{
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    mail.mailComposeDelegate = self;
    [mail setSubject:@""];
    [mail setMessageBody:@"" isHTML:NO];
    [mail setToRecipients:@[[NSString stringWithFormat:@"%@",mailId]]];
    
    [self presentViewController:mail animated:YES completion:NULL];
}

-(void)sendMessage :(NSString *)number
{
    NSURL *whatsappURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.whatsapp.com/send?phone=%@",[@"+91" stringByAppendingString:number]]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 155;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PopoverViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PopoverViewController"];
    vc.modalPresentationStyle = UIModalPresentationPopover;
    vc.preferredContentSize = CGSizeMake(self.view.frame.size.width-20, 500);
    vc.popoverPresentationController.delegate = self;
    vc.popoverPresentationController.permittedArrowDirections = 0;
    vc.popoverPresentationController.sourceView = self.view;
    vc.popoverPresentationController.sourceRect = CGRectMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds),0,0);
   // [self presentViewController:vc animated:YES completion:nil];
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    
    return UIModalPresentationNone;
}

-(void)loadDataFromApi :(NSString *)url :(NSString *)type{
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[WebServices sharedInstance] getWithParameter:nil withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
            NSLog(@"response izz %@",responseObject);
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self removeNoDataFound:self.view];
                
                [self->headerArray removeAllObjects];
                [self->headerArray addObjectsFromArray:responseObject];
                if(self->headerArray.count > 0 )
                {
                    [self.tableView reloadData];
                }
                else{
                    [self showNoDataFound:self.view];
                }
                
            });
            
        }];
        
        
    }else{
        
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}

-(void)showNoDataFound :(UIView *)view
{
    self.tableView.hidden = YES;
    if(![noDataLabel isDescendantOfView:view])
    {
        [view addSubview:noDataLabel];
    }
    [noDataLabel setFont:[UIFont systemFontOfSize:20]];
    noDataLabel.textAlignment=NSTextAlignmentCenter;
    noDataLabel.layer.masksToBounds  = YES;
    noDataLabel.layer.shadowOpacity  = 2.5;
    noDataLabel.layer.shadowColor    = [[UIColor grayColor] CGColor];
    noDataLabel.layer.shadowOffset   = CGSizeMake(0, 1);
    noDataLabel.layer.shadowRadius   = 2;
    noDataLabel.text=@"No Data Found";
}

-(void)removeNoDataFound :(UIView *)view
{
    self.tableView.hidden = NO;
    [noDataLabel removeFromSuperview];
}

#pragma mark - Memory Warning

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
