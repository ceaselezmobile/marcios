//
//  ContactViewController.h
//  test
//
//  Created by ceazeles on 16/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactTableViewCell.h"
#import "ConatctHeaderTableViewCell.h"
#import "SWRevealViewController.h"
#import "Utitlity.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "PopoverViewController.h"
#import <MessageUI/MessageUI.h>

@interface ContactViewController : UIViewController
{
    NSMutableArray *arrSelectedSectionIndex;
    BOOL isMultipleExpansionAllowed;
    NSMutableArray *headerArray;
    NSMutableArray *contactArray;
    UILabel *noDataLabel;
}

-(void)loadDataFromApi :(NSString *)url :(NSString *)type;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;

@end
