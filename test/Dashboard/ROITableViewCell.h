//
//  ROITableViewCell.h
//  test
//
//  Created by ceazeles on 14/11/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ROITableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UILabel *roiValueLabel;

@end

NS_ASSUME_NONNULL_END
