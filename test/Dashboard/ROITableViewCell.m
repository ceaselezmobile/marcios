//
//  ROITableViewCell.m
//  test
//
//  Created by ceazeles on 14/11/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "ROITableViewCell.h"

@implementation ROITableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
