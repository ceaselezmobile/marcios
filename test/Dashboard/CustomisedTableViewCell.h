//
//  CustomisedTableViewCell.h
//  test
//
//  Created by ceazeles on 24/10/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomisedTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *bankNameHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *refNoHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *accountNameHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *ddHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *lddHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *riskReportHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *ceoNoteHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *bidHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *trustHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *resolutionHeaderText;

@property (weak, nonatomic) IBOutlet UITextField *bankNameDataText;
@property (weak, nonatomic) IBOutlet UITextField *refNoDataText;
@property (weak, nonatomic) IBOutlet UITextField *accountNameDataText;
@property (weak, nonatomic) IBOutlet UITextField *ddDataText;
@property (weak, nonatomic) IBOutlet UITextField *lddDataText;
@property (weak, nonatomic) IBOutlet UITextField *riskReportDataText;
@property (weak, nonatomic) IBOutlet UITextField *ceoNoteDataText;
@property (weak, nonatomic) IBOutlet UITextField *bidDataText;
@property (weak, nonatomic) IBOutlet UITextField *trustDataText;
@property (weak, nonatomic) IBOutlet UITextField *resolutionDataText;

@property (weak, nonatomic) IBOutlet UITextField *recievedPimHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *referredPimHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *shortListedPimHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *rejectedPimHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *totalDdHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *totalLddHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *totalRiskMgmtHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *bidWonHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *bidLossHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *acquiredAcctHeaderText;


@property (weak, nonatomic) IBOutlet UITextField *recievedPimDataText;
@property (weak, nonatomic) IBOutlet UITextField *referredPimDataText;
@property (weak, nonatomic) IBOutlet UITextField *shortListedPimDataText;
@property (weak, nonatomic) IBOutlet UITextField *rejectedPimDataText;
@property (weak, nonatomic) IBOutlet UITextField *totalDdDataText;
@property (weak, nonatomic) IBOutlet UITextField *totalLddDataText;
@property (weak, nonatomic) IBOutlet UITextField *totalRiskMgmtDataText;
@property (weak, nonatomic) IBOutlet UITextField *bidWonDataText;
@property (weak, nonatomic) IBOutlet UITextField *bidLossDataText;
@property (weak, nonatomic) IBOutlet UITextField *acquiredAcctDataText;

@property (weak, nonatomic) IBOutlet UITextField *trustNameHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *noOfAccountsText;
@property (weak, nonatomic) IBOutlet UITextField *arcSriText;
@property (weak, nonatomic) IBOutlet UITextField *arcSrrText;
@property (weak, nonatomic) IBOutlet UITextField *arcSroText;
@property (weak, nonatomic) IBOutlet UITextField *businessSriText;
@property (weak, nonatomic) IBOutlet UITextField *businessSrrText;
@property (weak, nonatomic) IBOutlet UITextField *businessSroText;

@property (weak, nonatomic) IBOutlet UITextField *trustNameDataText;
@property (weak, nonatomic) IBOutlet UITextField *noOfAccountsDataText;
@property (weak, nonatomic) IBOutlet UITextField *arcSriDataText;
@property (weak, nonatomic) IBOutlet UITextField *arcSrrDataText;
@property (weak, nonatomic) IBOutlet UITextField *arcSroDataText;
@property (weak, nonatomic) IBOutlet UITextField *businessSriDataText;
@property (weak, nonatomic) IBOutlet UITextField *businessSrrDataText;
@property (weak, nonatomic) IBOutlet UITextField *businessSroDataText;

@property (weak, nonatomic) IBOutlet UITextField *srInvestmentHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *srInvestmentDataText;
@property (weak, nonatomic) IBOutlet UITextField *srRecoveredHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *srRecoveredDataText;
@property (weak, nonatomic) IBOutlet UITextField *srOutstandingHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *srOutstandingDataText;
@property (weak, nonatomic) IBOutlet UITextField *mgmtFeeExpHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *mgmtFeeExpDataText;
@property (weak, nonatomic) IBOutlet UITextField *mgmtFeeRecHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *mgmtFeeRecDataText;
@property (weak, nonatomic) IBOutlet UITextField *incentiveRecievedHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *incentiveRecievedDataText;
@property (weak, nonatomic) IBOutlet UITextField *expenseReimbursedHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *expenseReimbursedDataText;
@property (weak, nonatomic) IBOutlet UITextField *expenseOutstandingHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *expenseOutstandingDataText;
@property (weak, nonatomic) IBOutlet UITextField *interestReimbursedHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *interestReimbursedDataText;
@property (weak, nonatomic) IBOutlet UITextField *interestOutstandingHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *interestOutstandingDataText;


@end

NS_ASSUME_NONNULL_END
