//
//  DashboardDetailViewController.h
//  test
//
//  Created by ceazeles on 27/10/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface DashboardDetailViewController : UIViewController
{
    NSMutableDictionary *dictionary;
    NSMutableArray *docArray;
    
}

@property(strong,nonatomic)NSMutableDictionary *dict;
@property(strong,nonatomic)NSMutableDictionary *theDetailArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
