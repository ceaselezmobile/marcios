//
//  DashboardViewController.h
//  test
//
//  Created by ceaselez on 27/11/17.
//  Copyright © 2017 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DemoBaseViewController.h"
#import <Charts/Charts.h>
#import "IntAxisValueFormatter.h"
#import "DashboardDataCollectionViewCell.h"
#import "Utitlity.h"
#import "GlobalURL.h"
#import "WebServices.h"
#import "MBProgressHUD.h"
#import "DashboardTableViewCell.h"
#import "CustomisedTableViewCell.h"
#import "DashboardDetailViewController.h"
#import "ROITableViewCell.h"

@interface DashboardViewController : DemoBaseViewController
{
    UILabel *noDataLabel;
    NSMutableArray *titleArray;
    NSMutableArray *resolutionTitleArray;
    NSMutableArray *array;
    NSMutableArray *processedPimArray;
    NSMutableArray *graphArray;
    int xaxiscount;
    int yaxisVal;
    NSMutableArray *xVals;
  
}
@property (weak, nonatomic) IBOutlet UIImageView *meetingImage;
@property (weak, nonatomic) IBOutlet UIImageView *actionPlannerImage;
@property (weak, nonatomic) IBOutlet BarChartView *chartView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *ButtonArray;
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *radioButtonArray;

@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *chartButtonArray;
@property (weak, nonatomic) IBOutlet UIScrollView *pimScrollView;
@property (weak, nonatomic) IBOutlet UITableView *consolidatedPimTable;
@property (weak, nonatomic) IBOutlet UITableView *feeIncentiveTable;
@property (weak, nonatomic) IBOutlet UITableView *processedPimTable;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *caseView;
@property (weak, nonatomic) IBOutlet UIScrollView *feeIncentiveScrollView;

@property (weak, nonatomic) IBOutlet UIScrollView *srScrollView;
@property (weak, nonatomic) IBOutlet UITableView *srTableView;
@property (weak, nonatomic) IBOutlet UIView *caseButtonView;
@property (weak, nonatomic) IBOutlet UIView *preAcqButtonView;
@property (weak, nonatomic) IBOutlet UIView *resolutionButtonView;
@property (weak, nonatomic) IBOutlet UIView *preAcquisitionView;
@property (weak, nonatomic) IBOutlet UIView *radioButtonView;

@property (weak, nonatomic) IBOutlet UIView *resolutionView;
@property (weak, nonatomic) IBOutlet UICollectionView *resolutionCollectionView;
@property (weak, nonatomic) IBOutlet BarChartView *resolutionChartView;
@property (weak, nonatomic) IBOutlet UIView *resolutionRadioButtonView;
@property (weak, nonatomic) IBOutlet LineChartView *linechartView;
@property (weak, nonatomic) IBOutlet UITableView *roiTableView;


@end
