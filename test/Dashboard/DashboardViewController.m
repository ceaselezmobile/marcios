//
//  DashboardViewController.m
//  test
//
//  Created by ceaselez on 27/11/17.
//  Copyright © 2017 ceaselez. All rights reserved.
//

#import "DashboardViewController.h"
#import "SWRevealViewController.h"
#import "DashBoardPieChartViewController.h"
#import "MySharedManager.h"
#import "UIImage+Color.h"

@interface DashboardViewController ()<ChartViewDelegate,IChartAxisValueFormatter>
@property (weak, nonatomic) IBOutlet UIButton *revealButtonItem;
@property (nonatomic, strong) DashBoardPieChartViewController *DashBoardPieChartViewController;

@end

@implementation DashboardViewController
{
    MySharedManager *sharedManager;
}
- (void)viewDidLoad {
    sharedManager = [MySharedManager sharedManager];
    
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [self.revealButtonItem addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    titleArray = [NSMutableArray arrayWithObjects:@"PIM Stage Analysis",@"Consolidated PIMs",@"PIM Rejection Analysis",@"Bid Win Vs Lost", nil];
    resolutionTitleArray = [NSMutableArray arrayWithObjects:@"SR Redemption",@"Recovery Analysis",@"Expense Analysis",@"ROI",@"Fee & Incentive Analysis", nil];
    
    self.titleLabel.text = @"PIM Stage Analysis";
    
    self.options = @[
                     @{@"key": @"toggleValues", @"label": @"Toggle Values"},
                     @{@"key": @"toggleHighlight", @"label": @"Toggle Highlight"},
                     @{@"key": @"animateX", @"label": @"Animate X"},
                     @{@"key": @"animateY", @"label": @"Animate Y"},
                     @{@"key": @"animateXY", @"label": @"Animate XY"},
                     @{@"key": @"saveToGallery", @"label": @"Save to Camera Roll"},
                     @{@"key": @"togglePinchZoom", @"label": @"Toggle PinchZoom"},
                     @{@"key": @"toggleAutoScaleMinMax", @"label": @"Toggle auto scale min/max"},
                     @{@"key": @"toggleData", @"label": @"Toggle Data"},
                     @{@"key": @"toggleBarBorders", @"label": @"Show Bar Borders"},
                     ];
    
    _chartView.delegate = self;
    _chartView.chartDescription.enabled = NO;
    _chartView.pinchZoomEnabled = NO;
    _chartView.drawBarShadowEnabled = NO;
    _chartView.drawGridBackgroundEnabled = NO;
    _chartView.doubleTapToZoomEnabled=NO;
    [_chartView zoomIn];
    
    
    _resolutionChartView.delegate = self;
    _resolutionChartView.chartDescription.enabled = NO;
    _resolutionChartView.pinchZoomEnabled = NO;
    _resolutionChartView.drawBarShadowEnabled = NO;
    _resolutionChartView.drawGridBackgroundEnabled = NO;
    _resolutionChartView.doubleTapToZoomEnabled=NO;
    [_resolutionChartView zoomIn];
    [_resolutionChartView zoomWithScaleX:2 scaleY:0 x:0 y:0];
    
    
    BalloonMarker *marker = [[BalloonMarker alloc]
                             initWithColor: [UIColor colorWithWhite:180/255. alpha:1.0]
                             font: [UIFont systemFontOfSize:12.0]
                             textColor: UIColor.whiteColor
                             insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0)];
    marker.minimumSize = CGSizeMake(80.f, 40.f);
    
    marker.chartView = _chartView;
    _chartView.marker = marker;
    marker.chartView = _resolutionChartView;
    _resolutionChartView.marker = marker;
    
    ChartLegend *legend = _chartView.legend;
    legend.horizontalAlignment = ChartLegendHorizontalAlignmentLeft;
    legend.verticalAlignment = ChartLegendVerticalAlignmentBottom;
    legend.orientation = ChartLegendOrientationHorizontal;
    legend.drawInside = NO;
    legend.font = [UIFont fontWithName:@"Avenir-Medium" size:12.f];
    legend.yOffset = 10.0;
    legend.xOffset = 10.0;
    legend.yEntrySpace = 0.0;
    
    ChartXAxis *xAxis = _chartView.xAxis;
    xAxis.labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
    xAxis.granularity = 1.f;
    xAxis.centerAxisLabelsEnabled = YES;
    xAxis.valueFormatter = self;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    
    NSNumberFormatter *leftAxisFormatter = [[NSNumberFormatter alloc] init];
    leftAxisFormatter.maximumFractionDigits = 1;
    
    ChartYAxis *leftAxis = _chartView.leftAxis;
    leftAxis.labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
    // leftAxis.valueFormatter = [[LargeValueFormatter alloc] init];
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.spaceTop = 0.35;
    leftAxis.axisMinimum = 0;
    _chartView.rightAxis.enabled = NO;
    
    
    ChartLegend *legend1 = _resolutionChartView.legend;
    legend1.horizontalAlignment = ChartLegendHorizontalAlignmentLeft;
    legend1.verticalAlignment = ChartLegendVerticalAlignmentBottom;
    legend1.orientation = ChartLegendOrientationHorizontal;
    legend1.drawInside = NO;
    legend1.font = [UIFont fontWithName:@"Avenir-Medium" size:12.f];
    legend1.yOffset = 10.0;
    legend1.xOffset = 10.0;
    legend1.yEntrySpace = 0.0;
    
    ChartXAxis *xAxis1 = _resolutionChartView.xAxis;
    xAxis1.labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
    xAxis1.granularity = 1.f;
    xAxis1.centerAxisLabelsEnabled = YES;
    xAxis1.valueFormatter = self;
    xAxis1.labelPosition = XAxisLabelPositionBottom;
    
    ChartYAxis *leftAxis1 = _resolutionChartView.leftAxis;
    leftAxis1.labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
    // leftAxis.valueFormatter = [[LargeValueFormatter alloc] init];
    leftAxis1.drawGridLinesEnabled = NO;
    leftAxis1.spaceTop = 0.35;
    leftAxis1.axisMinimum = 0;
    _resolutionChartView.rightAxis.enabled = NO;
    
    
    _linechartView.delegate = self;
    
    _linechartView.chartDescription.enabled = NO;
    
    _linechartView.dragEnabled = YES;
    [_linechartView setScaleEnabled:YES];
    _linechartView.pinchZoomEnabled = YES;
    _linechartView.drawGridBackgroundEnabled = NO;
    
    // x-axis limit line
    ChartLimitLine *llXAxis = [[ChartLimitLine alloc] initWithLimit:10.0 label:@"Index 10"];
    llXAxis.lineWidth = 4.0;
    llXAxis.lineDashLengths = @[@(10.f), @(10.f), @(0.f)];
    llXAxis.labelPosition = ChartLimitLabelPositionRightBottom;
    llXAxis.valueFont = [UIFont systemFontOfSize:10.f];
    
    //  [_linechartView.xAxis addLimitLine:llXAxis];
    
    
    _linechartView.xAxis.gridLineDashLengths = @[@10.0, @10.0];
    _linechartView.xAxis.gridLineDashPhase = 0.f;
    
    
    ChartYAxis *leftAxisLine = _linechartView.leftAxis;
    [leftAxisLine removeAllLimitLines];
    leftAxisLine.gridLineDashLengths = @[@5.f, @5.f];
    leftAxisLine.drawZeroLineEnabled = NO;
    leftAxisLine.drawLimitLinesBehindDataEnabled = YES;
    
    _linechartView.rightAxis.enabled = NO;
    
    // [_linechartView.viewPortHandler setMaximumScaleY: 2.f];
    // [_linechartView.viewPortHandler setMaximumScaleX: 2.f];
    
    _linechartView.marker = marker;
    
    _linechartView.legend.form = ChartLegendFormLine;
    
    ChartXAxis *xAxisLine = _linechartView.xAxis;
    xAxisLine.labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
    xAxisLine.granularity = 1.f;
    xAxisLine.centerAxisLabelsEnabled = NO;
    xAxisLine.valueFormatter = self;
    xAxisLine.labelPosition = XAxisLabelPositionBottom;
    xAxisLine.spaceMax = 0.5f;
    xAxisLine.spaceMin = 0.5f;
    
    
    [_chartView animateWithXAxisDuration:2.5];
    [_linechartView animateWithXAxisDuration:2.5];
    
    array = [NSMutableArray array];
    processedPimArray = [NSMutableArray array];
    graphArray = [NSMutableArray array];
    xVals=[NSMutableArray new];
    
    
    [self loadDataFromApi:[NSString stringWithFormat:@"/GetChartProgress?userid=%@&type=1",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]] :@"Case"];
    
    self.caseView.hidden = NO;
    self.preAcquisitionView.hidden = YES;
    
    
    noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height/2,self.view.frame.size.width ,50)];
}


-(void) viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}


- (void)updateChartData
{
    if (self.shouldHideData)
    {
        _chartView.data = nil;
        return;
    }
    
    xaxiscount=(int)[graphArray count];
    [self setDataCount:xaxiscount range:yaxisVal];
}

- (void)setDataCount:(int)count range:(double)range
{
   // [_chartView zoomWithScaleX:count/3 scaleY:0 x:0 y:0];
    if([self.titleLabel.text isEqualToString:@"Consolidated PIMs"])
    {
        float groupSpace = 0.1f;
        float barSpace = 0.01f;
        float barWidth = 0.08f;
        // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"
        
        NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals3 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals4 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals5 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals6 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals7 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals8 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals9 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals10 = [[NSMutableArray alloc] init];
        
        int groupCount = count;
        int startYear = 0;
        int endYear = startYear + groupCount;
        
        for (int i = startYear; i < endYear; i++)
        {
            [yVals1 addObject:[[BarChartDataEntry alloc]
                               initWithX:i
                               y:[[[graphArray valueForKey:@"TotalPIMCount"]objectAtIndex:i] doubleValue]]];
            
            [yVals2 addObject:[[BarChartDataEntry alloc]
                               initWithX:i
                               y:[[[graphArray valueForKey:@"ReferPIMCount"]objectAtIndex:i] doubleValue]]];
            [yVals3 addObject:[[BarChartDataEntry alloc]
                               initWithX:i
                               y:[[[graphArray valueForKey:@"RejectPIMCount"]objectAtIndex:i] doubleValue]]];
            
            [yVals4 addObject:[[BarChartDataEntry alloc]
                               initWithX:i
                               y:[[[graphArray valueForKey:@"ShortlistPIMCount"]objectAtIndex:i] doubleValue]]];
            
            [yVals5 addObject:[[BarChartDataEntry alloc]
                               initWithX:i
                               y:[[[graphArray valueForKey:@"TotalDDCount"]objectAtIndex:i] doubleValue]]];
            
            [yVals6 addObject:[[BarChartDataEntry alloc]
                               initWithX:i
                               y:[[[graphArray valueForKey:@"TotalLDDCount"]objectAtIndex:i] doubleValue]]];
            [yVals7 addObject:[[BarChartDataEntry alloc]
                               initWithX:i
                               y:[[[graphArray valueForKey:@"TotalRMCount"]objectAtIndex:i] doubleValue]]];
            
            [yVals8 addObject:[[BarChartDataEntry alloc]
                               initWithX:i
                               y:[[[graphArray valueForKey:@"TotalBidWonCount"]objectAtIndex:i] doubleValue]]];
            
            [yVals9 addObject:[[BarChartDataEntry alloc]
                               initWithX:i
                               y:[[[graphArray valueForKey:@"TotalBidLossCount"]objectAtIndex:i] doubleValue]]];
            
            [yVals10 addObject:[[BarChartDataEntry alloc]
                                initWithX:i
                                y:[[[graphArray valueForKey:@"TotalAcquiredCount"]objectAtIndex:i] doubleValue]]];
            
            
            //        [yVals3 addObject:[[BarChartDataEntry alloc]
            //                           initWithX:i
            //                           y:(double) (arc4random_uniform(randomMultiplier))]];
            //
            //        [yVals4 addObject:[[BarChartDataEntry alloc]
            //                           initWithX:i
            //                           y:(double) (arc4random_uniform(randomMultiplier))]];
        }
        
        BarChartDataSet *set1 = nil, *set2 = nil, *set3 = nil, *set4 = nil ,*set5 = nil, *set6 = nil, *set7 = nil, *set8 = nil , *set9 = nil, *set10 = nil;
        /*  if (_chartView.data.dataSetCount > 0)
         {
         set1 = (BarChartDataSet *)_chartView.data.dataSets[0];
         set2 = (BarChartDataSet *)_chartView.data.dataSets[1];
         set3 = (BarChartDataSet *)_chartView.data.dataSets[2];
         set4 = (BarChartDataSet *)_chartView.data.dataSets[3];
         set5 = (BarChartDataSet *)_chartView.data.dataSets[4];
         set6 = (BarChartDataSet *)_chartView.data.dataSets[5];
         set7 = (BarChartDataSet *)_chartView.data.dataSets[6];
         set8 = (BarChartDataSet *)_chartView.data.dataSets[7];
         set9 = (BarChartDataSet *)_chartView.data.dataSets[8];
         set10 = (BarChartDataSet *)_chartView.data.dataSets[9];
         
         set1.values = yVals1;
         set2.values = yVals2;
         set3.values = yVals3;
         set4.values = yVals4;
         set5.values = yVals5;
         set6.values = yVals6;
         set7.values = yVals7;
         set8.values = yVals8;
         set9.values = yVals9;
         set10.values = yVals10;
         
         BarChartData *data = _chartView.barData;
         
         _chartView.xAxis.axisMinimum = startYear;
         _chartView.xAxis.axisMaximum = [data groupWidthWithGroupSpace:groupSpace barSpace: barSpace] * xaxiscount + startYear;
         [data groupBarsFromX: startYear groupSpace: groupSpace barSpace: barSpace];
         [_chartView.data notifyDataChanged];
         [_chartView notifyDataSetChanged];
         }
         else
         {*/
        set1 = [[BarChartDataSet alloc] initWithValues:yVals1 label:@"Total PIM Count"];
        [set1 setColor:[UIColor colorWithRed:104/255.f green:241/255.f blue:175/255.f alpha:1.f]];
        
        set2 = [[BarChartDataSet alloc] initWithValues:yVals2 label:@"Refer PIM Count"];
        [set2 setColor:[UIColor colorWithRed:164/255.f green:228/255.f blue:251/255.f alpha:1.f]];
        
        set3 = [[BarChartDataSet alloc] initWithValues:yVals3 label:@"ShortlistPIMCount"];
        [set3 setColor:[UIColor colorWithRed:242/255.f green:247/255.f blue:158/255.f alpha:1.f]];
        
        set4 = [[BarChartDataSet alloc] initWithValues:yVals4 label:@"RejectPIMCount"];
        [set4 setColor:[UIColor colorWithRed:255/255.f green:102/255.f blue:0/255.f alpha:1.f]];
        
        set5 = [[BarChartDataSet alloc] initWithValues:yVals5 label:@"Total DD Count"];
        [set5 setColor:[UIColor colorWithRed:204/255.f green:241/255.f blue:175/255.f alpha:1.f]];
        
        set6 = [[BarChartDataSet alloc] initWithValues:yVals6 label:@"Total LDD Count"];
        [set6 setColor:[UIColor colorWithRed:64/255.f green:228/255.f blue:251/255.f alpha:1.f]];
        
        set7 = [[BarChartDataSet alloc] initWithValues:yVals7 label:@"Total RM Count"];
        [set7 setColor:[UIColor colorWithRed:42/255.f green:247/255.f blue:18/255.f alpha:1.f]];
        
        set8 = [[BarChartDataSet alloc] initWithValues:yVals8 label:@"Total Bid Won Count"];
        [set8 setColor:[UIColor colorWithRed:55/255.f green:102/255.f blue:0/255.f alpha:1.f]];
        set9 = [[BarChartDataSet alloc] initWithValues:yVals9 label:@"Total Bid Loss Count"];
        [set9 setColor:[UIColor colorWithRed:242/255.f green:247/255.f blue:58/255.f alpha:1.f]];
        
        set10 = [[BarChartDataSet alloc] initWithValues:yVals10 label:@"Total Acquired Count"];
        [set10 setColor:[UIColor colorWithRed:255/255.f green:2/255.f blue:0/255.f alpha:1.f]];
        
        NSMutableArray *dataSets = [[NSMutableArray alloc] init];
        [dataSets addObject:set1];
        [dataSets addObject:set2];
        [dataSets addObject:set3];
        [dataSets addObject:set4];
        [dataSets addObject:set5];
        [dataSets addObject:set6];
        [dataSets addObject:set7];
        [dataSets addObject:set8];
        [dataSets addObject:set9];
        [dataSets addObject:set10];
        
        BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
        [data setValueFont:[UIFont fontWithName:@"Avenir-Medium" size:10.f]];
        //        [data setValueFormatter:[[LargeValueFormatter alloc] init]];
        
        
        // specify the width each bar should have
        data.barWidth = barWidth;
        
        // restrict the x-axis range
        _chartView.xAxis.axisMinimum = startYear;
        
        // groupWidthWithGroupSpace(...) is a helper that calculates the width each group needs based on the provided parameters
        _chartView.xAxis.axisMaximum = startYear + [data groupWidthWithGroupSpace:groupSpace barSpace: barSpace] * groupCount;
        
        [data groupBarsFromX: startYear groupSpace: groupSpace barSpace: barSpace];
        
        _chartView.data = data;
        [_chartView.data notifyDataChanged];
        [_chartView notifyDataSetChanged];
        //  }
    }
    else if([self.titleLabel.text isEqualToString:@"PIM Rejection Analysis"])
    {
        float val = 0.99/graphArray.count;
        float someVal = val/2;
        
        float groupSpace = 0.01f;
        float barSpace = someVal;
        float barWidth = someVal;
        // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"
        
        
        NSMutableArray *yVals = [[NSMutableArray alloc] init];
        BarChartDataSet *set1 = nil;
        
        int groupCount = 0;
        int startYear = 0;
        
        
        NSMutableArray *dataSets = [[NSMutableArray alloc] init];
        for (int i = startYear; i < graphArray.count; i++)
        {
            NSMutableArray *arr = [[graphArray valueForKey:@"details"]objectAtIndex:i];
            groupCount = (int)arr.count;
            NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
            for(int k=0;k<arr.count;k++)
            {
                if(yVals1.count <= k)
                {
                    [yVals1 addObject:[[BarChartDataEntry alloc]
                                       initWithX:i
                                       y:[[[[[graphArray valueForKey:@"details"]objectAtIndex:i]valueForKey:@"Count"]objectAtIndex:k] doubleValue]]];
                }
                /* if(yVals1.count == arr.count && yVals2.count <= k)
                 {
                 [yVals2 addObject:[[BarChartDataEntry alloc]
                 initWithX:i
                 y:[[[[[graphArray valueForKey:@"details"]objectAtIndex:i]valueForKey:@"Count"]objectAtIndex:k] doubleValue]]];
                 }
                 if(yVals2.count == arr.count && yVals3.count <= k)
                 {
                 [yVals3 addObject:[[BarChartDataEntry alloc]
                 initWithX:i
                 y:[[[[[graphArray valueForKey:@"details"]objectAtIndex:i]valueForKey:@"Count"]objectAtIndex:k] doubleValue]]];
                 }*/
            }
            [yVals addObject:yVals1];
            set1 = [[BarChartDataSet alloc] initWithValues:yVals[i] label:[[graphArray valueForKey:@"BankName"]objectAtIndex:i]];
            
            NSMutableArray *colors = [NSMutableArray array];
            
            for (float h= 0.0 ; h < graphArray.count; h++) {
                UIColor *color = [UIColor colorWithRed:h*10 green:h*12 blue:h*50 alpha:1.0];
                [colors addObject:color];
            }
            set1.colors = @[ChartColorTemplates.material[i]];
            [dataSets addObject:set1];
        }
    
        /*  for(int x=0;x<graphArray.count;x++)
         {
         set1 = [[BarChartDataSet alloc] initWithValues:yValues[x] label:[[graphArray valueForKey:@"BankName"]objectAtIndex:x]];
         set1.colors = @[ChartColorTemplates.material[x]];
         [dataSets addObject:set1];
         }*/
        
        BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
        [data setValueFont:[UIFont fontWithName:@"Avenir-Medium" size:10.f]];
        //        [data setValueFormatter:[[LargeValueFormatter alloc] init]];
        
        
        // specify the width each bar should have
        data.barWidth = barWidth;
        
        // restrict the x-axis range
        _chartView.xAxis.axisMinimum = startYear;
        
        // groupWidthWithGroupSpace(...) is a helper that calculates the width each group needs based on the provided parameters
        _chartView.xAxis.axisMaximum = startYear + [data groupWidthWithGroupSpace:groupSpace barSpace: barSpace] * groupCount;
        
        [data groupBarsFromX: startYear groupSpace: groupSpace barSpace: barSpace];
        
        _chartView.data = data;
        [_chartView.data notifyDataChanged];
        [_chartView notifyDataSetChanged];
        // }
    }
    else if([self.titleLabel.text isEqualToString:@"ROI"])
    {
        NSMutableArray *values = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < count; i++)
        {
            
            [values addObject:[[ChartDataEntry alloc] initWithX:i y:[[[graphArray valueForKey:@"ROI"]objectAtIndex:i] doubleValue] icon: [UIImage imageNamed:@"icon"]]];
        }
        
        LineChartDataSet *set1 = nil;
        
        set1 = [[LineChartDataSet alloc] initWithValues:values label:@"ROI (%)"];
        
        set1.drawIconsEnabled = NO;
        
        set1.lineDashLengths = @[@5.f, @2.5f];
        set1.highlightLineDashLengths = @[@5.f, @2.5f];
        [set1 setColor:UIColor.blackColor];
        [set1 setCircleColor:UIColor.blackColor];
        set1.lineWidth = 1.0;
        set1.circleRadius = 3.0;
        set1.drawCircleHoleEnabled = NO;
        set1.valueFont = [UIFont systemFontOfSize:9.f];
        set1.formLineDashLengths = @[@5.f, @2.5f];
        set1.formLineWidth = 1.0;
        set1.formSize = 15.0;
        
        NSArray *gradientColors = @[
                                    (id)[ChartColorTemplates colorFromString:@"#00ff0000"].CGColor,
                                    (id)[ChartColorTemplates colorFromString:@"#ffff0000"].CGColor
                                    ];
        CGGradientRef gradient = CGGradientCreateWithColors(nil, (CFArrayRef)gradientColors, nil);
        
        set1.fillAlpha = 1.f;
        set1.fill = [ChartFill fillWithLinearGradient:gradient angle:90.f];
        set1.drawFilledEnabled = YES;
        
        CGGradientRelease(gradient);
        
        NSMutableArray *dataSets = [[NSMutableArray alloc] init];
        [dataSets addObject:set1];
        
        LineChartData *data = [[LineChartData alloc] initWithDataSets:dataSets];
        
        _linechartView.data = data;
        
    }
    else if([self.titleLabel.text isEqualToString:@"Expense Analysis"])
    {
        float groupSpace = 0.04f;
        float barSpace = 0.02f;
        float barWidth = 0.46f;
        // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"
        
        NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
        
        
        int groupCount = count;
        int startYear = 0;
        int endYear = startYear + groupCount;
        
        for (int i = startYear; i < endYear; i++)
        {
            [yVals1 addObject:[[BarChartDataEntry alloc] initWithX:i yValues:@[@([[[graphArray valueForKey:@"OutstandingExpenses"]objectAtIndex:i] doubleValue]), @([[[graphArray valueForKey:@"ReimbursedExpenses"]objectAtIndex:i] doubleValue])] icon: [UIImage imageNamed:@"icon"]]];
            
            [yVals2 addObject:[[BarChartDataEntry alloc] initWithX:i yValues:@[@([[[graphArray valueForKey:@"OutstandingInterest"]objectAtIndex:i] doubleValue]), @([[[graphArray valueForKey:@"ReimbursedInterest"]objectAtIndex:i] doubleValue])] icon: [UIImage imageNamed:@"icon"]]];
            
            
        }
        
        BarChartDataSet *set1 = nil, *set2 = nil;
        /*if (_chartView.data.dataSetCount > 0)
         {
         set1 = (BarChartDataSet *)_chartView.data.dataSets[0];
         set2 = (BarChartDataSet *)_chartView.data.dataSets[1];
         
         set1.values = yVals1;
         set2.values = yVals2;
         
         
         BarChartData *data = _chartView.barData;
         
         _chartView.xAxis.axisMinimum = startYear;
         _chartView.xAxis.axisMaximum = [data groupWidthWithGroupSpace:groupSpace barSpace: barSpace] * xaxiscount + startYear;
         [data groupBarsFromX: startYear groupSpace: groupSpace barSpace: barSpace];
         
         [_chartView.data notifyDataChanged];
         [_chartView notifyDataSetChanged];
         }
         else
         {*/
        set1 = [[BarChartDataSet alloc] initWithValues:yVals1 label:@""];
        set1.colors = @[ChartColorTemplates.material[0], ChartColorTemplates.material[1]];
        set1.stackLabels = @[@"Outstanding Expenses", @"ARC SRO Amount"];
        
        set2 = [[BarChartDataSet alloc] initWithValues:yVals2 label:@""];
        set2.colors = @[ChartColorTemplates.material[2], ChartColorTemplates.material[4]];
        set2.stackLabels = @[@"Bank SRR Amount", @"Bank SRO Amount"];
        
        NSMutableArray *dataSets = [[NSMutableArray alloc] init];
        [dataSets addObject:set1];
        [dataSets addObject:set2];
        
        
        BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
        [data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:10.f]];
        // [data setValueFormatter:[[LargeValueFormatter alloc] init]];
        
        // specify the width each bar should have
        data.barWidth = barWidth;
        
        // restrict the x-axis range
        _resolutionChartView.xAxis.axisMinimum = startYear;
        
        // groupWidthWithGroupSpace(...) is a helper that calculates the width each group needs based on the provided parameters
        _resolutionChartView.xAxis.axisMaximum = startYear + [data groupWidthWithGroupSpace:groupSpace barSpace: barSpace] * groupCount;
        
        [data groupBarsFromX: startYear groupSpace: groupSpace barSpace: barSpace];
        
        _resolutionChartView.data = data;
        //  }
        
    }
    else if([self.titleLabel.text isEqualToString:@"Bid Win Vs Lost"])
    {
        float groupSpace = 0.04f;
        float barSpace = 0.02f;
        float barWidth = 0.46f;
        // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"
        
        NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
        
        
        int groupCount = count;
        int startYear = 0;
        int endYear = startYear + groupCount;
        
        for (int i = startYear; i < endYear; i++)
        {
            [yVals1 addObject:[[BarChartDataEntry alloc]
                               initWithX:i
                               y:[[[graphArray valueForKey:@"WonBids"]objectAtIndex:i] doubleValue]]];
            
            [yVals2 addObject:[[BarChartDataEntry alloc]
                               initWithX:i
                               y:[[[graphArray valueForKey:@"LostBids"]objectAtIndex:i] doubleValue]]];
            
            
            
            //        [yVals3 addObject:[[BarChartDataEntry alloc]
            //                           initWithX:i
            //                           y:(double) (arc4random_uniform(randomMultiplier))]];
            //
            //        [yVals4 addObject:[[BarChartDataEntry alloc]
            //                           initWithX:i
            //                           y:(double) (arc4random_uniform(randomMultiplier))]];
        }
        
        BarChartDataSet *set1 = nil, *set2 = nil;
        /*  if (_chartView.data.dataSetCount > 0)
         {
         set1 = (BarChartDataSet *)_chartView.data.dataSets[0];
         set2 = (BarChartDataSet *)_chartView.data.dataSets[1];
         set3 = (BarChartDataSet *)_chartView.data.dataSets[2];
         set4 = (BarChartDataSet *)_chartView.data.dataSets[3];
         set5 = (BarChartDataSet *)_chartView.data.dataSets[4];
         set6 = (BarChartDataSet *)_chartView.data.dataSets[5];
         set7 = (BarChartDataSet *)_chartView.data.dataSets[6];
         set8 = (BarChartDataSet *)_chartView.data.dataSets[7];
         set9 = (BarChartDataSet *)_chartView.data.dataSets[8];
         set10 = (BarChartDataSet *)_chartView.data.dataSets[9];
         
         set1.values = yVals1;
         set2.values = yVals2;
         set3.values = yVals3;
         set4.values = yVals4;
         set5.values = yVals5;
         set6.values = yVals6;
         set7.values = yVals7;
         set8.values = yVals8;
         set9.values = yVals9;
         set10.values = yVals10;
         
         BarChartData *data = _chartView.barData;
         
         _chartView.xAxis.axisMinimum = startYear;
         _chartView.xAxis.axisMaximum = [data groupWidthWithGroupSpace:groupSpace barSpace: barSpace] * xaxiscount + startYear;
         [data groupBarsFromX: startYear groupSpace: groupSpace barSpace: barSpace];
         [_chartView.data notifyDataChanged];
         [_chartView notifyDataSetChanged];
         }
         else
         {*/
        set1 = [[BarChartDataSet alloc] initWithValues:yVals1 label:@"Won"];
        [set1 setColor:[UIColor colorWithRed:104/255.f green:241/255.f blue:175/255.f alpha:1.f]];
        
        set2 = [[BarChartDataSet alloc] initWithValues:yVals2 label:@"Lost"];
        [set2 setColor:[UIColor colorWithRed:164/255.f green:228/255.f blue:251/255.f alpha:1.f]];
        
        
        
        NSMutableArray *dataSets = [[NSMutableArray alloc] init];
        [dataSets addObject:set1];
        [dataSets addObject:set2];
        
        
        BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
        [data setValueFont:[UIFont fontWithName:@"Avenir-Medium" size:10.f]];
        //        [data setValueFormatter:[[LargeValueFormatter alloc] init]];
        
        
        // specify the width each bar should have
        data.barWidth = barWidth;
        
        // restrict the x-axis range
        _chartView.xAxis.axisMinimum = startYear;
        
        // groupWidthWithGroupSpace(...) is a helper that calculates the width each group needs based on the provided parameters
        _chartView.xAxis.axisMaximum = startYear + [data groupWidthWithGroupSpace:groupSpace barSpace: barSpace] * groupCount;
        
        [data groupBarsFromX: startYear groupSpace: groupSpace barSpace: barSpace];
        
        _chartView.data = data;
        [_chartView.data notifyDataChanged];
        [_chartView notifyDataSetChanged];
        //  }
        
    }
    
    else
        
    {
        float groupSpace = 0.04f;
        float barSpace = 0.02f;
        float barWidth = 0.46f;
        // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"
        
        NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
        
        
        int groupCount = count;
        int startYear = 0;
        int endYear = startYear + groupCount;
        
        for (int i = startYear; i < endYear; i++)
        {
            [yVals1 addObject:[[BarChartDataEntry alloc] initWithX:i yValues:@[@([[[graphArray valueForKey:@"ARCSRRAmount"]objectAtIndex:i] doubleValue]), @([[[graphArray valueForKey:@"ARCSROAmount"]objectAtIndex:i] doubleValue])] icon: [UIImage imageNamed:@"icon"]]];
            
            [yVals2 addObject:[[BarChartDataEntry alloc] initWithX:i yValues:@[@([[[graphArray valueForKey:@"BankSRRAmount"]objectAtIndex:i] doubleValue]), @([[[graphArray valueForKey:@"BankSROAmount"]objectAtIndex:i] doubleValue])] icon: [UIImage imageNamed:@"icon"]]];
            
            
        }
        
        BarChartDataSet *set1 = nil, *set2 = nil;
        /*if (_chartView.data.dataSetCount > 0)
         {
         set1 = (BarChartDataSet *)_chartView.data.dataSets[0];
         set2 = (BarChartDataSet *)_chartView.data.dataSets[1];
         
         set1.values = yVals1;
         set2.values = yVals2;
         
         
         BarChartData *data = _chartView.barData;
         
         _chartView.xAxis.axisMinimum = startYear;
         _chartView.xAxis.axisMaximum = [data groupWidthWithGroupSpace:groupSpace barSpace: barSpace] * xaxiscount + startYear;
         [data groupBarsFromX: startYear groupSpace: groupSpace barSpace: barSpace];
         
         [_chartView.data notifyDataChanged];
         [_chartView notifyDataSetChanged];
         }
         else
         {*/
        set1 = [[BarChartDataSet alloc] initWithValues:yVals1 label:@""];
        set1.colors = @[ChartColorTemplates.material[0], ChartColorTemplates.material[1]];
        set1.stackLabels = @[@"ARC SRR Amount", @"ARC SRO Amount"];
        
        set2 = [[BarChartDataSet alloc] initWithValues:yVals2 label:@""];
        set2.colors = @[ChartColorTemplates.material[2], ChartColorTemplates.material[4]];
        set2.stackLabels = @[@"Bank SRR Amount", @"Bank SRO Amount"];
        
        NSMutableArray *dataSets = [[NSMutableArray alloc] init];
        [dataSets addObject:set1];
        [dataSets addObject:set2];
        
        
        BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
        [data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:10.f]];
        // [data setValueFormatter:[[LargeValueFormatter alloc] init]];
        
        // specify the width each bar should have
        data.barWidth = barWidth;
        
        // restrict the x-axis range
        _resolutionChartView.xAxis.axisMinimum = startYear;
        
        // groupWidthWithGroupSpace(...) is a helper that calculates the width each group needs based on the provided parameters
        _resolutionChartView.xAxis.axisMaximum = startYear + [data groupWidthWithGroupSpace:groupSpace barSpace: barSpace] * groupCount;
        
        [data groupBarsFromX: startYear groupSpace: groupSpace barSpace: barSpace];
        
        _resolutionChartView.data = data;
        //  }
    }
}



- (void)optionTapped:(NSString *)key
{
    [super handleOption:key forChartView:_chartView];
}

- (NSString * _Nonnull)stringForValue:(double)value axis:(ChartAxisBase * _Nullable)axis
{
    NSString *xAxisStringValue = @"";
    int myInt = (int)value;
    if(xVals.count > myInt)
    {
        xAxisStringValue = [xVals objectAtIndex:myInt];
        return xAxisStringValue;
    }
    else
    {
        return 0;
    }
}
- (NSString * _Nonnull)stringForDisplayValue:(double)value axis:(ChartAxisBase * _Nullable)axis
{
    NSString *xAxisStringValue = @"";
    int myInt = (int)value;
    if(xVals.count > myInt)
    {
        xAxisStringValue = [xVals objectAtIndex:myInt];
        return xAxisStringValue;
    }
    else
    {
        return 0;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return titleArray.count;
    
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"headerCell";
    DashboardDataCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.titleLabel.text = titleArray[indexPath.row];
    self.titleLabel.text = titleArray[0];
    
    if(indexPath.row == 0)
    {
        
        cell.selected = YES;
        [self.collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
        //  cell.bgView.hidden = NO;
        [cell.titleLabel setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:95.0/255.0 blue:70.0/255.0 alpha:1]];
    }
    else{
        cell.selected = NO;
        // cell.bgView.hidden = YES;
        [cell.titleLabel setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:95.0/255.0 blue:70.0/255.0 alpha:0.8]];
    }
    return cell;
    
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath

{
    DashboardDataCollectionViewCell *cell = (DashboardDataCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    [cell.titleLabel setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:95.0/255.0 blue:70.0/255.0 alpha:1]];
    self.titleLabel.text = titleArray[indexPath.row];
    //[self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    if(collectionView == self.collectionView)
        
    {
        if(indexPath.item == 0)
        {
            self.caseView.hidden = YES;
            self.chartView.hidden = YES;
            self.scrollView.hidden = NO;
            self.pimScrollView.hidden = YES;
            self.radioButtonView.hidden = YES;
            
            [self loadDataFromApi:[NSString stringWithFormat:@"/GetPIMStageChart?userid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]] :@"Progress"];
            
        }
        if(indexPath.item == 1)
        {
            self.caseView.hidden = YES;
            self.chartView.hidden = NO;
            self.scrollView.hidden = YES;
            self.pimScrollView.hidden = YES;
            self.radioButtonView.hidden = NO;
            
            [self loadDataFromApi:[NSString stringWithFormat:@"/GetConsolidatedPIMChart?userid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]] :@"Graph"];
            
        }
        if(indexPath.item == 2)
        {
            self.radioButtonView.hidden = YES;
            self.chartView.hidden = NO;
            self.caseView.hidden = YES;
            self.scrollView.hidden = YES;
            self.pimScrollView.hidden = YES;
            
            [self loadDataFromApi:[NSString stringWithFormat:@"/GetPimRejectionAnalysisChart?userid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]] :@"GraphPIMRejected"];
        }
        if(indexPath.item == 3)
        {
            self.radioButtonView.hidden = YES;
            self.chartView.hidden = NO;
            self.caseView.hidden = YES;
            self.scrollView.hidden = YES;
            self.pimScrollView.hidden = YES;
            
            [self loadDataFromApi:[NSString stringWithFormat:@"/GetBIDStatusChart?userid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]] :@"GraphBidStatus"];
        }
        
    }
    else{
        if(indexPath.item == 0)
        {
            self.resolutionRadioButtonView.hidden = NO;
            self.resolutionChartView.hidden = NO;
            self.srScrollView.hidden = YES;
            self.linechartView.hidden = YES;
            self.feeIncentiveScrollView.hidden = YES;
            self.roiTableView.hidden = YES;
            
            [self loadDataFromApi:[NSString stringWithFormat:@"/GetSRRecoveredVsOutstandingChart?userid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]] :@"GraphSRRecovered"];
        }
        else  if(indexPath.item == 2)
        {
            self.resolutionChartView.hidden = NO;
            self.linechartView.hidden = YES;
            self.resolutionRadioButtonView.hidden = YES;
            self.srScrollView.hidden = YES;
            self.feeIncentiveScrollView.hidden = YES;
            self.roiTableView.hidden = YES;
            
            [self loadDataFromApi:[NSString stringWithFormat:@"/GetExpenseChart?userid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]] :@"GraphExpenseAnalysis"];
        }
        else if(indexPath.item == 3)
        {
            
            self.resolutionChartView.hidden = YES;
            self.linechartView.hidden = NO;
            self.resolutionRadioButtonView.hidden = NO;
            self.roiTableView.hidden = YES;
            self.feeIncentiveScrollView.hidden = YES;
            self.srScrollView.hidden = YES;
            
            [self loadDataFromApi:[NSString stringWithFormat:@"/GetROIChart?userid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]] :@"GraphROI"];
        }
        else if(indexPath.item == 4)
        {
            self.resolutionRadioButtonView.hidden = YES;
            self.resolutionChartView.hidden = YES;
            self.srScrollView.hidden = YES;
            self.feeIncentiveScrollView.hidden = NO;
            self.linechartView.hidden = YES;
            self.roiTableView.hidden = YES;
        }
        else
        {
            self.resolutionRadioButtonView.hidden = YES;
            self.resolutionChartView.hidden = YES;
            self.srScrollView.hidden = YES;
            self.feeIncentiveScrollView.hidden = YES;
            self.linechartView.hidden = YES;
            self.roiTableView.hidden = YES;
        }
        
        
    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    DashboardDataCollectionViewCell *cell = (DashboardDataCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    [cell.titleLabel setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:95.0/255.0 blue:70.0/255.0 alpha:0.8]];
    // cell.bgView.hidden = YES;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGSize calCulateSizze =[(NSString*)[titleArray objectAtIndex:indexPath.row] sizeWithAttributes:NULL];
    calCulateSizze.width = calCulateSizze.width + 70;
    calCulateSizze.height = 28;
    return calCulateSizze;
}

- (IBAction)stackholderBtn:(id)sender {
    sharedManager.passingMode = @"Category wise companies";
    sharedManager.slideMenuSlected = @"Dashboard";
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DashBoardPieChartViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
    [self presentViewController:myNavController animated:YES completion:nil];
}
- (IBAction)actionplannerBtn:(id)sender {
    sharedManager.passingMode = @"Action Analysis(Self)";
    sharedManager.slideMenuSlected = @"Dashboard";
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DashBoardPieChartViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
    [self presentViewController:myNavController animated:YES completion:nil];
    
}
- (IBAction)meetingManagementBtn:(id)sender {
    sharedManager.passingMode = @"Meeting Action Analysis(Self)";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    sharedManager.slideMenuSlected = @"Dashboard";
    
    DashBoardPieChartViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
    [self presentViewController:myNavController animated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if(tableView == self.tableView || tableView == self.roiTableView)
    {
        return 1;
    }
    else{
        return 2;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(tableView == self.tableView)
    {
        return array.count;
    }
    if(tableView == self.roiTableView)
    {
        return graphArray.count;
    }
    else if(tableView == self.processedPimTable){
        if(section == 0)
        {
            return 1;
        }
        else
            return processedPimArray.count;
    }
    else if(tableView == self.feeIncentiveTable){
        if(section == 0)
        {
            return 1;
        }
        else
            return 2;
    }
    else{
        if(section == 0)
        {
            return 1;
        }
        else
            return graphArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.tableView)
    {
        DashboardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if(array.count > 0)
        {
            NSDictionary *dict = array[indexPath.row];
            cell.caseIdLabel.text = dict[@"CaseID"];
            cell.npaAccountLabel.text = dict[@"AccountName"];
        }
        return cell;
    }
    if(tableView == self.roiTableView)
    {
        ROITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"roiCell"];
        if(graphArray.count > 0)
        {
            NSDictionary *dict = graphArray[indexPath.row];
            cell.yearLabel.text = dict[@"Year"];
            cell.roiValueLabel.text = [dict[@"ROI"]stringValue];
        }
        return cell;
    }
    else if(tableView == self.processedPimTable){
        
        CustomisedTableViewCell *cell;
        if(indexPath.section == 0)
        {
            cell= [tableView dequeueReusableCellWithIdentifier:@"headerCell"];
            cell.bankNameHeaderText.text = @"Bank Name";
            cell.refNoHeaderText.text = @"Ref No";
            cell.accountNameHeaderText.text = @"Account";
            cell.ddHeaderText.text = @"DueDligence";
            cell.lddHeaderText.text = @"Legal DueDligence";
            cell.riskReportHeaderText.text = @"Risk Report";
            cell.ceoNoteHeaderText.text = @"CEO Note";
            cell.bidHeaderText.text = @"Bid";
            cell.trustHeaderText.text = @"Trust";
            cell.resolutionHeaderText.text = @"Resolution";
        }
        else if(indexPath.section == 1)
        {
            cell= [tableView dequeueReusableCellWithIdentifier:@"dataCell"];
            if(processedPimArray.count > 0)
            {
                NSDictionary *dict = processedPimArray[indexPath.row];
                cell.bankNameDataText.text = dict[@"BankName"];
                cell.refNoDataText.text = dict[@"ProjectCode"];
                cell.accountNameDataText.text = dict[@"AccountName"];
                cell.ddDataText.text = dict[@"DDStage"];
                cell.lddDataText.text = dict[@"LDDStage"];
                cell.riskReportDataText.text = dict[@"RiskStage"];
                cell.ceoNoteDataText.text = dict[@"CEONoteStage"];
                cell.bidDataText.text = dict[@"BidStage"];
                cell.trustDataText.text = dict[@"TrustStage"];
                cell.resolutionDataText.text = dict[@"RecoveryStage"];
                
                if([cell.ddDataText.text isEqualToString:@"Not Done"] )
                {
                    cell.ddDataText.backgroundColor = [UIColor colorWithRed:139.0/255.0 green:69.0/255.0 blue:19.0/255.0 alpha:1.0];
                }
                else{
                    cell.ddDataText.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:139.0/255.0 blue:34.0/255.0 alpha:1.0];
                }
                
                if([cell.lddDataText.text isEqualToString:@"Not Done"] )
                {
                    cell.lddDataText.backgroundColor = [UIColor colorWithRed:139.0/255.0 green:69.0/255.0 blue:19.0/255.0 alpha:1.0];
                }
                else{
                    cell.lddDataText.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:139.0/255.0 blue:34.0/255.0 alpha:1.0];
                }
                
                if([cell.riskReportDataText.text isEqualToString:@"Not Done"] )
                {
                    cell.riskReportDataText.backgroundColor = [UIColor colorWithRed:139.0/255.0 green:69.0/255.0 blue:19.0/255.0 alpha:1.0];
                }
                else{
                    cell.riskReportDataText.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:139.0/255.0 blue:34.0/255.0 alpha:1.0];
                }
                
                if([cell.ceoNoteDataText.text isEqualToString:@"Not Done"] )
                {
                    cell.ceoNoteDataText.backgroundColor = [UIColor colorWithRed:139.0/255.0 green:69.0/255.0 blue:19.0/255.0 alpha:1.0];
                }
                else{
                    cell.ceoNoteDataText.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:139.0/255.0 blue:34.0/255.0 alpha:1.0];
                }
                
                if([cell.bidDataText.text isEqualToString:@"Not Done"] )
                {
                    cell.bidDataText.backgroundColor = [UIColor colorWithRed:139.0/255.0 green:69.0/255.0 blue:19.0/255.0 alpha:1.0];
                }
                else if([cell.bidDataText.text isEqualToString:@"Lost"] ){
                    cell.bidDataText.backgroundColor = [UIColor colorWithRed:205.0/255.0 green:92.0/255.0 blue:92.0/255.0 alpha:1.0];
                }
                else{
                    cell.bidDataText.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:139.0/255.0 blue:34.0/255.0 alpha:1.0];
                }
                
                if([cell.trustDataText.text isEqualToString:@"Not Done"] )
                {
                    cell.trustDataText.backgroundColor = [UIColor colorWithRed:139.0/255.0 green:69.0/255.0 blue:19.0/255.0 alpha:1.0];
                }
                else{
                    cell.trustDataText.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:139.0/255.0 blue:34.0/255.0 alpha:1.0];
                }
                
                if([cell.resolutionDataText.text isEqualToString:@"Not Started"] )
                {
                    cell.resolutionDataText.backgroundColor = [UIColor colorWithRed:139.0/255.0 green:69.0/255.0 blue:19.0/255.0 alpha:1.0];
                }
                else{
                    cell.resolutionDataText.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:139.0/255.0 blue:34.0/255.0 alpha:1.0];
                }
            }
        }
        return cell;
    }
    else if(tableView == self.consolidatedPimTable){
        CustomisedTableViewCell *cell;
        if(indexPath.section == 0)
        {
            cell= [tableView dequeueReusableCellWithIdentifier:@"headerCell"];
            cell.bankNameHeaderText.text = @"Bank Name";
            cell.recievedPimHeaderText.text = @"Received PIMs";
            cell.referredPimHeaderText.text = @"Refered PIMs";
            cell.shortListedPimHeaderText.text = @"Shortlisted PIMs";
            cell.rejectedPimHeaderText.text = @"Rejected PIMs";
            cell.totalDdHeaderText.text = @"Total DDs";
            cell.totalLddHeaderText.text = @"Total LDDs";
            cell.totalRiskMgmtHeaderText.text = @"Total Risk Management";
            cell.bidWonHeaderText.text = @"BID Won Acccounts";
            cell.bidLossHeaderText.text = @"BID Loss Acccounts";
            cell.acquiredAcctHeaderText.text = @"Acquired Acccounts";
            
        }
        else if(indexPath.section == 1)
        {
            cell= [tableView dequeueReusableCellWithIdentifier:@"dataCell"];
            
            if(graphArray.count > 0)
            {
                NSDictionary *dict = graphArray[indexPath.row];
                cell.bankNameDataText.text = dict[@"BankName"];
                cell.recievedPimDataText.text = [dict[@"TotalPIMCount"]stringValue];
                cell.referredPimDataText.text = [dict[@"ReferPIMCount"]stringValue];
                cell.shortListedPimDataText.text = [dict[@"ShortlistPIMCount"]stringValue];
                cell.rejectedPimDataText.text = [dict[@"RejectPIMCount"]stringValue];
                cell.totalDdDataText.text = [dict[@"TotalDDCount"]stringValue];
                cell.totalLddDataText.text = [dict[@"TotalLDDCount"]stringValue];
                cell.totalRiskMgmtDataText.text = [dict[@"TotalRMCount"]stringValue];
                cell.bidWonDataText.text = [dict[@"TotalBidWonCount"]stringValue];
                cell.bidLossDataText.text = [dict[@"TotalBidLossCount"]stringValue];
                cell.acquiredAcctDataText.text = [dict[@"TotalAcquiredCount"]stringValue];
            }
            
        }
        return cell;
    }
    else if(tableView == self.feeIncentiveTable)
    {
        CustomisedTableViewCell *cell;
        if(indexPath.section == 0)
        {
            cell= [tableView dequeueReusableCellWithIdentifier:@"headerCell"];
            cell.trustNameHeaderText.text = @"Trust";
            cell.srInvestmentHeaderText.text = @"SR Investment";
            cell.srRecoveredHeaderText.text = @"SR Recovered";
            cell.srOutstandingHeaderText.text = @"SR Outstanding";
            cell.mgmtFeeExpHeaderText.text = @"Mgmt Fee (Expected)";
            cell.mgmtFeeRecHeaderText.text = @"Mgmt Fee (Recieved)";
            cell.incentiveRecievedHeaderText.text = @"Incentive Received";
            cell.expenseReimbursedHeaderText.text = @"Expense Reimbursed";
            cell.expenseOutstandingHeaderText.text = @"Expense Outstanding";
            cell.interestReimbursedHeaderText.text = @"Interest Reimbursed";
            cell.interestOutstandingHeaderText.text = @"Interest Outstanding";
            
        }
        else if(indexPath.section == 1)
        {
            cell= [tableView dequeueReusableCellWithIdentifier:@"dataCell"];
            cell.trustNameDataText.text = @"";
            cell.srInvestmentDataText.text = @"";
            cell.srRecoveredDataText.text = @"";
            cell.srOutstandingDataText.text = @"";
            cell.mgmtFeeExpDataText.text = @"";
            cell.mgmtFeeRecDataText.text = @"";
            cell.incentiveRecievedDataText.text = @"";
            cell.expenseReimbursedDataText.text = @"";
            cell.expenseOutstandingDataText.text = @"";
            cell.interestReimbursedDataText.text = @"";
            cell.interestOutstandingDataText.text = @"";
            
        }
        return cell;
    }
    else{
        CustomisedTableViewCell *cell;
        if(indexPath.section == 0)
        {
            cell= [tableView dequeueReusableCellWithIdentifier:@"headerCell"];
            cell.bankNameHeaderText.text = @"Bank Name";
            cell.trustNameHeaderText.text = @"Trust Name";
            cell.noOfAccountsText.text = @"No of Accounts";
            cell.arcSriText.text = @"ARC SRI (Cr.)";
            cell.arcSrrText.text = @"ARC SRR (Cr.)";
            cell.arcSroText.text = @"ARC SRO (Cr.)";
            cell.businessSriText.text = @"Bank SRI (Cr.)";
            cell.businessSrrText.text = @"Bank SRR (Cr.)";
            cell.businessSroText.text = @"Bank SRO (Cr.)";
            
            
        }
        else if(indexPath.section == 1)
        {
            cell= [tableView dequeueReusableCellWithIdentifier:@"dataCell"];
            
            if(graphArray.count > 0)
            {
                NSDictionary *dict = graphArray[indexPath.row];
                cell.bankNameDataText.text = dict[@"BankName"];
                cell.trustNameDataText.text = dict[@"TrustName"];
                cell.noOfAccountsDataText.text = [dict[@"AccountCount"]stringValue];
                cell.arcSriDataText.text = [dict[@"ARCSRIAmount"]stringValue];
                cell.arcSrrDataText.text = [dict[@"ARCSRRAmount"]stringValue];
                cell.arcSroDataText.text = [dict[@"ARCSROAmount"]stringValue];
                cell.businessSriDataText.text = [dict[@"BankSRIAmount"]stringValue];
                cell.businessSrrDataText.text = [dict[@"BankSRRAmount"]stringValue];
                cell.businessSroDataText.text = [dict[@"BankSROAmount"]stringValue];
                
            }
            
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.tableView)
    {
        
        NSDictionary *dict = array[indexPath.row];
        DashboardDetailViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardDetailViewController"];
        vc.dict = [dict mutableCopy];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.tableView)
    {
        return 77;
    }
    else{
        return 43;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == self.roiTableView){
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width/2, 40)];
        [label1 setFont:[UIFont boldSystemFontOfSize:16]];
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Year"];
        [attributeString addAttribute:NSUnderlineStyleAttributeName
                                value:[NSNumber numberWithInt:1]
                                range:(NSRange){0,[attributeString length]}];
        label1.attributedText = attributeString;
        label1.textAlignment = NSTextAlignmentCenter;
        
        
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(view.frame.size.width/2, 0, view.frame.size.width/2, 40)];
        [label2 setFont:[UIFont boldSystemFontOfSize:16]];
        NSMutableAttributedString *attributeString2 = [[NSMutableAttributedString alloc] initWithString:@"ROI (%)"];
        [attributeString2 addAttribute:NSUnderlineStyleAttributeName
                                 value:[NSNumber numberWithInt:1]
                                 range:(NSRange){0,[attributeString2 length]}];
        label2.attributedText = attributeString2;
        label2.textAlignment = NSTextAlignmentCenter;
        
        
        [view addSubview:label1];
        [view addSubview:label2];
        [view setBackgroundColor:[UIColor whiteColor]];
        return view;
        
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(tableView == self.roiTableView)
    {
        return 40;
    }
    return 0;
}


-(void)loadDataFromApi :(NSString *)url :(NSString *)type{
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[WebServices sharedInstance] getWithParameter:nil withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
            NSLog(@"response izz %@",responseObject);
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [xVals removeAllObjects];
                
                if([type isEqualToString:@"Case"])
                {
                    self->array = responseObject;
                    if(self->array.count > 0 )
                    {
                        [self.tableView reloadData];
                        [self removeNoDataFound:self.view];
                    }
                    else{
                        [self showNoDataFound:self.view];
                    }
                }
                else if([type isEqualToString:@"Progress"]){
                    self->processedPimArray = responseObject;
                    if(self->processedPimArray.count > 0 )
                    {
                        [self.processedPimTable reloadData];
                        [self removeNoDataFound:self.view];
                    }
                    else{
                        [self showNoDataFound:self.view];
                    }
                }
                else{
                    
                    if([type isEqualToString:@"Graph"])
                    {
                        self->graphArray = responseObject;
                        if(self->graphArray.count > 0 )
                        {
                            [self removeNoDataFound:self.view];
                            for (int i=0;i<[graphArray count];i++)
                            {
                                [xVals addObject:[[graphArray valueForKey:@"BankName"]objectAtIndex:i]];
                                
                            }
                            NSLog(@"%@",xVals);
                            [self updateChartData];
                            
                            self.caseView.hidden = YES;
                            self.chartView.hidden = NO;
                            self.scrollView.hidden = YES;
                        }
                        else{
                            [self showNoDataFound:self.view];
                        }
                    }
                    else if([type isEqualToString:@"GraphPIMRejected"])
                    {
                        self->graphArray = responseObject;
                        if(self->graphArray.count > 0 )
                        {
                            [self removeNoDataFound:self.view];
                            // for (int i=0;i<[graphArray count];i++)
                            // {
                            NSMutableArray *arr = [NSMutableArray array];
                            arr = [[graphArray valueForKey:@"details"]objectAtIndex:0];
                            
                            for(int k=0 ; k<arr.count;k++)
                            {
                                [xVals addObject:[[arr valueForKey:@"Parameter"]objectAtIndex:k]];
                            }
                            // }
                            NSLog(@"%@",xVals);
                            [self updateChartData];
                            
                            self.caseView.hidden = YES;
                            self.chartView.hidden = NO;
                            self.scrollView.hidden = YES;
                        }
                        else{
                            [self showNoDataFound:self.view];
                        }
                    }
                    else if([type isEqualToString:@"GraphExpenseAnalysis"])
                    {
                        self->graphArray = responseObject;
                        if(self->graphArray.count > 0 )
                        {
                            [self removeNoDataFound:self.view];
                            for (int i=0;i<[graphArray count];i++)
                            {
                                [xVals addObject:[[graphArray valueForKey:@"TrustName"]objectAtIndex:i]];
                                
                            }
                            NSLog(@"%@",xVals);
                            [self updateChartData];
                            
                            self.caseView.hidden = YES;
                            self.chartView.hidden = YES;
                            self.scrollView.hidden = YES;
                            self.resolutionChartView.hidden = NO;
                            
                        }
                        else{
                            [self showNoDataFound:self.view];
                        }
                        
                    }
                    else if([type isEqualToString:@"GraphBidStatus"])
                    {
                        self->graphArray = responseObject;
                        if(self->graphArray.count > 0 )
                        {
                            [self removeNoDataFound:self.view];
                            for (int i=0;i<[graphArray count];i++)
                            {
                                [xVals addObject:[[graphArray valueForKey:@"BankName"]objectAtIndex:i]];
                                
                            }
                            NSLog(@"%@",xVals);
                            [self updateChartData];
                            
                            self.caseView.hidden = YES;
                            self.chartView.hidden = NO;
                            self.scrollView.hidden = YES;
                            self.resolutionChartView.hidden = YES;
                            
                        }
                        else{
                            [self showNoDataFound:self.view];
                        }
                        
                    }
                    else if([type isEqualToString:@"GraphROI"])
                    {
                        self->graphArray = responseObject;
                        if(self->graphArray.count > 0 )
                        {
                            [self removeNoDataFound:self.view];
                            for (int i=0;i<[graphArray count];i++)
                            {
                                [xVals addObject:[[graphArray valueForKey:@"Year"]objectAtIndex:i]];
                                
                            }
                            NSLog(@"%@",xVals);
                            [self updateChartData];
                            
                            self.caseView.hidden = YES;
                            self.chartView.hidden = YES;
                            self.scrollView.hidden = YES;
                            self.resolutionChartView.hidden = YES;
                            self.linechartView.hidden = NO;
                            
                        }
                        else{
                            [self showNoDataFound:self.view];
                        }
                        
                    }
                    else{
                        self->graphArray = responseObject;
                        if(self->graphArray.count > 0 )
                        {
                            [self removeNoDataFound:self.view];
                            for (int i=0;i<[graphArray count];i++)
                            {
                                [xVals addObject:[[graphArray valueForKey:@"TrustName"]objectAtIndex:i]];
                                
                            }
                            NSLog(@"%@",xVals);
                            [self updateChartData];
                            
                            self.caseView.hidden = YES;
                            self.chartView.hidden = YES;
                            self.scrollView.hidden = YES;
                            self.resolutionChartView.hidden = NO;
                            
                        }
                        else{
                            [self showNoDataFound:self.view];
                        }
                    }
                }
            });
        }];
    }else{
        
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}



-(void)showNoDataFound :(UIView *)view
{
    self.tableView.hidden = YES;
    if(![noDataLabel isDescendantOfView:view])
    {
        [view addSubview:noDataLabel];
    }
    [noDataLabel setFont:[UIFont systemFontOfSize:20]];
    noDataLabel.textAlignment=NSTextAlignmentCenter;
    noDataLabel.layer.masksToBounds  = YES;
    noDataLabel.layer.shadowOpacity  = 2.5;
    noDataLabel.layer.shadowColor    = [[UIColor grayColor] CGColor];
    noDataLabel.layer.shadowOffset   = CGSizeMake(0, 1);
    noDataLabel.layer.shadowRadius   = 2;
    noDataLabel.text = @"No Data Found";
}

-(void)removeNoDataFound :(UIView *)view
{
    self.tableView.hidden = NO;
    [noDataLabel removeFromSuperview];
}

- (IBAction)upcomingButtonClick:(UIButton *)sender {
    
    for ( int i=0; i < [self.ButtonArray count]; i++) {
        [[self.ButtonArray objectAtIndex:i] setImage:[UIImage
                                                      imageNamed:@"radio-off-button.png"]
                                            forState:UIControlStateNormal];
    }
    [sender setImage:[UIImage imageNamed:@"radio-on-button.png"]
            forState:UIControlStateNormal];
    
    if(sender.tag == 1)
    {
        [self loadDataFromApi:[NSString stringWithFormat:@"/GetChartProgress?userid=%@&type=1",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]] :@"Case"];
    }
    else if(sender.tag == 2){
        [self loadDataFromApi:[NSString stringWithFormat:@"/GetChartProgress?userid=%@&type=2",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]] :@"Case"];
    }
}

- (IBAction)showHideChartButtonClick:(UIButton *)sender {
    
    for ( int i=0; i < [self.chartButtonArray count]; i++) {
        [[self.chartButtonArray objectAtIndex:i] setImage:[UIImage
                                                           imageNamed:@"radio-off-button.png"]
                                                 forState:UIControlStateNormal];
    }
    [sender setImage:[UIImage imageNamed:@"radio-on-button.png"]
            forState:UIControlStateNormal];
    
    if(sender.tag == 3)
    {
        if([self.titleLabel.text isEqualToString:@"Consolidated PIMs"])
        {
            self.chartView.hidden = NO;
            self.pimScrollView.hidden = YES;
            self.srScrollView.hidden = YES;
        }
        else if([self.titleLabel.text isEqualToString:@"SR Recovered Vs Outstanding"])
        {
            self.chartView.hidden = NO;
            self.pimScrollView.hidden = YES;
            self.srScrollView.hidden = YES;
        }
        
    }
    else if(sender.tag == 4){
        if([self.titleLabel.text isEqualToString:@"Consolidated PIMs"])
        {
            self.chartView.hidden = YES;
            self.pimScrollView.hidden = NO;
            self.srScrollView.hidden = YES;
            [self.consolidatedPimTable reloadData];
        }
        else if([self.titleLabel.text isEqualToString:@"SR Recovered Vs Outstanding"])
        {
            self.chartView.hidden = YES;
            self.pimScrollView.hidden = YES;
            self.srScrollView.hidden = NO;
            [self.srTableView reloadData];
        }
    }
    else if(sender.tag == 5){
        self.srScrollView.hidden = YES;
        self.chartView.hidden = NO;
    }
    else if(sender.tag == 6){
        self.srScrollView.hidden = NO;
        self.chartView.hidden = YES;
    }
}

- (IBAction)resolutionRadioButtonClick:(UIButton *)sender {
    
    for ( int i=0; i < [self.radioButtonArray count]; i++) {
        [[self.radioButtonArray objectAtIndex:i] setImage:[UIImage
                                                           imageNamed:@"radio-off-button.png"]
                                                 forState:UIControlStateNormal];
    }
    [sender setImage:[UIImage imageNamed:@"radio-on-button.png"]
            forState:UIControlStateNormal];
    
    if([self.titleLabel.text isEqualToString:@"SR Redemption"])
    {
        
        if(sender.tag == 5){
            self.srScrollView.hidden = YES;
            self.resolutionChartView.hidden = NO;
        }
        else if(sender.tag == 6){
            self.srScrollView.hidden = NO;
            self.resolutionChartView.hidden = YES;
            [self.srTableView reloadData];
        }
    }
    else{
        if(sender.tag == 5){
            self.srScrollView.hidden = YES;
            self.resolutionChartView.hidden = YES;
            self.linechartView.hidden = NO;
            self.roiTableView.hidden = YES;
        }
        else if(sender.tag == 6)
        {
            self.srScrollView.hidden = YES;
            self.resolutionChartView.hidden = YES;
            self.linechartView.hidden = YES;
            self.roiTableView.hidden = NO;
            [self.roiTableView reloadData];
        }
    }
}


- (IBAction)caseButtonClick:(id)sender {
    self.titleLabel.hidden = YES;
    
    self.caseButtonView.hidden = NO;
    self.preAcqButtonView.hidden = YES;
    self.resolutionButtonView.hidden = YES;
    
    self.resolutionView.hidden = YES;
    self.caseView.hidden = NO;
    self.preAcquisitionView.hidden = YES;
    
   // [self.tableView reloadData];
}

- (IBAction)preAcqButtonClick:(id)sender {
    self.titleLabel.hidden = NO;
    titleArray = [NSMutableArray arrayWithObjects:@"PIM Stage Analysis",@"Consolidated PIMs",@"PIM Rejection Analysis",@"Bid Win Vs Lost", nil];
    
    [self.collectionView reloadData];
    
    self.caseButtonView.hidden = YES;
    self.preAcqButtonView.hidden = NO;
    self.resolutionButtonView.hidden = YES;
    
    self.caseView.hidden = YES;
    self.preAcquisitionView.hidden = NO;
    self.resolutionView.hidden = YES;
    
    self.scrollView.hidden = NO;
    self.pimScrollView.hidden = YES;
    self.chartView.hidden = YES;
    
    [self loadDataFromApi:[NSString stringWithFormat:@"/GetPIMStageChart?userid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]] :@"Progress"];
}

- (IBAction)resolutionButtonClick:(id)sender {
    self.titleLabel.hidden = NO;
    
    titleArray = [NSMutableArray arrayWithObjects:@"SR Redemption",@"Recovery Analysis",@"Expense Analysis",@"ROI",@"Fee & Incentive Analysis", nil];
    [self.resolutionCollectionView reloadData];
    
    self.caseButtonView.hidden = YES;
    self.preAcqButtonView.hidden = YES;
    self.resolutionButtonView.hidden = NO;
    
    self.resolutionView.hidden = NO;
    self.caseView.hidden = YES;
    self.preAcquisitionView.hidden = YES;
    
    self.resolutionChartView.hidden = NO;
    self.srScrollView.hidden = YES;
    self.linechartView.hidden = YES;
    
    [self loadDataFromApi:[NSString stringWithFormat:@"/GetSRRecoveredVsOutstandingChart?userid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]] :@"GraphSRRecovered"];
}
@end
