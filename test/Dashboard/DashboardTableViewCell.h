//
//  DashboardTableViewCell.h
//  test
//
//  Created by ceazeles on 23/10/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DashboardTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *caseIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *npaAccountLabel;

@end

NS_ASSUME_NONNULL_END
