//
//  DashboardDetailViewController.m
//  test
//
//  Created by ceazeles on 27/10/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "DashboardDetailViewController.h"

@interface DashboardDetailViewController ()

@end

@implementation DashboardDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 13 ;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomTableViewCell *cell;
    dictionary = self.dict;
    if(indexPath.section == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"caseIdCell"];
        //cell.accountName.text = dataDict[@"AccountName"];
        cell.caseIdText.text = dictionary[@"CaseID"];
        cell.caseIdText .layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
    }
    if(indexPath.section == 1)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"accountNameCell"];
        //if(dataDict.count>0)
        //{
        cell.accountName.text = dictionary[@"AccountName"];
        cell.accountName .layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        //}
        
    }
    if(indexPath.section == 2)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"assetCell"];
        // if(dataDict.count>0)
        // {
        cell.assetNameText.text = dictionary[@"AssetName"];
        cell.assetNameText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
        
    }
    if(indexPath.section == 3)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"hearingDateCell"];
        if(![[dictionary valueForKey:@"HearingDate"] isEqual:[NSNull null]])
        {
            
            cell.hearingDateText.text = dictionary[@"HearingDate"];
            cell.hearingDateText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
        
    }
    if(indexPath.section == 4)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"nextHearingDateCell"];
        // if(dataDict.count>0)
        // {
        cell.nextHearingDateText.text = dictionary[@"NextHearingDate"];
        cell.nextHearingDateText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
        
    }
    
    if(indexPath.section == 5)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"defenderCell"];
        // if(dataDict.count>0)
        // {
        cell.defenderText.text = dictionary[@"Defender"];
        cell.defenderText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 6)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"advocateCell"];
        if(![[dictionary valueForKey:@"Advocate"] isEqual:[NSNull null]])
        {
            
            cell.advocateNameText.text = dictionary[@"Advocate"];
            cell.advocateNameText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    if(indexPath.section == 7)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"phoneCell"];
        // if(dataDict.count>0)
        // {
        cell.advocatePhoneText.text = dictionary[@"Phone"];
        cell.advocatePhoneText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 8)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"emailCell"];
        // if(dataDict.count>0)
        // {
        cell.advocateEmailText.text = dictionary[@"Email"];
        cell.advocateEmailText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 9)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"asOnDateCell"];
        // if(dataDict.count>0)
        // {
        cell.asOnDateText.text = dictionary[@"CaseProgressAsOnDate"];
        cell.asOnDateText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 10)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"asOnPreviousDateCell"];
        // if(dataDict.count>0)
        // {
        cell.asOnPreviousDateText.text = dictionary[@"AsOnPreviousHearing"];
        cell.asOnPreviousDateText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 11)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"descForNextCell"];
        // if(dataDict.count>0)
        // {
        cell.descHearingText.text = dictionary[@"DescriptionForNextHearing"];
        cell.descHearingText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 12)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"reviewProgressCell"];
        // if(dataDict.count>0)
        // {
        cell.reviewProgressText.text = dictionary[@"ReviewProgress"];
        cell.reviewProgressText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    
    return cell;
}

- (IBAction)backbuttonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
