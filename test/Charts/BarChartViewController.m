
#import "BarChartViewController.h"
#import "test-Swift.h"
#import "IntAxisValueFormatter.h"
#import "MySharedManager.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"

@interface BarChartViewController () <ChartViewDelegate,IChartAxisValueFormatter>

@property (weak, nonatomic) IBOutlet BarChartView *chartView;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;


@end

@implementation BarChartViewController
{
    NSMutableArray *dataArray;
    int yaxisVal;
    NSMutableArray *xVals;
    NSMutableArray *markernamesarray;
    NSString *statusString;
    __weak IBOutlet UIButton *menuBtn;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:YES];

    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    statusString = @"";
    [self loadDataFromApi];

    dataArray=[NSMutableArray new];
    xVals=[NSMutableArray new];
    markernamesarray=[NSMutableArray new];
    [self setupBarLineChartView:_chartView];

    _chartView.delegate = self;
    _chartView.chartDescription.enabled = NO;
    _chartView.pinchZoomEnabled = YES;
    _chartView.drawBarShadowEnabled = NO;
    _chartView.drawGridBackgroundEnabled = NO;
    _chartView.drawValueAboveBarEnabled=YES;
    _chartView.doubleTapToZoomEnabled=NO;
    _chartView.legend.enabled = NO;
    [_chartView zoomIn];
    [_chartView zoomWithScaleX:0.5 scaleY:0 x:0 y:0];
//    [_chartView setScaleMinima:0 scaleY:0.5];
//        [_chartView setExtraOffsetsWithLeft:0 top:0 right:20 bottom:0];

    ChartXAxis *xAxis = _chartView.xAxis;
    xAxis.labelFont = [UIFont fontWithName:@"Avenir-Medium" size:10.f];
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelRotationAngle=60;
    xAxis.granularity = 1.f;
    xAxis.axisRange = 1;
    xAxis.centerAxisLabelsEnabled = NO;
    xAxis.valueFormatter = self;

    ChartYAxis *leftAxis = _chartView.leftAxis;
    leftAxis.enabled=YES;
    leftAxis.labelFont = [UIFont fontWithName:@"Avenir-Medium" size:10.f];
    leftAxis.drawGridLinesEnabled = YES;
    leftAxis.spaceTop = 0;
    leftAxis.axisMinimum = 0;
    _chartView.leftAxis.axisMinimum = 0.0;


    XYMarkerView *marker = [[XYMarkerView alloc]
                                  initWithColor: [UIColor colorWithWhite:180/255. alpha:1.0]
                                  font: [UIFont systemFontOfSize:12.0]
                                  textColor: UIColor.whiteColor
                                  insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0)
                                  xAxisValueFormatter: _chartView.xAxis.valueFormatter];
    marker.chartView = _chartView;
    marker.minimumSize = CGSizeMake(80.f, 40.f);
    _chartView.marker = marker;
 
}
-(void)viewWillAppear:(BOOL)animated{
    [self viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self loadDataFromApi];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateChartData
{
    if (self.shouldHideData)
    {
        _chartView.data = nil;
        return;
    }
     int xaxiscount=(int)[dataArray count];
    [self setDataCount:xaxiscount range:yaxisVal];
}

- (void)setDataCount:(int)count range:(double)range
{
    
    NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < dataArray.count; i++)
    {
        [yVals1 addObject:[[BarChartDataEntry alloc]
                           initWithX:i
                           y:[[[dataArray objectAtIndex:i] objectForKey:@"GroupCount"] doubleValue]]];
    }
    
    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithValues:yVals1 label:@"Planned Duration"];
    [set1 setColor:[UIColor colorWithRed:104/255.f green:241/255.f blue:175/255.f alpha:1.f]];
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:set1];
        BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
        data.barWidth = 0.9f;
        _chartView.data = data;
//     _chartView.fitBars = YES;
    [_chartView.data notifyDataChanged];
    [_chartView notifyDataSetChanged];
    
}

- (void)optionTapped:(NSString *)key
{
    [super handleOption:key forChartView:_chartView];
}
- (NSString * _Nonnull)stringForValue:(double)value axis:(ChartAxisBase * _Nullable)axis
{
    NSString *xAxisStringValue = @"";
    int myInt = (int)value;
    if(markernamesarray.count > myInt)
    {
        xAxisStringValue = [markernamesarray objectAtIndex:myInt];
        return xAxisStringValue;
    }
    else
    {
        return 0;
    }
}
- (IBAction)statusButton:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select" message:nil preferredStyle:UIAlertControllerStyleAlert];
    // create the actions handled by each button
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"All" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        statusString = @"";
        [self.statusButton setTitle:@"All" forState:UIControlStateNormal];
        [self loadDataFromApi];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Conducted" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.statusButton setTitle:@"Conducted" forState:UIControlStateNormal];
        statusString = @"Publish";
        [self loadDataFromApi];
    }];
    
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"Not conducted" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.statusButton setTitle:@"Not conducted" forState:UIControlStateNormal];
        statusString = @"Pending";
        [self loadDataFromApi];
    }];
    
    
    // add actions to our sheet
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    [actionSheet addAction:action3];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        // Called when user taps outside
    }]];
    // bring up the action sheet
    [self presentViewController:actionSheet animated:YES completion:nil];
}

-(void)loadDataFromApi{
    
    if([Utitlity isConnectedTointernet]){
        
        [self showProgress];
        NSMutableURLRequest *urlRequest;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if ([statusString isEqualToString:@""]) {
            urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@GroupWiseMeetingCount?userid=%@",BASEURL,[defaults objectForKey:@"UserID"]]]];
        }
        else{
           urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@GroupWiseMeetingCount?userid=%@&Status=%@",BASEURL,[defaults objectForKey:@"UserID"],statusString]]];
        }
        
            
        NSLog(@"urlRequest------%@",urlRequest);
        
        //create the Method "GET"
        [urlRequest setHTTPMethod:@"GET"];
        
        NSURLSession *session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                              if(httpResponse.statusCode == 200)
                                              {
                                                  NSLog(@"data------%@",data);
                                                  
                                                  NSError *parseError = nil;
                      
                                                  dataArray = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError] mutableCopy];

                                                  NSLog(@"dataArray---%@",dataArray);

                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [self hideProgress];
                                                      [xVals removeAllObjects];
                                                      [markernamesarray removeAllObjects];

                                                      for (int i=0;i<dataArray.count;i++)
                                                      {
                                                          [markernamesarray addObject:[[dataArray objectAtIndex:i] objectForKey:@"GroupDescription"]];
                                                          [xVals addObject:[[dataArray objectAtIndex:i] objectForKey:@"GroupCount"]];
                                                      }
                                                      NSLog(@"%@",xVals);
                                                      [self updateChartData];
                                                  });
                                                  
                                              }
                                              else
                                              {
                                                  NSLog(@"Error");
                                              }
                                          }];
        [dataTask resume];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)showProgress{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}

-(void)hideProgress{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}
@end
