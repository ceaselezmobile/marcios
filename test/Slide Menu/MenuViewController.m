
#import "MenuViewController.h"
#import "ProfileTableViewCell.h"
#import "ViewController.h"
#import "UIView+Toast.h"
#import "SWRevealViewController.h"
#import "MySharedManager.h"
#import "Utitlity.h"
@implementation SWUITableViewCell

@end

@implementation MenuViewController

{
    long selectedSection;
    BOOL mitrPlusClicked;
    BOOL stackholderClicked;
    BOOL actionClicked;
    BOOL meetingClicked;
    BOOL arcClicked;
    BOOL dataQueryClicked;
    BOOL actionDashboardClicked;
    BOOL adhocMeetingClicked;
    BOOL meetingActionClicked;
    BOOL meetingDashboardClicked;
    BOOL meetingDocumentClicked;
    BOOL preAcquistionSelection;
    BOOL acquistionSelection;
    BOOL resolutionSelection;
    
    MySharedManager *sharedManager;
    
    long selectedRow;
    IBOutlet UITableView *dataTable;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (mitrPlusClicked) {
        return 8;
    }
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == 1 && stackholderClicked) {
        if (dataQueryClicked) {
            return 3;
        }
        
        return 2;
    }
    else if (section == 3 && actionClicked && mitrPlusClicked) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"IsExternal"] isEqualToString:@"Yes"]) {
            
            if(actionDashboardClicked){
                return 5;
            }
            return 2;
        }
        else if (actionDashboardClicked) {
            return 7;
        }
        return 3;
    }
    else if (section == 4 && meetingClicked && mitrPlusClicked) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"IsExternal"] isEqualToString:@"Yes"]) {
            int rows = 0;
            if (meetingActionClicked) {
                rows += 1;
            }
            if (meetingDashboardClicked) {
                rows += 2;
            }
            if (meetingDocumentClicked) {
                rows += 1;
            }
            return 3+rows;
        }
        else{
            int rows = 0;
            if (adhocMeetingClicked) {
                rows += 3;
            }
            if (meetingActionClicked) {
                rows += 2;
            }
            if (meetingDashboardClicked) {
                rows += 4;
            }
            if (meetingDocumentClicked) {
                rows += 2;
            }
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"IsMOMApprover"] isEqualToString:@"YES"] ) {
                rows += 1;
            }
            return 4+rows;
        }
        return 3;
    }
    if ((section == 5 && arcClicked && mitrPlusClicked) || (!mitrPlusClicked && section == 3 && arcClicked)) {
        int rows = 4;
        if (preAcquistionSelection) {
            rows += 9;
        }
         if(acquistionSelection){
           rows += 1;
        }
        if(resolutionSelection){
            rows += 6;
        }
        return rows;
    }
    return 0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    sharedManager = [MySharedManager sharedManager];
    sharedManager.slideMenuSlected = @"yes";
    if (indexPath.section == 1 && indexPath.row == 1) {
        if (dataQueryClicked) {
            dataQueryClicked = NO;
        }
        else
            dataQueryClicked = YES;
    }
    
    
    if (indexPath.section == 3 && [[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • DashBoard"]) {
        if (actionDashboardClicked) {
            actionDashboardClicked = NO;
        }
        else
            actionDashboardClicked = YES;
    }
    
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Adhoc Meeting"]) {
        if (adhocMeetingClicked) {
            adhocMeetingClicked = NO;
        }
        else
            adhocMeetingClicked = YES;
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Meeting Actions"]) {
        if (meetingActionClicked) {
            meetingActionClicked = NO;
        }
        else
            meetingActionClicked = YES;
    }
    if (indexPath.section == 4 &&[[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • DashBoard"]) {
        if (meetingDashboardClicked) {
            meetingDashboardClicked = NO;
        }
        else
            meetingDashboardClicked = YES;
    }
    if (indexPath.section == 4 &&[[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Document"]) {
        if (meetingDocumentClicked) {
            meetingDocumentClicked = NO;
        }
        else
            meetingDocumentClicked = YES;
    }
    
    
    if (indexPath.section == 1 && indexPath.row == 0) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_UpdateStakeHolderViewController) {
            self.UpdateStakeHolderViewController = [storyBoard instantiateViewControllerWithIdentifier:@"UpdateStakeHolderViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_UpdateStakeHolderViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if(dataQueryClicked){
        if (indexPath.section == 1 && indexPath.row == 2  ) {
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            if (!_ContactDirectoryViewController) {
                self.ContactDirectoryViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ContactDirectoryViewController"];
            }
            UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_ContactDirectoryViewController];
            [self.revealViewController pushFrontViewController:nc animated:YES];
        }
        if (indexPath.section == 1 && indexPath.row == 5 ) {
            sharedManager.passingMode = @"Category wise companies";
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            if (!_DashBoardPieChartViewController) {
                self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
            }
            UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
            [self.revealViewController pushFrontViewController:nc animated:YES];
        }
        if (indexPath.section == 1 && indexPath.row == 6 )
        {
            sharedManager.passingMode = @"Industry wise companies";
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            if (!_DashBoardPieChartViewController) {
                self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
            }
            UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
            [self.revealViewController pushFrontViewController:nc animated:YES];
        }
    }
    else{
        if (indexPath.section == 1 && indexPath.row == 4  ) {
            sharedManager.passingMode = @"Category wise companies";
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            if (!_DashBoardPieChartViewController) {
                self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
            }
            UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
            [self.revealViewController pushFrontViewController:nc animated:YES];
        }
        if (indexPath.section == 1 && indexPath.row == 5 ) {
            sharedManager.passingMode = @"Industry wise companies";
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            if (!_DashBoardPieChartViewController) {
                self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
            }
            UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
            [self.revealViewController pushFrontViewController:nc animated:YES];
        }
    }
    
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Add Action"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_ActionListViewController) {
            self.ActionListViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ActionListViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_ActionListViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Update Action Progress"] || ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Action Status"] && indexPath.section == 3 )) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_UpdateActionProgressViewController) {
            self.UpdateActionProgressViewController = [storyBoard instantiateViewControllerWithIdentifier:@"UpdateActionProgressViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_UpdateActionProgressViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Action Analysis(Self)"]) {
        sharedManager.passingMode = @"Action Analysis(Self)";
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_DashBoardPieChartViewController) {
            self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Action Analysis(Others)"]) {
        sharedManager.passingMode = @"Action Analysis(Others)";
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_DashBoardPieChartViewController) {
            self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Dependency Actions \n              Analysis"]) {
        sharedManager.passingMode = @"Dependency Actions Analysis";
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_DashBoardPieChartViewController) {
            self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Delayed Completion  \n              Actions Analysis"]) {
        sharedManager.passingMode = @"Delayed Completion  Actions Analysis";
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_DashBoardPieChartViewController) {
            self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • Create Meeting"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_MeetingDetailsViewController) {
            self.MeetingDetailsViewController = [storyBoard instantiateViewControllerWithIdentifier:@"MeetingDetailsViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_MeetingDetailsViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Meeting Calendars"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_MeetingCalenderViewController) {
            self.MeetingCalenderViewController = [storyBoard instantiateViewControllerWithIdentifier:@"MeetingCalenderViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_MeetingCalenderViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Conduct Meeting"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_ConductMeetingViewController) {
            self.ConductMeetingViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ConductMeetingViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_ConductMeetingViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Update Progress"] || ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Action Status"] && indexPath.section == 4 )) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_UpdateMeetingStatusViewController) {
            self.UpdateMeetingStatusViewController = [storyBoard instantiateViewControllerWithIdentifier:@"UpdateMeetingStatusViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_UpdateMeetingStatusViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Approve/Disapprove Actions"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_ApproveActionsViewController) {
            self.ApproveActionsViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ApproveActionsViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_ApproveActionsViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Meeting Action \n              Analysis(Self)" ]) {
        sharedManager.passingMode = @"Meeting Action Analysis(Self)";
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_DashBoardPieChartViewController) {
            self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Meeting Action \n              Analysis(Others)"]) {
        sharedManager.passingMode = @"Meeting Action Analysis(Others)";
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_DashBoardPieChartViewController) {
            self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Group Wise \n              Meetings"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_BarChartViewController) {
            self.BarChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"BarChartViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_BarChartViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • My Meeting"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_MyMeetingViewController) {
            self.MyMeetingViewController = [storyBoard instantiateViewControllerWithIdentifier:@"MyMeetingViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_MyMeetingViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • MOM / Document Retrieval"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_DocumentsRetrievalVC) {
            self.DocumentsRetrievalVC = [storyBoard instantiateViewControllerWithIdentifier:@"DocumentsRetrievalVC"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DocumentsRetrievalVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • Document Upload"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_DocumentUploadVC) {
            self.DocumentUploadVC = [storyBoard instantiateViewControllerWithIdentifier:@"DocumentUploadVC"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DocumentUploadVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • MOM Approval"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_MOMApprovalVC) {
            self.MOMApprovalVC = [storyBoard instantiateViewControllerWithIdentifier:@"MOMApprovalVC"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_MOMApprovalVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • PIMs Report"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_PIMVC) {
            self.PIMVC = [storyBoard instantiateViewControllerWithIdentifier:@"PIMDetailViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_PIMVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • Due Deligence"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_DDVC) {
            self.DDVC = [storyBoard instantiateViewControllerWithIdentifier:@"DueDiligenceViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DDVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • Bidding"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_biddingVC) {
            self.biddingVC = [storyBoard instantiateViewControllerWithIdentifier:@"BiddingViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_biddingVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • Sale Process Note"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_SPNVC) {
            self.SPNVC = [storyBoard instantiateViewControllerWithIdentifier:@"SPNViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_SPNVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • Expression of Interest"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_EOIVC) {
            self.EOIVC = [storyBoard instantiateViewControllerWithIdentifier:@"EOIViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_EOIVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • PIMs"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_pimsVC) {
            self.pimsVC = [storyBoard instantiateViewControllerWithIdentifier:@"PIMSViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_pimsVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • Trust Details"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_trustVC) {
            self.trustVC = [storyBoard instantiateViewControllerWithIdentifier:@"TrustListViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_trustVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • Risk Management"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_riskViewController) {
            self.riskViewController = [storyBoard instantiateViewControllerWithIdentifier:@"RiskManagementViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_riskViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • Docs & Details"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_docsDetailViewController) {
            self.docsDetailViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DocsDetailsViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_docsDetailViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • Legal Due Deligence"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!self.legalddViewController) {
            self.legalddViewController = [storyBoard instantiateViewControllerWithIdentifier:@"LegalDueDiligenceViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_legalddViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • Note to CEO"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!self.noteViewController) {
            self.noteViewController = [storyBoard instantiateViewControllerWithIdentifier:@"NoteToCeoViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:self.noteViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • Assign Stakeholders"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!self.assignStakeViewController) {
            self.assignStakeViewController = [storyBoard instantiateViewControllerWithIdentifier:@"AssignStakeholdersViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:self.assignStakeViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • Possession Process"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!self.possessionProcessVC) {
            self.possessionProcessVC = [storyBoard instantiateViewControllerWithIdentifier:@"PossessionProcessViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:self.possessionProcessVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    /* if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • Note to Board"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!self.noteToBoardViewController) {
            self.noteToBoardViewController = [storyBoard instantiateViewControllerWithIdentifier:@"NoteToBoardViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:self.noteToBoardViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    } */
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • Legal Proceedings"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!self.legalProceedingViewController) {
            self.legalProceedingViewController = [storyBoard instantiateViewControllerWithIdentifier:@"LegalProceedingsViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:self.legalProceedingViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Dashboard"] && arcClicked) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!self.dashboardViewController) {
            self.dashboardViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:self.dashboardViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Pre Acquisition"]) {
        if (preAcquistionSelection) {
            preAcquistionSelection = NO;
        }
        else
            preAcquistionSelection = YES;
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Acquisition"]) {
        if (acquistionSelection) {
            acquistionSelection = NO;
        }
        else
            acquistionSelection = YES;
    }
    
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Resolution"]) {
        if (resolutionSelection) {
            resolutionSelection = NO;
        }
        else
            resolutionSelection = YES;
    }
    
    [dataTable reloadData];
}
-(void)logout{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self presentViewController:myNavController animated:YES completion:nil];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:nil forKey:@"EmailID"];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"subCell"];
    NSMutableArray *list = [[NSMutableArray alloc] init];
    if (indexPath.section == 1 && stackholderClicked) {
        list =  [NSMutableArray arrayWithObjects:@"    • Add stakeholder",@"    • Data Query",nil];
        if (dataQueryClicked) {
            [list insertObject:@"           • Contact dicrectory" atIndex:[list indexOfObject:@"    • Data Query"]+1];
        }
        
    }
    if (indexPath.section == 3 && actionClicked ) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"IsExternal"] isEqualToString:@"Yes"]) {
            if (actionDashboardClicked) {
                list =  [NSMutableArray arrayWithObjects:@"    • Update Action Progress",@"    • DashBoard",@"           • Action Analysis(Self)",@"           • Dependency Actions \n              Analysis",@"           • Delayed Completion  \n              Actions Analysis",nil];
            }
            else{
                list =  [NSMutableArray arrayWithObjects:@"    • Update Action Progress",@"    • DashBoard",nil];
            }
            
        }
        else{
            if (actionDashboardClicked) {
                list =  [NSMutableArray arrayWithObjects:@"    • Add Action",@"    • Update Action Progress",@"    • DashBoard",@"           • Action Analysis(Self)",@"           • Action Analysis(Others)",@"           • Dependency Actions \n              Analysis",@"           • Delayed Completion  \n              Actions Analysis",nil];
            }
            else{
                list =  [NSMutableArray arrayWithObjects:@"    • Add Action",@"    • Update Action Progress",@"    • DashBoard",nil];
            }
        }
    }
    if (indexPath.section == 4 && meetingClicked ) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"IsExternal"] isEqualToString:@"Yes"]) {
            list =  [NSMutableArray arrayWithObjects:@"    • Meeting Actions",@"    • DashBoard",@"    • Document",nil];
            if (meetingActionClicked) {
                [list insertObject:@"           • Update Progress" atIndex:[list indexOfObject:@"    • Meeting Actions"]+1];
            }
            if (meetingDashboardClicked) {
                [list insertObject:@"           • Meeting Action \n              Analysis(Self)" atIndex:[list indexOfObject:@"    • DashBoard"]+1];
                [list insertObject:@"           • My Meeting" atIndex:[list indexOfObject:@"    • DashBoard"]+2];
            }
            if (meetingDocumentClicked) {
                [list insertObject:@"          • Document Upload" atIndex:[list indexOfObject:@"    • Document"]+1];
            }
            
        }
        else{
            list =  [NSMutableArray arrayWithObjects:@"    • Adhoc Meeting",@"    • Meeting Actions",@"    • DashBoard",@"    • Document",nil];
            
            if (adhocMeetingClicked) {
                [list insertObject:@"          • Create Meeting" atIndex:[list indexOfObject:@"    • Adhoc Meeting"]+1];
                [list insertObject:@"           • Meeting Calendars" atIndex:[list indexOfObject:@"    • Adhoc Meeting"]+2];
                [list insertObject:@"           • Conduct Meeting" atIndex:[list indexOfObject:@"    • Adhoc Meeting"]+3];
            }
            if (meetingActionClicked) {
                [list insertObject:@"           • Update Progress" atIndex:[list indexOfObject:@"    • Meeting Actions"]+1];
                [list insertObject:@"           • Approve/Disapprove Actions" atIndex:[list indexOfObject:@"    • Meeting Actions"]+2];
            }
            if (meetingDashboardClicked) {
                [list insertObject:@"           • Meeting Action \n              Analysis(Self)" atIndex:[list indexOfObject:@"    • DashBoard"]+1];
                [list insertObject:@"           • Meeting Action \n              Analysis(Others)" atIndex:[list indexOfObject:@"    • DashBoard"]+2];
                [list insertObject:@"           • Group Wise \n              Meetings" atIndex:[list indexOfObject:@"    • DashBoard"]+3];
                [list insertObject:@"           • My Meeting" atIndex:[list indexOfObject:@"    • DashBoard"]+4];
            }
            if (meetingDocumentClicked) {
                [list insertObject:@"          • Document Upload" atIndex:[list indexOfObject:@"    • Document"]+1];
                [list insertObject:@"           • MOM/Document Retrieval" atIndex:[list indexOfObject:@"    • Document"]+2];
            }
        }
        if ( [[[NSUserDefaults standardUserDefaults]objectForKey:@"IsMOMApprover"] isEqualToString:@"YES"]) {
            long indexValue = [list indexOfObject:@"    • DashBoard"];
            [list insertObject:@"    • MOM Approval" atIndex:indexValue];
        }
    }
    
    if ((indexPath.section == 5 && arcClicked) || (!mitrPlusClicked && indexPath.section == 3 && arcClicked)) {
        list =  [NSMutableArray arrayWithObjects:@"    • Pre Acquisition",@"    • Acquisition",@"    • Resolution",@"    • Dashboard",nil];
        if (preAcquistionSelection) {
            [list insertObject:@"          • Sale Process Note" atIndex:[list indexOfObject:@"    • Pre Acquisition"]+1];
            [list insertObject:@"          • Expression of Interest" atIndex:[list indexOfObject:@"    • Pre Acquisition"]+2];
            [list insertObject:@"          • PIMs Report" atIndex:[list indexOfObject:@"    • Pre Acquisition"]+3];
            [list insertObject:@"          • PIMs" atIndex:[list indexOfObject:@"    • Pre Acquisition"]+4];
            [list insertObject:@"          • Due Deligence" atIndex:[list indexOfObject:@"    • Pre Acquisition"]+5];
            [list insertObject:@"          • Legal Due Deligence" atIndex:[list indexOfObject:@"    • Pre Acquisition"]+6];
            [list insertObject:@"          • Note to CEO" atIndex:[list indexOfObject:@"    • Pre Acquisition"]+7];
            [list insertObject:@"          • Risk Management" atIndex:[list indexOfObject:@"    • Pre Acquisition"]+8];
            [list insertObject:@"          • Bidding" atIndex:[list indexOfObject:@"    • Pre Acquisition"]+9];
            
        }
         if(acquistionSelection)
        {
            [list insertObject:@"          • Trust Details" atIndex:[list indexOfObject:@"    • Acquisition"]+1];
          //  [list insertObject:@"          • Docs & Details" atIndex:[list indexOfObject:@"    • Acquisition"]+2];
        }
        if(resolutionSelection)
        {
            [list insertObject:@"          • Assign Stakeholders" atIndex:[list indexOfObject:@"    • Resolution"]+1];
            [list insertObject:@"          • Legal Proceedings" atIndex:[list indexOfObject:@"    • Resolution"]+2];
            [list insertObject:@"          • Possession Process" atIndex:[list indexOfObject:@"    • Resolution"]+3];
            [list insertObject:@"          • Sale Process" atIndex:[list indexOfObject:@"    • Resolution"]+1];
            [list insertObject:@"          • Action Process" atIndex:[list indexOfObject:@"    • Resolution"]+2];
            [list insertObject:@"          • Bid Process & Recovery" atIndex:[list indexOfObject:@"    • Resolution"]+3];
            
        }
    }
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.adjustsFontSizeToFitWidth = NO;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.text = [list objectAtIndex:indexPath.row];
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    ProfileTableViewCell *cell;
    if (section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"profile"];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        cell.nameLabel.text =[defaults objectForKey:@"EmployeeName"];
        cell.mailIDLabel.text =[defaults objectForKey:@"EmailID"];
        [cell.contentView.layer insertSublayer:[Utitlity getTheGradientColorCode:cell.contentView] atIndex:0];
    }
    else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        NSArray *imageList ;
        if (mitrPlusClicked) {
            imageList = [[NSArray alloc] initWithObjects:@"",@"StakeHolders.png",@"MITR.png",@"Action Planner.png",@"Meeting.png",@"ARCImg.png",@"phone-book.png",@"logout.png",nil];
        }
        else{
            imageList = [[NSArray alloc] initWithObjects:@"",@"StakeHolders.png",@"MITR.png",@"ARCImg.png",@"phone-book.png",@"logout.png",nil];
        }
        NSArray *list ;
        if (mitrPlusClicked) {
            list = [[NSArray alloc] initWithObjects:@"",@"External Stakeholder Management",@"MITR Plus",@"   Action Management",@"   Meeting Management",@"ARC",@"Contacts",@"Logout",nil];
            if (section == 3 || section == 4 ) {
                cell.leadingContrain.constant = 30;
            }
            else
                cell.leadingContrain.constant = 10;
        }
        else{
            cell.leadingContrain.constant = 10;
            list = [[NSArray alloc] initWithObjects:@"",@"External Stakeholder Management",@"MITR Plus",@"ARC",@"Contacts",@"Logout",nil];
        }
        
        cell.headerTittleLabel.text = [list objectAtIndex:section];
        cell.headerImageView.image = [UIImage imageNamed:[imageList objectAtIndex:section]];
        cell.headerImageView.image = [cell.headerImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.headerImageView setTintColor:[UIColor colorWithRed:235.0/255.0 green:95.0/255.0 blue:70.0/255.0 alpha:1.0]];
        cell.clickableBtn.tag = section;
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (section == 0) {
        return 180;
    }
    if ([[defaults objectForKey:@"AccessModule"] rangeOfString:@"External Stakeholder Management"].location == NSNotFound || [[[NSUserDefaults standardUserDefaults] objectForKey:@"IsExternal"] isEqualToString:@"Yes"]) {
        if (section == 1) {
            return 0;
        }
    }
    if ([[defaults objectForKey:@"AccessModule"] rangeOfString:@"MITR Plus-Meeting Management"].location == NSNotFound) {
        if (section == 4 && mitrPlusClicked) {
            return 0;
        }
    }
    if ([[defaults objectForKey:@"AccessModule"] rangeOfString:@"MITR Plus-Action Management"].location == NSNotFound) {
        if (section == 3 && mitrPlusClicked) {
            return 0;
        }
    }
    
    return 70;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([[defaults objectForKey:@"AccessModule"] rangeOfString:@"ARC-Acquisition"].location == NSNotFound) {
        if ((indexPath.section == 3 && indexPath.row == 0 && !mitrPlusClicked) || (indexPath.section == 5 && indexPath.row == 0 && mitrPlusClicked)) {
            return 0;
        }
    }
    if ([[defaults objectForKey:@"AccessModule"] rangeOfString:@"ARC-Transition"].location == NSNotFound) {
        if ((indexPath.section == 3 && indexPath.row == 1 && !mitrPlusClicked) || (indexPath.section == 5 && indexPath.row == 1 && mitrPlusClicked)) {
            return 0;
        }
    }
    if ([[defaults objectForKey:@"AccessModule"] rangeOfString:@"ARC-Resolution"].location == NSNotFound) {
        if ((indexPath.section == 3 && indexPath.row == 2 && !mitrPlusClicked) || (indexPath.section == 5 && indexPath.row == 2 && mitrPlusClicked)) {
            return 0;
        }
    }
    if ([[defaults objectForKey:@"AccessModule"] rangeOfString:@"ARC-Closure"].location == NSNotFound) {
        if ((indexPath.section == 3 && indexPath.row == 3 && !mitrPlusClicked) || (indexPath.section == 5 && indexPath.row == 3 && mitrPlusClicked)) {
            return 0;
        }
    }
    
    
    return 50;
}
- (IBAction)sectionBtn:(id)sender {
    UIButton *clickedButton = (UIButton*)sender;
    
    if (clickedButton.tag == 1 && ![[[NSUserDefaults standardUserDefaults] objectForKey:@"IsExternal"] isEqualToString:@"Yes"]) {
        
        if (stackholderClicked) {
            stackholderClicked = NO;
        }
        else
            stackholderClicked =YES;
    }
    if (clickedButton.tag == 2) {
        if (mitrPlusClicked) {
            mitrPlusClicked = NO;
        }
        else
            mitrPlusClicked = YES;
    }
    if (mitrPlusClicked && clickedButton.tag == 3 ) {
        if (actionClicked) {
            actionClicked = NO;
        }
        else
            actionClicked = YES;
    }
    
    if (mitrPlusClicked && clickedButton.tag == 4) {
        if (meetingClicked) {
            meetingClicked = NO;
        }
        else
            meetingClicked = YES;
    }
    if ((clickedButton.tag == 5 && mitrPlusClicked) || (!mitrPlusClicked && clickedButton.tag == 3)) {
        arcClicked = !arcClicked;
//        if (arcClicked) {
//            arcClicked = NO;
//        }
//        else{
//            arcClicked = YES;
//        }
        
    }
    if ((clickedButton.tag == 6 && mitrPlusClicked) || (!mitrPlusClicked && clickedButton.tag == 4)) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_ContactVC) {
            self.ContactVC = [storyBoard instantiateViewControllerWithIdentifier:@"ContactViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_ContactVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if (clickedButton.tag == 7 || (!mitrPlusClicked && clickedButton.tag == 5)) {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Logout"
                                                                      message:@"Are you sure?"
                                                               preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Yes"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        [self logout];
                                    }];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:yesButton];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    if (selectedSection == clickedButton.tag) {
        selectedRow = 0;
        if (clickedButton.tag == 1) {
            dataQueryClicked = NO;
        }
        if (clickedButton.tag == 2) {
            actionDashboardClicked = NO;
            adhocMeetingClicked = NO;
            meetingActionClicked = NO;
            meetingDashboardClicked = NO;
            meetingDocumentClicked = NO;
            actionClicked = NO;
            meetingClicked = NO;
        }
        if (clickedButton.tag == 3 && mitrPlusClicked) {
            actionDashboardClicked = NO;
        }
        
        if (clickedButton.tag == 4 && mitrPlusClicked) {
            adhocMeetingClicked = NO;
            meetingActionClicked = NO;
            meetingDashboardClicked = NO;
            meetingDocumentClicked = NO;
        }
    }
    selectedSection = clickedButton.tag;
    [dataTable reloadData];
}
- (IBAction)changePassword:(id)sender {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if (!_SettingsVC) {
        self.SettingsVC = [storyBoard instantiateViewControllerWithIdentifier:@"SettingsVC"];
    }
    [self presentViewController:_SettingsVC animated:YES completion:nil];
}


@end

