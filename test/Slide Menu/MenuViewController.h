#import <UIKit/UIKit.h>
#import "ActionListViewController.h"
#import "UpdateActionProgressViewController.h"
#import "MeetingDetailsViewController.h"
#import "AddStackHoldersViewController.h"
#import "UpdateStakeHolderViewController.h"
#import "ContactDirectoryViewController.h"
#import "DashBoardPieChartViewController.h"
#import "UpdateMeetingStatusViewController.h"
#import "MeetingCalenderViewController.h"
#import "ConductMeetingViewController.h"
#import "ApproveActionsViewController.h"
#import "UpdatePassVC.h"
#import "SettingsVC.h"
#import "MyMeetingViewController.h"
#import "BarChartViewController.h"
#import "DocumentsRetrievalVC.h"
#import "DocumentUploadVC.h"
#import "MOMApprovalVC.h"
#import "PIMDetailViewController.h"
#import "ContactViewController.h"
#import "SPNViewController.h"
#import "EOIViewController.h"
#import "DueDiligenceViewController.h"
#import "BiddingViewController.h"
#import "PIMSViewController.h"
#import "TrustListViewController.h"
#import "RiskManagementViewController.h"
#import "DocsDetailsViewController.h"
#import "LegalDueDiligenceViewController.h"
#import "NoteToCeoViewController.h"
#import "AssignStakeholdersViewController.h"
#import "LegalProceedingsViewController.h"
#import "DashboardViewController.h"
#import "NoteToBoardViewController.h"
#import "PossessionProcessViewController.h"

@interface SWUITableViewCell : UITableViewCell
@end

@interface MenuViewController : UITableViewController
@property (nonatomic, strong) ActionListViewController *ActionListViewController;
@property (nonatomic, strong) UpdateActionProgressViewController *UpdateActionProgressViewController;
@property (nonatomic, strong) MeetingDetailsViewController *MeetingDetailsViewController;
@property (nonatomic, strong) AddStackHoldersViewController *AddStackHoldersViewController;
@property (nonatomic, strong) UpdateStakeHolderViewController *UpdateStakeHolderViewController;
@property (nonatomic, strong) ContactDirectoryViewController *ContactDirectoryViewController;
@property (nonatomic, strong) DashBoardPieChartViewController *DashBoardPieChartViewController;
@property (nonatomic, strong) UpdateMeetingStatusViewController *UpdateMeetingStatusViewController;
@property (nonatomic, strong) MeetingCalenderViewController *MeetingCalenderViewController;
@property (nonatomic, strong) ConductMeetingViewController *ConductMeetingViewController;
@property (nonatomic, strong) ApproveActionsViewController *ApproveActionsViewController;
@property (nonatomic, strong) UpdatePassVC *UpdatePassVC;
@property (nonatomic, strong) SettingsVC *SettingsVC;
@property (nonatomic, strong) MyMeetingViewController *MyMeetingViewController;
@property (nonatomic, strong) BarChartViewController *BarChartViewController;
@property (nonatomic, strong) DocumentsRetrievalVC *DocumentsRetrievalVC;
@property (nonatomic, strong) DocumentUploadVC *DocumentUploadVC;
@property (nonatomic, strong) MOMApprovalVC *MOMApprovalVC;
@property (nonatomic, strong) PIMDetailViewController *PIMVC;
@property (nonatomic, strong) ContactViewController *ContactVC;
@property (nonatomic, strong) SPNViewController *SPNVC;
@property (nonatomic, strong) EOIViewController *EOIVC;
@property (nonatomic, strong) DueDiligenceViewController *DDVC;
@property (nonatomic, strong) BiddingViewController *biddingVC;
@property (nonatomic, strong) PIMSViewController *pimsVC;
@property (nonatomic, strong) TrustListViewController *trustVC;
@property (nonatomic, strong) RiskManagementViewController *riskViewController;
@property (nonatomic, strong) DocsDetailsViewController *docsDetailViewController;
@property (nonatomic, strong) LegalDueDiligenceViewController *legalddViewController;
@property (nonatomic, strong) NoteToCeoViewController *noteViewController;
@property (nonatomic, strong) AssignStakeholdersViewController *assignStakeViewController;
@property (nonatomic, strong) LegalProceedingsViewController *legalProceedingViewController;
@property (nonatomic, strong) DashboardViewController *dashboardViewController;
@property (nonatomic, strong) NoteToBoardViewController *noteToBoardViewController;
@property (nonatomic, strong) PossessionProcessViewController *possessionProcessVC;


@end

