//
//  CustomTableViewCell.h
//  test
//
//  Created by ceaselez on 29/11/17.
//  Copyright © 2017 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZTextView.h"
@interface CustomTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *employeeTypeTF;
@property (weak, nonatomic) IBOutlet UITextField *categoryTF;
@property (weak, nonatomic) IBOutlet UITextField *assignedToTF;
@property (weak, nonatomic) IBOutlet UITextField *startDate;
@property (weak, nonatomic) IBOutlet UITextField *targetDateTF;
@property (weak, nonatomic) IBOutlet UITextField *actionTypeTF;
@property (weak, nonatomic) IBOutlet UITextView *actionDescriptionTV;
@property (weak, nonatomic) IBOutlet UITextField *companyTF;
@property (weak, nonatomic) IBOutlet UITextField *mainCategoryTF;
@property (weak, nonatomic) IBOutlet UIImageView *employeeIV;
@property (weak, nonatomic) IBOutlet UIImageView *categoryIV;
@property (weak, nonatomic) IBOutlet UIImageView *companyIV;
@property (weak, nonatomic) IBOutlet UIImageView *assignedToIV;
@property (weak, nonatomic) IBOutlet UIImageView *startDateIV;
@property (weak, nonatomic) IBOutlet UIImageView *targetDateIV;
@property (weak, nonatomic) IBOutlet UIImageView *actionTypeIV;
@property (weak, nonatomic) IBOutlet UIImageView *actionDescrptionIV;
@property (weak, nonatomic) IBOutlet UILabel *plannerListDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *plannerListHeadderLabel;
@property (weak, nonatomic) IBOutlet UILabel *plannerEmployeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *plannerCategoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *plannerCompanyLabel;
@property (weak, nonatomic) IBOutlet UILabel *actionListDescripition;
@property (weak, nonatomic) IBOutlet UILabel *plannerAssignedToLabel;
@property (weak, nonatomic) IBOutlet UILabel *plannerStartDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *plannerTargetDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *plannerActionTypeLabel;
@property (weak, nonatomic) IBOutlet UITextView *plannerActionDescriptionTV;
@property (weak, nonatomic) IBOutlet SZTextView *updateRemaksTV;
@property (weak, nonatomic) IBOutlet UITextField *updateCompletionDateTF;
@property (weak, nonatomic) IBOutlet UILabel *actionListAssignee;
@property (weak, nonatomic) IBOutlet UILabel *actionListDate;
@property (weak, nonatomic) IBOutlet UILabel *actiionListCategory;
@property (weak, nonatomic) IBOutlet UILabel *actionListEmployeeTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *actionListGroupCategoryList;
@property (weak, nonatomic) IBOutlet UILabel *actionListTargetDate;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UILabel *actionListType;
@property (weak, nonatomic) IBOutlet UILabel *docName;
@property (weak, nonatomic) IBOutlet UIButton *downloadBtn;
@property (weak, nonatomic) IBOutlet SZTextView *fileDescription;
@property (weak, nonatomic) IBOutlet SZTextView *FileDescTV;
@property (weak, nonatomic) IBOutlet UITextField *StakeHolderTF;
@property (weak, nonatomic) IBOutlet UIImageView *StakeHolderIV;
@property (weak, nonatomic) IBOutlet UILabel *updateActionAssignedToTopicLabel;
@property (weak, nonatomic) IBOutlet UITextField *groupCategory;
@property (weak, nonatomic) IBOutlet UILabel *popOverTopicLabel;
@property (weak, nonatomic) IBOutlet UILabel *popOverContentLabel;
@property (weak, nonatomic) IBOutlet UITextField *bankNameText;
@property (weak, nonatomic) IBOutlet UITextField *refNoText;
@property (weak, nonatomic) IBOutlet UITextField *notificationDateText;
@property (weak, nonatomic) IBOutlet UITextField *eoiDateText;
@property (weak, nonatomic) IBOutlet UITextField *roomDateText;
@property (weak, nonatomic) IBOutlet UITextField *roomTimingText;
@property (weak, nonatomic) IBOutlet UITextField *cutOffText;
@property (weak, nonatomic) IBOutlet UITextField *bidSubmissionDateText;
@property (weak, nonatomic) IBOutlet UITextField *bidTypeText;
@property (weak, nonatomic) IBOutlet UITextField *bidDateText;
@property (weak, nonatomic) IBOutlet UITextField *bidTimeText;
@property (weak, nonatomic) IBOutlet UITextField *totalAmountText;
@property (weak, nonatomic) IBOutlet UILabel *documentName;
@property (weak, nonatomic) IBOutlet UITextField *srRatioText;
@property (weak, nonatomic) IBOutlet UITextField *srRemainingText;
@property (weak, nonatomic) IBOutlet UITextField *upsideRatioText;
@property (weak, nonatomic) IBOutlet UITextField *upsideRemainingRatio;
@property (weak, nonatomic) IBOutlet UITextField *managementFeeHeader;
@property (weak, nonatomic) IBOutlet UITextField *managementFeeData;

@property (weak, nonatomic) IBOutlet UITextField *managementYearData;
@property (weak, nonatomic) IBOutlet UITextField *managementPercData;
@property (weak, nonatomic) IBOutlet UITextField *incentiveFeeHeader;
@property (weak, nonatomic) IBOutlet UITextField *incentiveFeeData;
@property (weak, nonatomic) IBOutlet UITextField *incentiveYearData;
@property (weak, nonatomic) IBOutlet UITextField *incentivePerData;
@property (weak, nonatomic) IBOutlet UITextField *interestHeader;
@property (weak, nonatomic) IBOutlet UITextField *interestData;
@property (weak, nonatomic) IBOutlet UITextField *submittedDate;
@property (weak, nonatomic) IBOutlet UITextField *ndaDate;

@property (weak, nonatomic) IBOutlet UITextField *accountName;
@property (weak, nonatomic) IBOutlet UITextField *bankArrangement;
@property (weak, nonatomic) IBOutlet UITextField *unitLocation;
@property (weak, nonatomic) IBOutlet UITextField *ourShare;
@property (weak, nonatomic) IBOutlet UITextField *otherShare;
@property (weak, nonatomic) IBOutlet UITextField *landBankShare;
@property (weak, nonatomic) IBOutlet UITextField *lnbShare;
@property (weak, nonatomic) IBOutlet UITextField *pnmShare;
@property (weak, nonatomic) IBOutlet UITextField *realBalance;
@property (weak, nonatomic) IBOutlet UITextField *unchargedInterest;
@property (weak, nonatomic) IBOutlet UITextField *totalBalance;
@property (weak, nonatomic) IBOutlet UITextField *fmvText;
@property (weak, nonatomic) IBOutlet UITextField *dsvText;
@property (weak, nonatomic) IBOutlet UITextField *reservePriceText;
@property (weak, nonatomic) IBOutlet UITextField *rejectionParameterText;
@property (weak, nonatomic) IBOutlet UITextField *statusText;
@property (weak, nonatomic) IBOutlet UITextField *remarkText;
@property (weak, nonatomic) IBOutlet UITextField *pimDocText;
@property (weak, nonatomic) IBOutlet UITextField *incrementalValueText;
@property (weak, nonatomic) IBOutlet UITextField *initialBidAmount;
@property (weak, nonatomic) IBOutlet UITextField *finalBidAmount;
@property (weak, nonatomic) IBOutlet UITextField *bidResult;
@property (weak, nonatomic) IBOutlet UITextField *accountTypeText;
@property (weak, nonatomic) IBOutlet UITextField *trancheNameText;
@property (weak, nonatomic) IBOutlet UITextField *partnerNameText;
@property (weak, nonatomic) IBOutlet UITextField *lineOfActivityText;
@property (weak, nonatomic) IBOutlet UITextField *assetClarificationText;
@property (weak, nonatomic) IBOutlet UITextField *amountOutstandingText;
@property (weak, nonatomic) IBOutlet UITextField *asOnDateText;
@property (weak, nonatomic) IBOutlet UITextField *amountOfBid;
@property (weak, nonatomic) IBOutlet UITextField *securityValuesText;
@property (weak, nonatomic) IBOutlet UITextField *dueDeligenceByText;
@property (weak, nonatomic) IBOutlet UITextField *riskManagementHeadText;
@property (weak, nonatomic) IBOutlet UITextField *dueDeligenceObservations;
@property (weak, nonatomic) IBOutlet UITextField *riskManagementObservations;
@property (weak, nonatomic) IBOutlet UITextField *riskScoreText;
@property (weak, nonatomic) IBOutlet UITextField *realisableValueText;
@property (weak, nonatomic) IBOutlet UITextField *reservePriceCashText;
@property (weak, nonatomic) IBOutlet UITextField *trustNameText;
@property (weak, nonatomic) IBOutlet UITextField *purchaseTypeText;
@property (weak, nonatomic) IBOutlet UITextField *purchaseValueText;
@property (weak, nonatomic) IBOutlet UITextField *cashPaidText;
@property (weak, nonatomic) IBOutlet UITextField *accountNumberText;
@property (weak, nonatomic) IBOutlet UITextField *ifscCodeText;
@property (weak, nonatomic) IBOutlet UITextField *trustCreatedDateText;
@property (weak, nonatomic) IBOutlet UITextField *branchNameText;
@property (weak, nonatomic) IBOutlet UITextField *arcPercText;
@property (weak, nonatomic) IBOutlet UITextField *arcAmountText;
@property (weak, nonatomic) IBOutlet UITextField *bankPercText;
@property (weak, nonatomic) IBOutlet UITextField *bankAmountText;
@property (weak, nonatomic) IBOutlet SZTextView *remarksTextView;
@property (weak, nonatomic) IBOutlet SZTextView *rejectionParameterTextView;
@property (weak, nonatomic) IBOutlet UITextField *locationNameText;
@property (weak, nonatomic) IBOutlet UITextField *activityBorrowerNameText;
@property (weak, nonatomic) IBOutlet UITextField *bankingArrangementText;
@property (weak, nonatomic) IBOutlet UITextField *npaDateText;
@property (weak, nonatomic) IBOutlet UITextField *assetDetailsText;
@property (weak, nonatomic) IBOutlet UITextField *legalActionText;
@property (weak, nonatomic) IBOutlet SZTextView *legalDDText;
@property (weak, nonatomic) IBOutlet SZTextView *riskManagementObservationsTextView;
@property (weak, nonatomic) IBOutlet SZTextView *recommendationsTextView;
@property (weak, nonatomic) IBOutlet SZTextView *sarfaesiActionTextView;

@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UITextField *accountNameText;
@property (weak, nonatomic) IBOutlet UITextField *purchaseRatioText;

@property (weak, nonatomic) IBOutlet UITextField *caseIdText;
@property (weak, nonatomic) IBOutlet UITextField *assetNameText;
@property (weak, nonatomic) IBOutlet UITextField *hearingDateText;
@property (weak, nonatomic) IBOutlet UITextField *nextHearingDateText;
@property (weak, nonatomic) IBOutlet UITextField *defenderText;
@property (weak, nonatomic) IBOutlet UITextField *advocateNameText;
@property (weak, nonatomic) IBOutlet UITextField *advocatePhoneText;
@property (weak, nonatomic) IBOutlet UITextField *advocateEmailText;
@property (weak, nonatomic) IBOutlet UITextField *asOnPreviousDateText;
@property (weak, nonatomic) IBOutlet UITextField *descHearingText;
@property (weak, nonatomic) IBOutlet UITextField *reviewProgressText;

@property (weak, nonatomic) IBOutlet SZTextView *ddObservationsTextView;
@property (weak, nonatomic) IBOutlet UITextField *totalRiskScoreText;
@property (weak, nonatomic) IBOutlet SZTextView *acquisitionHeadTextView;
@property (weak, nonatomic) IBOutlet SZTextView *otherSpecTextView;
@property (weak, nonatomic) IBOutlet SZTextView *borrowerCommentTextView;
@property (weak, nonatomic) IBOutlet SZTextView *securityValueTextView;
@property (weak, nonatomic) IBOutlet SZTextView *statusTextView;
@property (weak, nonatomic) IBOutlet SZTextView *assetDetailsTextView;
@property (weak, nonatomic) IBOutlet SZTextView *legalActionsTextView;
@property (weak, nonatomic) IBOutlet SZTextView *partnerNamesTextView;


@end
