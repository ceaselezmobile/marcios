//
//  AddActionViewController.h
//  test
//
//  Created by ceaselez on 30/11/17.
//  Copyright © 2017 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DashboardViewController.h"
@interface AddActionViewController : UIViewController
@property (nonatomic, strong) DashboardViewController *DashboardViewController;
@property(nonatomic, assign) BOOL update;
@property(nonatomic, assign) BOOL meetingAction;
@property(nonatomic, strong) NSString *meetingDate;
@property(nonatomic, weak) NSString *meetingID;
@property(nonatomic, strong) NSDictionary *meetingActionDict;
@property(nonatomic, assign) BOOL Dependency;


@end
