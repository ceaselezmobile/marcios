//
//  UpcomingBidsViewController.h
//  test
//
//  Created by ceazeles on 15/10/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DemoBaseViewController.h"
#import <Charts/Charts.h>
#import "IntAxisValueFormatter.h"
#import "SWRevealViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface UpcomingBidsViewController : DemoBaseViewController
@property (weak, nonatomic) IBOutlet BarChartView *chartView;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;

@end

NS_ASSUME_NONNULL_END
