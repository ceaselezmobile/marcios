//
//  DueDiligenceTableViewCell.h
//  test
//
//  Created by ceazeles on 05/09/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DueDiligenceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *bankNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *refNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *accNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *dueDiligenceLabel;

@end
