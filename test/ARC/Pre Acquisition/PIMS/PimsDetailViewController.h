//
//  PimsDetailViewController.h
//  test
//
//  Created by ceazeles on 11/09/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalURL.h"
#import "MBProgressHUD.h"

@interface PimsDetailViewController : UIViewController

@property(strong,nonatomic)NSString *theId;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
