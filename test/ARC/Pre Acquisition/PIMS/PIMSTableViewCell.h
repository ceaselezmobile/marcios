//
//  PIMSTableViewCell.h
//  test
//
//  Created by ceazeles on 11/09/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PIMSTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *bankName;
@property (weak, nonatomic) IBOutlet UILabel *refNo;
@property (weak, nonatomic) IBOutlet UILabel *accountName;
@property (weak, nonatomic) IBOutlet UILabel *ledgerBalance;
@property (weak, nonatomic) IBOutlet UILabel *isWilfulDefaulter;

@end
