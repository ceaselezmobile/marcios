//
//  PIMSViewController.h
//  test
//
//  Created by ceazeles on 11/09/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "PIMSTableViewCell.h"
#import "Utitlity.h"
#import "GlobalURL.h"
#import "WebServices.h"
#import "MBProgressHUD.h"
#import "PimsDetailViewController.h"

@interface PIMSViewController : UIViewController
{
    NSMutableArray *array;
    NSMutableArray *searchResultArray;
    UILabel *noDataLabel;
}

@property (assign)BOOL searchBarActive;
@property(strong,nonatomic)NSString *theURL;
@property(strong,nonatomic)NSString *searchFlag;
@property(strong,nonatomic)NSString *searchScreen;

@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
