//
//  PimsDetailViewController.m
//  test
//
//  Created by ceazeles on 11/09/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "PimsDetailViewController.h"

@interface PimsDetailViewController ()

@end

@implementation PimsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
     [MBProgressHUD showHUDAddedTo:_webView animated:YES];
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:[BASEURL stringByAppendingString:[NSString stringWithFormat:@"/GetAllPIMDetail?userid=%@&pimid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"],self.theId]]]];
    [_webView loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
    [MBProgressHUD hideHUDForView:webView animated:YES];
}

- (IBAction)backButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
