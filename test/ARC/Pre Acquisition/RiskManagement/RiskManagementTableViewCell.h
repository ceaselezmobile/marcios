//
//  RiskManagementTableViewCell.h
//  test
//
//  Created by ceazeles on 19/09/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RiskManagementTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *bankName;
@property (weak, nonatomic) IBOutlet UILabel *referenceNumber;
@property (weak, nonatomic) IBOutlet UILabel *accountName;
@property (weak, nonatomic) IBOutlet UILabel *bidAmount;
@property (weak, nonatomic) IBOutlet UILabel *remarks;
@property (weak, nonatomic) IBOutlet UILabel *riskScoreLabel;

@end
