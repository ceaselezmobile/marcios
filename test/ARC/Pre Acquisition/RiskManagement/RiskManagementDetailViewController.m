//
//  RiskManagementDetailViewController.m
//  test
//
//  Created by ceazeles on 20/09/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "RiskManagementDetailViewController.h"

@interface RiskManagementDetailViewController ()

@end

@implementation RiskManagementDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    array = [NSMutableArray array];
    docArray = [NSMutableArray array];
    self.popOverView.hidden = YES;
    
    [self loadDataFromApi:[NSString stringWithFormat:@"/GetAllRiskMgmtDetailsById?userid=%@&pimid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"],self.theId]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
   /* if(array.count > 0)
    {
        if([[array[0]valueForKey:@"AccountType"] isEqualToString:@"Individual"])
        {
            return 16;
        }
    }*/
    return 20;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(array.count > 0)
    {
        if([[array[0]valueForKey:@"AccountType"] isEqualToString:@"Individual"])
        {
            if(section == 4)
            {
                return 0;
            }
        }
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomTableViewCell *cell;
    if(indexPath.section == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"bankNameCell"];
        if(array.count>0){
            cell.bankNameText.text = [array[0]valueForKey:@"BankName"];
            cell.bankNameText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
        
    }
    else if(indexPath.section == 1)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"accountNameCell"];
        if(array.count>0){
            cell.accountName.text = [array[0]valueForKey:@"AccountName"];
            cell.accountName.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 2)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"referenceNoCell"];
        if(array.count>0){
            cell.refNoText.text = [array[0]valueForKey:@"ProjectCode"];
            cell.refNoText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 3)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"accountTypeCell"];
        if(array.count>0){
            cell.accountTypeText.text = [array[0]valueForKey:@"AccountType"];
            cell.accountTypeText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 4)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"trancheNameCell"];
        if(array.count>0){
            cell.trancheNameText.text = [array[0]valueForKey:@"TranchName"];
            cell.trancheNameText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 5)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"partnerNameCell"];
        if(array.count>0){
            cell.partnerNamesTextView.text = [array[0]valueForKey:@"PartnersOrDirectors"];
            cell.partnerNamesTextView.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 6)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"lineActivityCell"];
        if(array.count>0){
            cell.lineOfActivityText.text = [array[0]valueForKey:@"LineOfActivity"];
            cell.lineOfActivityText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 7)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"assetClarificationCell"];
        if(array.count>0){
            cell.assetClarificationText.text = [array[0]valueForKey:@"AssetClassification"];
            cell.assetClarificationText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 8)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"amtOutstandingCell"];
        if(array.count>0){
            cell.amountOutstandingText.text = [[array[0]valueForKey:@"AmountOutStanding"]stringValue];
            cell.amountOutstandingText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 9)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"asOnDateCell"];
        if(array.count>0){
            cell.asOnDateText.text = [array[0]valueForKey:@"AsOnDate"];
            cell.asOnDateText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 10)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"amountOfBidCell"];
        if(array.count>0){
            cell.amountOfBid.text = [[array[0]valueForKey:@"AmountOfBid"]stringValue];
            cell.amountOfBid.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 11)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"securityValueCell"];
        if(array.count>0){
            cell.securityValueTextView.text = [array[0]valueForKey:@"ValueOfSecurities"];
            cell.securityValueTextView.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 12)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"dueDeligenceCell"];
        if(array.count>0){
            cell.dueDeligenceByText.text = [array[0]valueForKey:@"DueDiligenceBy"];
            cell.dueDeligenceByText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 13)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"riskManagementHeadCell"];
        if(array.count>0){
            cell.riskManagementHeadText.text = [array[0]valueForKey:@"RiskManagementHead"];
            cell.riskManagementHeadText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 14)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"ddObservationsCell"];
        if(array.count>0){
            cell.ddObservationsTextView.text = [array[0]valueForKey:@"DDObservations"];
            cell.ddObservationsTextView.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 15)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"riskManagementObservationsCell"];
        if(array.count>0){
            if(!([[array[0]valueForKey:@"RiskManagementObservations"] isEqual:[NSNull null]] && [array[0]valueForKey:@"RiskManagementObservations"] != nil)){
                cell.riskManagementObservationsTextView.text = [array[0]valueForKey:@"RiskManagementObservations"];
            }
            cell.riskManagementObservationsTextView .layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 16)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"riskScoreCell"];
        if(array.count>0){
            if(!([[array[0]valueForKey:@"RiskScore"] isEqual:[NSNull null]] && [array[0]valueForKey:@"RiskScore"] != nil)){
                cell.riskScoreText.text = [[array[0]valueForKey:@"RiskScore"]stringValue];
            }
            cell.riskScoreText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 17)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"totalRiskScoreCell"];
        if(array.count>0){
           // if(!([[array[0]valueForKey:@"lstRiskScore"] isEqual:[NSNull null]] && [array[0]valueForKey:@"lstRiskScore"] != nil)){
               // cell.riskScoreText.text = [[array[0]valueForKey:@"lstRiskScore"]stringValue];
           // }
            cell.riskScoreText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 18)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"acquisitionCell"];
        if(array.count>0){
            
            cell.acquisitionHeadTextView.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 19)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"otherSpecCell"];
        if(array.count>0){
            
            cell.otherSpecTextView.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
  return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(void)loadDataFromApi :(NSString *)url{
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[WebServices sharedInstance] getWithParameter:nil withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
            NSLog(@"response izz %@",responseObject);
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self->array addObjectsFromArray:responseObject];
                if(self->array.count > 0 )
                {
                    [self->docArray addObjectsFromArray:[[responseObject objectAtIndex:0]objectForKey:@"lstEOIDocument"]];
                    [self.tableView reloadData];
                }
                
            });
            
        }];
        
        
    }else{
        
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}

- (IBAction)backButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
