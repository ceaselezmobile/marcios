//
//  MAilPopUpViewController.m
//  test
//
//  Created by ceazeles on 27/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "MAilPopUpViewController.h"

@interface MAilPopUpViewController ()<UIPopoverPresentationControllerDelegate,UIPopoverControllerDelegate,KPDropMenuDelegate>

@end

@implementation MAilPopUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"_docArray izz %@",_docArray);
    array = [NSMutableArray array];
    fromIdArray = [NSMutableArray array];
    fromNameArray = [NSMutableArray array];
    if(_docArray.count>0)
    {
        _firstAttachment.text = [[_docArray objectAtIndex:0]valueForKey:@"FileName"];
         [_tagsView addTag:[[_docArray objectAtIndex:0]valueForKey:@"FileName"]];
        
        if(_docArray.count>1)
        {
            _secondAttachment.text = [[_docArray objectAtIndex:1]valueForKey:@"FileName"];
            [_tagsView addTag:[[_docArray objectAtIndex:1]valueForKey:@"FileName"]];
        }
       
       
    }
    self.contentTextView.placeholder = @"Enter Body";
    _dropdownMenu.itemsFont = [UIFont fontWithName:@"Helvetica-Regular" size:12.0];
    _dropdownMenu.titleTextAlignment = NSTextAlignmentLeft;
    _dropdownMenu.delegate = self;
    [self loadDataFromApi:[NSString stringWithFormat:@"GetFromList?userid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]]];
    
    self.toRecipients = [[NSMutableArray alloc] init];
    self.toField = [[JSTokenField alloc] initWithFrame:CGRectMake(81, 130, self.view.frame.size.width-70, 42)];
    [self.toField setDelegate:self];
    [self.toField setBackgroundColor:[UIColor whiteColor]];
    // [self.view addSubview:self.toField];
    
    UIView *separator1 = [[UIView alloc] initWithFrame:CGRectMake(0, self.toField.bounds.size.height-1, self.toField.bounds.size.width, 1)];
    [separator1 setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
    [self.toField addSubview:separator1];
    [separator1 setBackgroundColor:[UIColor whiteColor]];
    
    [self setup];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    // NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    
    self.subjectText.text = [[self.bankName stringByAppendingString:@" EOI for Notification dated on "]stringByAppendingString:[dateFormatter stringFromDate:[NSDate date]]];
    if(self.emailId.length > 0)
    {
      self.toText.text = self.emailId;
    }
}

- (void)setup
{
    _tagsView.tagColorTheme = TagColorThemeCoolGray;
    //[self handleTagBlocks];
}

- (void)handleTagBlocks
{
    __weak typeof(self) weakSelf = self;
    [_tagsView setTapBlock:^(NSString *tagText, NSInteger idx)
     {
         NSString *message = [NSString stringWithFormat:@"Selected: %@", tagText];
         [weakSelf showAlertMessage:message];
     }];
    
    [_tagsView setDeleteBlock:^(NSString *tagText, NSInteger idx)
     {
         NSString *message = [NSString stringWithFormat:@"Removed: %@", tagText];
         [weakSelf showAlertMessage:message];
         [weakSelf.tagsView deleteTagAtIndex:idx];
     }];
}

- (void)showAlertMessage:(NSString *)message
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:action];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeButtonClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)sendMailClick:(id)sender {
    
    NSLog(@"Title is %@",_dropdownMenu.title);
    
    if([_dropdownMenu.title isEqualToString:@""] || _dropdownMenu.title == nil)
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please enter from email id" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    
    else if([self.toText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please enter recipient email id" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else if(![[Utitlity sharedInstance]validateEmailWithString:_toText.text])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please enter valid email id" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else if([self.subjectText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please enter the subject" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else if([self.bodyText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please enter the mail content" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else{
        [self sendMail:@"/POSTEoiMailToAllocatedPerson"];
    }
}

-(void)loadDataFromApi :(NSString *)url{
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[WebServices sharedInstance] getWithParameter:nil withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
            NSLog(@"response izz %@",responseObject);
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self->array addObjectsFromArray:responseObject];
                if(self->array.count > 0 )
                {
                    for(int i=0;i<array.count;i++)
                    {
                        [self->fromIdArray addObject:[array[i]valueForKey:@"EmailID"]];
                        [self->fromNameArray addObject:[array[i]valueForKey:@"Name"]];
                    }
                    _dropdownMenu.items = self->fromIdArray;
                    _dropdownMenu.itemsIDs = self->fromNameArray;
                }
                
            });
            
        }];
        
        
    }else{
        
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}

-(void)sendMail :(NSString *)url
{
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"],@"UserId",[[_docArray objectAtIndex:0]valueForKey:@"EOIID"],@"EOIID",_dropdownMenu.title,@"Sender",self.toText.text,@"Receivers",self.ccText.text,@"CC",@"",@"BCC",self.bodyText.text,@"Body",self.subjectText.text,@"Subject", nil];
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[WebServices sharedInstance] postwithParameter:dict withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
            NSLog(@"mail response izz %@",responseObject);
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                if([[responseObject valueForKey:@"SendCustomEmailToAllocatedPersonResult"] isEqualToString:@"Success."])
                {
                    [[Utitlity sharedInstance] showAlertViewWithMessage:@"Mail Sent" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
                     {
                         [self dismissViewControllerAnimated:YES completion:nil];
                     }];
                }
                else{
                    [[Utitlity sharedInstance] showAlertViewWithMessage:@"Mail sending failed.Try again." withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
                     {
                         
                     }];
                }
                
            });
            
        }];
        
        
    }else{
        
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

-(void)showToast
{
    [self dismissViewControllerAnimated:YES completion:^{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.label.text = @"Mail Sent";
        hud.label.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
        hud.margin = 10.f;
        [hud setOffset:CGPointMake(0, self.view.frame.size.height)];
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:0.5];
    }];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - KPDropMenu Delegate Methods

-(void)didSelectItem : (KPDropMenu *) dropMenu atIndex : (int) atIntedex{
    if(dropMenu == _dropdownMenu)
    {
        NSLog(@"%@ with TAG : %ld", dropMenu.items[atIntedex], (long)dropMenu.tag);
        _dropdownMenu.title = [[[dropMenu.itemsIDs[atIntedex]stringByAppendingString:@"<"]stringByAppendingString:dropMenu.items[atIntedex]]stringByAppendingString:@">"];
    }
    else
        NSLog(@"%@", dropMenu.items[atIntedex]);
}

-(void)didShow:(KPDropMenu *)dropMenu{
    NSLog(@"didShow");
}

-(void)didHide:(KPDropMenu *)dropMenu{
    NSLog(@"didHide");
}


#pragma mark -
#pragma mark JSTokenFieldDelegate

- (void)tokenField:(JSTokenField *)tokenField didAddToken:(NSString *)title representedObject:(id)obj
{
    NSDictionary *recipient = [NSDictionary dictionaryWithObject:obj forKey:title];
    [self.toRecipients addObject:recipient];
    NSLog(@"Added token for < %@ : %@ >\n%@", title, obj, self.toRecipients);
}

- (void)tokenField:(JSTokenField *)tokenField didRemoveTokenAtIndex:(NSUInteger)index
{
    [self.toRecipients removeObjectAtIndex:index];
    NSLog(@"Deleted token %tu\n%@", index, self.toRecipients);
}

- (BOOL)tokenFieldShouldReturn:(JSTokenField *)tokenField {
    NSMutableString *recipient = [NSMutableString string];
    
    NSMutableCharacterSet *charSet = [[NSCharacterSet whitespaceCharacterSet] mutableCopy];
    [charSet formUnionWithCharacterSet:[NSCharacterSet punctuationCharacterSet]];
    
    NSString *rawStr = [[tokenField textField] text];
    for (int i = 0; i < [rawStr length]; i++)
    {
        if (![charSet characterIsMember:[rawStr characterAtIndex:i]])
        {
            [recipient appendFormat:@"%@",[NSString stringWithFormat:@"%c", [rawStr characterAtIndex:i]]];
        }
    }
    
    if ([rawStr length])
    {
        [tokenField addTokenWithTitle:rawStr representedObject:recipient];
    }
    
    [[tokenField textField] setText:@""];
    
    return NO;
}

@end
