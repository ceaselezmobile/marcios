//
//  EOIDetailViewController.m
//  test
//
//  Created by ceazeles on 27/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "EOIDetailViewController.h"

@interface EOIDetailViewController ()<UIDocumentPickerDelegate>

@end

@implementation EOIDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    array = [NSMutableArray array];
    docArray = [NSMutableArray array];
    self.popOverView.hidden = YES;
    [self loadDataFromApi:[NSString stringWithFormat:@"/GetAllEOIListDetailByEoiId?userid=%@&eoiid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"],self.theId]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 9;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 8)
    {
        return docArray.count;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomTableViewCell *cell;
    if(indexPath.section == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"bankNameCell"];
        if(array.count>0){
            cell.bankNameText.text = [array[0]valueForKey:@"BankName"];
            cell.bankNameText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
        
    }
    else if(indexPath.section == 1)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"refNoCell"];
        if(array.count>0){
            cell.refNoText.text = [array[0]valueForKey:@"ProjectCode"];
            cell.refNoText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 2)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"notificationCell"];
        if(array.count>0){
            cell.notificationDateText.text = [array[0]valueForKey:@"NotificationDate"];
            cell.notificationDateText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 3)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"submissionCell"];
        if(array.count>0){
            cell.eoiDateText.text = [array[0]valueForKey:@"EOISubmissionDate"];
            cell.eoiDateText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 4)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"roomDateCell"];
        if(array.count>0){
            cell.roomDateText.text = [array[0]valueForKey:@"EOISubmittedByName"];
            cell.roomDateText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 5)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"submitedCell"];
        if(array.count>0){
            cell.submittedDate.text = [array[0]valueForKey:@"EOISubmittedDate"];
            cell.submittedDate.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 6)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"ndaCell"];
        if(array.count>0){
             if(!([[array[0]valueForKey:@"NDAValidFromDate"] isEqual:[NSNull null]] && [[array[0]valueForKey:@"NDAValidToDate"] isEqual:[NSNull null]])){
            cell.ndaDate.text = [[[array[0]valueForKey:@"NDAValidFromDate"]stringByAppendingString:@" TO "]stringByAppendingString:[array[0]valueForKey:@"NDAValidToDate"]];
             }
            cell.ndaDate.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    else if(indexPath.section == 7)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"docHeaderCell"];
    }
    else if(indexPath.section == 8)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"docCell"];
        if(array.count>0)
        {
            cell.docName.text = [docArray[indexPath.row]valueForKey:@"FileName"];
            
            cell.downloadBtn.tag = indexPath.row;
            [cell.downloadBtn addTarget:self action:@selector(download:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(void)download:(UIButton*)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:(UITableViewCell *)sender.superview.superview];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Do you want to dowload or view the file?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *Download = [UIAlertAction actionWithTitle:@"Download" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                               {
                                   [self downloadFiles:sender.tag];
                               }];
    UIAlertAction *viewFile = [UIAlertAction actionWithTitle:@"View" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                               {
                                   [self viewFile:sender.tag];
                               }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:Download];
    [alert addAction:viewFile];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)downloadFiles :(NSInteger)indexPath{
   [MBProgressHUD showHUDAddedTo:_webView animated:YES];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlToDownload ;
        NSString *type;
        
                urlToDownload = [[docArray objectAtIndex:indexPath] objectForKey:@"FilePath"];;
        
                type=[[docArray objectAtIndex:indexPath] objectForKey:@"FileName"];
        if(!([urlToDownload isEqual:[NSNull null]] || urlToDownload == nil)){
            
        NSLog(@"urlToDownload--------%@",urlToDownload);
        urlToDownload = [urlToDownload stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        //        urlToDownload = [urlToDownload stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSURL  *url = [NSURL URLWithString:urlToDownload];
        NSData *urlData = [NSData dataWithContentsOfURL:url];
        if ( urlData )
        {
            NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,type];
            
            //saving is done on main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:_webView animated:YES];
                [urlData writeToFile:filePath atomically:YES];
                NSLog(@"filepath test n---------%@",filePath);
                
                NSLog(@"File Saved !");
                
                //Create the file path of the document to upload
                NSURL *filePathToUpload = [NSURL fileURLWithPath:filePath]  ;
                UIDocumentPickerViewController *docPicker = [[UIDocumentPickerViewController alloc] initWithURL:filePathToUpload inMode:UIDocumentPickerModeExportToService];
                NSLog(@"filepath---------%@",filePathToUpload);
                docPicker.delegate = self;
                [self presentViewController:docPicker animated:YES completion:nil];
            });
        }
        }
        else{
            [[Utitlity sharedInstance] showAlertViewWithMessage:@"FilePath Not Found" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
             {
                 
             }];
        }
    });
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
{
   [MBProgressHUD hideHUDForView:_webView animated:YES];
    
        // Called when user uploaded the file - Display success alert
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"nsurl===%@",url);
          NSString *alertMessage = [NSString stringWithFormat:@"Successfully uploaded file %@", [url lastPathComponent]];
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"UIDocumentView"
                                                  message:alertMessage
                                                  preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertController animated:YES completion:nil];
           filepath = nil;
        
        });
    
}


-(void)viewFile:(NSInteger)indexPath{
    
    if([Utitlity isConnectedTointernet]){
        _popOverView.hidden = NO;
        
        [MBProgressHUD showHUDAddedTo:_webView animated:YES];
        NSString *pdfUrl ;
        
        pdfUrl = [[docArray objectAtIndex:indexPath] objectForKey:@"FilePath"];;
        if(!([pdfUrl isEqual:[NSNull null]] || pdfUrl == nil)){
        
        pdfUrl = [pdfUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:pdfUrl]];
        [_webView loadRequest:request];
        }
        else{
            [[Utitlity sharedInstance] showAlertViewWithMessage:@"FilePath Not Found" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
             {
                 
             }];
        }
    }else{
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}

-(void)loadDataFromApi :(NSString *)url{
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[WebServices sharedInstance] getWithParameter:nil withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
            NSLog(@"response izz %@",responseObject);
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self->array addObjectsFromArray:responseObject];
                if(self->array.count > 0 )
                {
                    [self->docArray addObjectsFromArray:[[responseObject objectAtIndex:0]objectForKey:@"lstEOIDocument"]];
                    [self.tableView reloadData];
                }
                
            });
            
        }];
        
        
    }else{
        
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}
- (IBAction)closePopupButtonClick:(id)sender {
    self.popOverView.hidden = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
    [MBProgressHUD hideHUDForView:webView animated:YES];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)backButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
