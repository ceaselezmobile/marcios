//
//  MAilPopUpViewController.h
//  test
//
//  Created by ceazeles on 27/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPDropMenu.h"
#import "Utitlity.h"
#import "GlobalURL.h"
#import "WebServices.h"
#import "MBProgressHUD.h"
#import "UITextView+Placeholder.h"
#import <AddressBookUI/AddressBookUI.h>
#import <AddressBook/AddressBook.h>
#import "JSTokenField.h"
#import "ASJTagsView.h"

@interface MAilPopUpViewController : UIViewController<JSTokenFieldDelegate>
{
    NSMutableArray *array;
    NSMutableArray *fromIdArray;
    NSMutableArray *fromNameArray;
    
}
@property (weak, nonatomic) IBOutlet KPDropMenu *dropdownMenu;
@property(strong,nonatomic)NSString *theId;
@property(strong,nonatomic)NSArray *docArray;
@property (nonatomic, strong) NSMutableArray *toRecipients;
@property (nonatomic, strong) NSString *bankName;
@property (nonatomic, strong) NSString *emailId;
@property (weak, nonatomic) IBOutlet UILabel *firstAttachment;
@property (weak, nonatomic) IBOutlet UILabel *secondAttachment;
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;
@property (nonatomic, strong) JSTokenField *toField;
@property (weak, nonatomic) IBOutlet ASJTagsView *tagsView;
@property (weak, nonatomic) IBOutlet UITextField *toText;
@property (weak, nonatomic) IBOutlet UITextField *ccText;
@property (weak, nonatomic) IBOutlet UITextView *subjectText;
@property (weak, nonatomic) IBOutlet UITextView *bodyText;

- (void)setup;
- (void)handleTagBlocks;
- (void)showAlertMessage:(NSString *)message;

@end
