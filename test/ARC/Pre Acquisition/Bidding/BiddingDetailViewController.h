//
//  BiddingDetailViewController.h
//  test
//
//  Created by ceazeles on 14/09/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utitlity.h"
#import "GlobalURL.h"
#import "WebServices.h"
#import "MBProgressHUD.h"
#import "CustomTableViewCell.h"


@interface BiddingDetailViewController : UIViewController
{
    NSMutableArray *array;
    NSMutableArray *docArray;
    NSURL *filepath;
    NSData *fileData;
}

@property(strong,nonatomic)NSString *theId;
@property(strong,nonatomic)NSMutableDictionary *theDetailArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *popOverView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
