//
//  BiddingTableViewCell.h
//  test
//
//  Created by ceazeles on 07/09/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BiddingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *bankNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *referenceNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *reservePriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *bidResultLabel;

@end
