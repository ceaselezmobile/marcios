//
//  SPNDetailViewController.m
//  test
//
//  Created by ceazeles on 23/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "SPNDetailViewController.h"

@interface SPNDetailViewController ()<UIDocumentPickerDelegate>

@end

@implementation SPNDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBarHidden = YES;
    self.datesView.hidden = NO;
    self.ratioView.hidden = YES;
    array = [NSMutableArray array];
    docArray = [NSMutableArray array];
    mainArray = [NSMutableArray array];
    managementArray = [NSMutableArray array];
    incentiveArray = [NSMutableArray array];
    
    [self loadDataFromApi:[NSString stringWithFormat:@"/GetSPNDetailBySpnId?userid=%@&spnid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"],self.theId]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if(tableView == self.tableView){
        return 12 ;
    }
    else{
        return 7;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == self.tableView){
        if(section == 13)
        {
            return docArray.count;
        }
        return 1;
    }
    else
    {
        if(section == 3)
        {
            return managementArray.count;
        }
        else if(section == 5)
        {
            return incentiveArray.count;
        }
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomTableViewCell *cell;
    if(tableView == self.tableView){
        if(indexPath.section == 0)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"bankNameCell"];
            if(array.count>0){
                cell.bankNameText.text = [array[0]valueForKey:@"BankName"];
                cell.bankNameText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            }
            
        }
        else if(indexPath.section == 1)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"refNoCell"];
            if(array.count>0){
                cell.refNoText.text = [array[0]valueForKey:@"ProjectCode"];
                cell.refNoText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            }
        }
        else if(indexPath.section == 2)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"notificationCell"];
            if(array.count>0){
                cell.notificationDateText.text = [array[0]valueForKey:@"NotificationDate"];
                cell.notificationDateText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            }
        }
        else if(indexPath.section == 3)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"submissionCell"];
            if(array.count>0){
                cell.eoiDateText.text = [array[0]valueForKey:@"EOISubmissionDate"];
                cell.eoiDateText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            }
        }
        else if(indexPath.section == 4)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"roomDateCell"];
            if(array.count>0){
                cell.roomDateText.text = [[[array[0]valueForKey:@"DataRoomOpenDate"]stringByAppendingString:@" TO "]stringByAppendingString:[array[0]valueForKey:@"DataRoomCloseDate"]];
                cell.roomDateText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            }
        }
        else if(indexPath.section == 5)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"roomTimingCell"];
            if(array.count>0){
                cell.roomTimingText.text = [array[0]valueForKey:@"DataRoomTimings"];
                cell.roomTimingText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            }
        }
        else if(indexPath.section == 6)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"cutOffCell"];
            if(array.count>0){
                cell.cutOffText.text = [array[0]valueForKey:@"CutoffDate"];
                cell.cutOffText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            }
        }
        else if(indexPath.section == 7)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"bidSubmissionCell"];
            if(array.count>0){
                cell.bidSubmissionDateText.text = [array[0]valueForKey:@"BidSubmissionDate"];
                cell.bidSubmissionDateText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            }
        }
        else if(indexPath.section == 8)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"bidTypeCell"];
            if(array.count>0){
                cell.bidTypeText.text = [array[0]valueForKey:@"BidType"];
                cell.bidTypeText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            }
        }
        else if(indexPath.section == 9)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"bidDateCell"];
            if(array.count>0){
                cell.bidDateText.text = [array[0]valueForKey:@"BidDate"];
                cell.bidDateText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            }
        }
        else if(indexPath.section == 10)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"bidTimeCell"];
            if(array.count>0){
                if(![[array[0]valueForKey:@"BidTime"] isEqual:[NSNull null]]){
                    cell.bidTimeText.text = [array[0]valueForKey:@"BidTime"];
                }
                cell.bidTimeText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            }
        }
        else if(indexPath.section == 11)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"totalAmountCell"];
            if(array.count>0){
                if(![[array[0]valueForKey:@"TotalAmount"] isEqual:[NSNull null]]){
                    cell.totalAmountText.text = [[array[0]valueForKey:@"TotalAmount"]stringValue];
                }
                cell.totalAmountText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            }
        }
        else if(indexPath.section == 12)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"docHeaderCell"];
        }
        else if(indexPath.section == 13)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"docCell"];
            cell.docName.text = [docArray[indexPath.row]valueForKey:@"FileName"];
        }
    }
    else{
        if(indexPath.section == 0)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"srRatioCell"];
            if(array.count>0){
                if(![[array[0]valueForKey:@"SRRatioARCBank"] isEqual:[NSNull null]]){
                    cell.srRatioText.text = [[array[0]valueForKey:@"SRRatioARCBank"]stringValue];
                    
                    int number = [[array[0] valueForKey:@"SRRatioARCBank"] intValue];
                    number = 100-number;
                    cell.srRemainingText.text = [NSString stringWithFormat:@"%d",number];
                }
                cell.srRatioText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                cell.srRemainingText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            }
        }
        else if(indexPath.section == 1)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"upsideRatioCell"];
            if(array.count>0){
                if(![[array[0]valueForKey:@"UpsideRatioARCBank"] isEqual:[NSNull null]]){
                    cell.upsideRatioText.text = [[array[0]valueForKey:@"UpsideRatioARCBank"]stringValue];
                    
                    int number = [[array[0] valueForKey:@"UpsideRatioARCBank"] intValue];
                    number = 100-number;
                    cell.upsideRemainingRatio.text = [NSString stringWithFormat:@"%d",number];
                }
                cell.upsideRatioText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                cell.upsideRemainingRatio.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            }
        }
        else if(indexPath.section == 2)
        {
            
            cell = [tableView dequeueReusableCellWithIdentifier:@"managementFeeCell"];
            if(array.count>0){
                if(![[array[0]valueForKey:@"EarlierManagementFee"] isEqual:[NSNull null]]){
                    cell.managementFeeData.text = [[array[0]valueForKey:@"EarlierManagementFee"]stringValue];
                    
                }
                cell.managementFeeHeader.text = @"Earlier Management Fee(%)";
                cell.managementFeeData.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                
            }
            
        }
        else if(indexPath.section == 3)
        {
            
            cell = [tableView dequeueReusableCellWithIdentifier:@"managementDataCell"];
            if(array.count>0){
                
                cell.managementYearData.text = [[managementArray[indexPath.row]valueForKey:@"Year"]stringValue];
                cell.managementPercData.text = [[managementArray[indexPath.row]valueForKey:@"Percentage"]stringValue];
                
                
            }
            
        }
        else if(indexPath.section == 4)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"incentiveFeeCell"];
            if(array.count>0){
                if(![[array[0]valueForKey:@"EarlierIncentiveFee"] isEqual:[NSNull null]]){
                    cell.incentiveFeeData.text = [[array[0]valueForKey:@"EarlierIncentiveFee"]stringValue];
                    
                }
                cell.incentiveFeeHeader.text = @"Earlier Incentive Fee(%)";
                cell.incentiveFeeData.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                
            }
            
        }
        else if(indexPath.section == 5)
        {
            
            cell = [tableView dequeueReusableCellWithIdentifier:@"incentiveDataCell"];
            if(array.count>0){
                
                cell.incentiveYearData.text = [[incentiveArray[indexPath.row]valueForKey:@"Year"]stringValue];
                cell.incentivePerData.text = [[incentiveArray[indexPath.row]valueForKey:@"Percentage"]stringValue];
            }
        }
        else if(indexPath.section == 6)
        {
            
            cell = [tableView dequeueReusableCellWithIdentifier:@"interestCell"];
            if(array.count>0){
                
                cell.interestHeader.text = @"Interest on Expenses (%)";
                cell.interestData.text = [[array[0]valueForKey:@"InterestPercentage"]stringValue];
                cell.interestData.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            }
        }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.tableView){
        return UITableViewAutomaticDimension;
    }
    else{
        if(indexPath.section == 3 || indexPath.section == 5)
        {
            return 44;
        }
        return 80;
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == self.ratioTable){
        if(section == 3 || section == 5)
        {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
            
            UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width/2, 40)];
            [label1 setFont:[UIFont boldSystemFontOfSize:16]];
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Year"];
            [attributeString addAttribute:NSUnderlineStyleAttributeName
                                    value:[NSNumber numberWithInt:1]
                                    range:(NSRange){0,[attributeString length]}];
            label1.attributedText = attributeString;
            label1.textAlignment = NSTextAlignmentCenter;
            
            
            UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(view.frame.size.width/2, 0, view.frame.size.width/2, 40)];
            [label2 setFont:[UIFont boldSystemFontOfSize:16]];
            NSMutableAttributedString *attributeString2 = [[NSMutableAttributedString alloc] initWithString:@"Percentage (%)"];
            [attributeString2 addAttribute:NSUnderlineStyleAttributeName
                                     value:[NSNumber numberWithInt:1]
                                     range:(NSRange){0,[attributeString2 length]}];
            label2.attributedText = attributeString2;
            label2.textAlignment = NSTextAlignmentCenter;
            
            
            [view addSubview:label1];
            [view addSubview:label2];
            [view setBackgroundColor:[UIColor whiteColor]];
            return view;
        }
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(tableView == self.ratioTable)
    {
        if(section == 3 || section == 5)
        {
            return 40;
        }
    }
    return 0;
}

-(void)loadDataFromApi :(NSString *)url{
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[WebServices sharedInstance] getWithParameter:nil withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
            NSLog(@"response izz %@",responseObject);
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                [self->array addObjectsFromArray:responseObject];
                if(self->array.count > 0 )
                {
                    [self->docArray addObjectsFromArray:[[responseObject objectAtIndex:0]objectForKey:@"lstSPNAttachment"]];
                    if(docArray.count > 0)
                    {
                        self.docNameLabel.text = [docArray[0]valueForKey:@"FileName"];
                        
                    }
                    else{
                        self.docNameLabel.text = @"No Document Found";
                        
                    }
                    self->mainArray = [[responseObject objectAtIndex:0]objectForKey:@"lstSPNPayableDetails"];
                    for (int i=0; i<mainArray.count; i++) {
                        if([[mainArray[i] valueForKey:@"PayableType"] isEqualToString:@"Management"])
                        {
                            [self->managementArray addObject:mainArray[i]];
                        }
                        else if([[mainArray[i] valueForKey:@"PayableType"] isEqualToString:@"Incentive"])
                        {
                            [self->incentiveArray addObject:mainArray[i]];
                        }
                    }
                    [self.tableView reloadData];
                    [self.ratioTable reloadData];
                }
                
            });
            
        }];
        
        
    }else{
        
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}

- (IBAction)datesButtonClick:(id)sender {
    self.datesView.hidden = NO;
    self.ratioView.hidden = YES;
    self.tableView.hidden = NO;
    self.ratioTable.hidden = YES;
}
- (IBAction)ratioButtonClick:(id)sender {
    self.datesView.hidden = YES;
    self.ratioView.hidden = NO;
    self.tableView.hidden = YES;
    self.ratioTable.hidden = NO;
}
- (IBAction)downloadDocClick:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Do you want to dowload or view the file?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *Download = [UIAlertAction actionWithTitle:@"Download" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                               {
                                   [self downloadFiles:nil];
                               }];
    UIAlertAction *viewFile = [UIAlertAction actionWithTitle:@"View" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                               {
                                   [self viewFile:nil];
                               }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:Download];
    [alert addAction:viewFile];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)downloadFiles :(NSIndexPath*)indexPath{
    [MBProgressHUD showHUDAddedTo:_webView animated:YES];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlToDownload ;
        NSString *type;
        if(docArray.count > 0)
        {
            
             urlToDownload = [[docArray objectAtIndex:0] objectForKey:@"FilePath"];;
            //urlToDownload = [[DOCUMENTURL stringByAppendingString:@"Documents/SPN/"] stringByAppendingString:[[docArray objectAtIndex:0] objectForKey:@"FileName"]];
            type=[[docArray objectAtIndex:0] objectForKey:@"FileName"];
            
            if(!([urlToDownload isEqual:[NSNull null]] || urlToDownload == nil)){
                NSLog(@"urlToDownload--------%@",urlToDownload);
                urlToDownload = [urlToDownload stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                //        urlToDownload = [urlToDownload stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                
                NSURL  *url = [NSURL URLWithString:urlToDownload];
                NSData *urlData = [NSData dataWithContentsOfURL:url];
                if ( urlData )
                {
                    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString  *documentsDirectory = [paths objectAtIndex:0];
                    NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,type];
                    
                    //saving is done on main thread
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:_webView animated:YES];
                        [urlData writeToFile:filePath atomically:YES];
                        NSLog(@"filepath test n---------%@",filePath);
                        
                        NSLog(@"File Saved !");
                        
                        //Create the file path of the document to upload
                        NSURL *filePathToUpload = [NSURL fileURLWithPath:filePath]  ;
                        UIDocumentPickerViewController *docPicker = [[UIDocumentPickerViewController alloc] initWithURL:filePathToUpload inMode:UIDocumentPickerModeExportToService];
                        NSLog(@"filepath---------%@",filePathToUpload);
                        docPicker.delegate = self;
                        [self presentViewController:docPicker animated:YES completion:nil];
                    });
                }
            }
            else{
                [[Utitlity sharedInstance] showAlertViewWithMessage:@"FilePath Not Found " withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
                 {
                     
                 }];
            }
        }
        else{
            [[Utitlity sharedInstance] showAlertViewWithMessage:@"No Document Found" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
             {
                 
             }];
        }
    });
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
{
    [MBProgressHUD hideHUDForView:_webView animated:YES];
    
    // Called when user uploaded the file - Display success alert
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"nsurl===%@",url);
        NSString *alertMessage = [NSString stringWithFormat:@"Successfully uploaded file %@", [url lastPathComponent]];
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"UIDocumentView"
                                              message:alertMessage
                                              preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alertController animated:YES completion:nil];
        
        
    });
    
}


-(void)viewFile:(NSIndexPath*)indexPath{
    
    if([Utitlity isConnectedTointernet]){
        _popOverView.hidden = NO;
        
        [MBProgressHUD showHUDAddedTo:_webView animated:YES];
        NSString *pdfUrl ;
        if(docArray.count > 0)
        {
            pdfUrl = [[docArray objectAtIndex:0] objectForKey:@"FilePath"];;
           // pdfUrl = [[DOCUMENTURL stringByAppendingString:@"Documents/SPN/"] stringByAppendingString:[[docArray objectAtIndex:0] objectForKey:@"FileName"]];
            if(!([pdfUrl isEqual:[NSNull null]] || pdfUrl == nil)){
                
                pdfUrl = [pdfUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:pdfUrl]];
                [_webView loadRequest:request];
            }
            else{
                [[Utitlity sharedInstance] showAlertViewWithMessage:@"FilePath Not Found " withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
                 {
                     _popOverView.hidden = YES;
                 }];
            }
        }else{
            [[Utitlity sharedInstance] showAlertViewWithMessage:@"No Document Found" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
             {
                 _popOverView.hidden = YES;
             }];
        }
    }
    else{
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
    [MBProgressHUD hideHUDForView:webView animated:YES];
}

- (IBAction)closeButtonClick:(id)sender {
    _popOverView.hidden = YES;
}

- (IBAction)backButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
