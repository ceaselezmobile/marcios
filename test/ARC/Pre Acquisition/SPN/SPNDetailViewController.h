//
//  SPNDetailViewController.h
//  test
//
//  Created by ceazeles on 23/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utitlity.h"
#import "GlobalURL.h"
#import "WebServices.h"
#import "MBProgressHUD.h"
#import "CustomTableViewCell.h"

@interface SPNDetailViewController : UIViewController
{
     NSMutableArray *array;
     NSMutableArray *docArray;
     NSMutableArray *mainArray;
     NSMutableArray *managementArray;
     NSMutableArray *incentiveArray;
    
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong,nonatomic)NSString *theId;
@property (weak, nonatomic) IBOutlet UIView *datesView;
@property (weak, nonatomic) IBOutlet UIView *ratioView;
@property (weak, nonatomic) IBOutlet UITableView *ratioTable;
@property (weak, nonatomic) IBOutlet UILabel *docNameLabel;
@property (weak, nonatomic) IBOutlet UIView *popOverView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIView *documentView;

@end
