//
//  SPNTableViewCell.h
//  test
//
//  Created by ceazeles on 23/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *bankName;
@property (weak, nonatomic) IBOutlet UILabel *refNo;
@property (weak, nonatomic) IBOutlet UILabel *totalAmount;

@end
