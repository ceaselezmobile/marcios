//
//  NoteToCeoDetailViewController.m
//  test
//
//  Created by ceazeles on 18/10/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "NoteToCeoDetailViewController.h"

@interface NoteToCeoDetailViewController ()

@end

@implementation NoteToCeoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dictionary = [NSMutableDictionary dictionary];
    docArray = [NSMutableArray array];
    
    [self loadDataFromApi:[NSString stringWithFormat:@"GetSanctionDetailById?sanctionid=%@&userid=%@",self.theId,[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 19 ;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomTableViewCell *cell;
    if(indexPath.section == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"bankNameCell"];
        //cell.accountName.text = dataDict[@"AccountName"];
        cell.bankNameText.text = dictionary[@"BankName"];
        cell.bankNameText .layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
    }
    if(indexPath.section == 1)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"accountNameCell"];
        //if(dataDict.count>0)
        //{
            cell.accountName.text = dictionary[@"AccountName"];
            cell.accountName .layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        //}
        
    }
    if(indexPath.section == 2)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"referenceNoCell"];
       // if(dataDict.count>0)
       // {
            cell.refNoText.text = dictionary[@"ProjectCode"];
            cell.refNoText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
       // }
        
    }
    if(indexPath.section == 3)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"accountTypeCell"];
         if(![[dictionary valueForKey:@"AccountTypeName"] isEqual:[NSNull null]])
         {
        
        cell.accountTypeText.text = dictionary[@"AccountTypeName"];
        cell.accountTypeText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
         }
        
    }
    if(indexPath.section == 4)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"tranchNameCell"];
        // if(dataDict.count>0)
        // {
        cell.trancheNameText.text = dictionary[@"AccountType"];
        cell.trancheNameText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
        
    }
   
    if(indexPath.section == 5)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"activityBorrowerCell"];
        // if(dataDict.count>0)
        // {
        cell.activityBorrowerNameText.text = dictionary[@"ActivityofBorrower"];
        cell.activityBorrowerNameText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 6)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"bankingArrangementCell"];
         if(![[dictionary valueForKey:@"BankingArrangeMent"] isEqual:[NSNull null]])
        {
         
        cell.bankingArrangementText.text = dictionary[@"BankingArrangeMent"];
        cell.bankingArrangementText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
         }
    }
    if(indexPath.section == 7)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"npaDateCell"];
        // if(dataDict.count>0)
        // {
        cell.npaDateText.text = dictionary[@"NPADate"];
        cell.npaDateText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 8)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"amountOutstandingCell"];
        // if(dataDict.count>0)
        // {
        cell.amountOutstandingText.text = [dictionary[@"OutstandingAmount"]stringValue];
        cell.amountOutstandingText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 9)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"reservePriceCell"];
        // if(dataDict.count>0)
        // {
        cell.reservePriceText.text = [dictionary[@"ReservePrice"]stringValue];
        cell.reservePriceText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 10)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"fmvValueCell"];
        // if(dataDict.count>0)
        // {
        cell.fmvText.text = [dictionary[@"FMV"]stringValue];
        cell.fmvText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 11)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"dsvValueCell"];
        // if(dataDict.count>0)
        // {
        cell.dsvText.text = [dictionary[@"DSV"]stringValue];
        cell.dsvText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 12)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"assetDetailsCell"];
        // if(dataDict.count>0)
        // {
        cell.assetDetailsTextView.text = dictionary[@"AssetDetails"];
        cell.assetDetailsTextView.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 13)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"ddObservationsCell"];
        // if(dataDict.count>0)
        // {
        cell.ddObservationsTextView.text = dictionary[@"DueDecription"];
        cell.ddObservationsTextView.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 14)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"legalActionCell"];
        // if(dataDict.count>0)
        // {
        cell.legalActionsTextView.text = dictionary[@"LegalAction"];
        cell.legalActionsTextView.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 15)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"legalddCell"];
        // if(dataDict.count>0)
        // {
        cell.legalDDText.text = dictionary[@"LegalDueDiligence"];
        cell.legalDDText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 16)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"riskManagementCell"];
        // if(dataDict.count>0)
        // {
        cell.riskManagementObservationsTextView.text = dictionary[@"RiskManagment"];
        cell.riskManagementObservationsTextView.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 17)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"recommendationsCell"];
        // if(dataDict.count>0)
        // {
        cell.recommendationsTextView.text = dictionary[@"Recommendation"];
        cell.recommendationsTextView.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    if(indexPath.section == 18)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"sarfaesiActionCell"];
        // if(dataDict.count>0)
        // {
        cell.sarfaesiActionTextView.text = dictionary[@"SARFAESIAction"];
        cell.sarfaesiActionTextView.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        // }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(void)loadDataFromApi :(NSString *)url{
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[WebServices sharedInstance] getWithParameter:nil withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
            NSLog(@"response izz %@",responseObject);
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                self->dictionary = responseObject;
                [self.tableView reloadData];
                
            });
            
        }];
        
        
    }else{
        
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}

-(void)sendtheStatus :(NSString *)url :(NSString *)isApprove :(NSString *)isReject :(NSString *)isForward :(NSString *)remarks
{
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"],@"UserID",self.theId,@"SanctionID",isApprove,@"IsApprove",isReject,@"IsReject",isForward,@"IsForward",remarks,@"Remarks",finalStatus,@"Type", nil];
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[WebServices sharedInstance] postwithParameter:dict withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
            NSLog(@"mail response izz %@",responseObject);
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
               
                if([[responseObject valueForKey:@"ForwardOrRejectOrApproveSanctionResult"] boolValue] == YES)
                {
                    [[Utitlity sharedInstance] showAlertViewWithMessage:@"Updated Successfully" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
                     {
                         
                     }];
                }
                
           });
            
        }];
        
        
    }else{
        
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    
}


- (IBAction)approveButtonClick:(id)sender {
    [[Utitlity sharedInstance]displayAlertViewWithMessage:@"Do you want to approve ?" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
     {
         [self popUpView:@"Approve"];
         
         isApprove = @"true";
         isReject = @"false";
         isForward = @"false";
     }];
}
- (IBAction)rejectButtonClick:(id)sender {
    [[Utitlity sharedInstance]displayAlertViewWithMessage:@"Do you want to reject ?" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
     {
         
         [self popUpView:@"Reject"];
         
         isApprove = @"false";
         isReject = @"true";
         isForward = @"false";
     }];
}

- (IBAction)forwardButtonClick:(id)sender {
    [[Utitlity sharedInstance]displayAlertViewWithMessage:@"Do you want to forward ?" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
     {
         [self popUpView:@"Forward"];
         
         isApprove = @"false";
         isReject = @"false";
         isForward = @"true";
     }];
}

- (IBAction)closeButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)popUpView :(NSString *)string
{
    HMPopUpView *hmPopUp = [[HMPopUpView alloc] initWithTitle:@"Add Comments" okButtonTitle:@"Okay" cancelButtonTitle:@"Cancel" delegate:self];
    
    hmPopUp.borderColor = [UIColor lightGrayColor];
    hmPopUp.titleSeparatorColor = [UIColor lightGrayColor];
    
    hmPopUp.okButtonBGColor = [UIColor colorWithRed:235.0/255.0 green:95.0/255.0 blue:70.0/255.0 alpha:1.0];;
    hmPopUp.okButtonTextColor = [UIColor whiteColor];
    
    hmPopUp.textFieldBGColor = [UIColor whiteColor];
    hmPopUp.borderWidth = 1;
    hmPopUp.transitionType = HMPopUpTransitionTypePopFromBottom;
    hmPopUp.dismissType = HMPopUpDismissTypeFadeOutTop;
    [hmPopUp showInView:self.view];
}

#pragma mark - HMPopUpViewDelegate
-(void)popUpView:(HMPopUpView *)view accepted:(BOOL)accept inputText:(NSString *)text{
    if (accept) {
        //do stuff
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        if([[defaults valueForKey:@"MemberType"] isEqualToString:@"FinalApprover"])
        {
            finalStatus = @"FinalApprove";
            
        }
        else if([[defaults valueForKey:@"MemberType"] isEqualToString:@"BoardMember"])
        {
            finalStatus = @"BoardMemberReject";
        }
        else
        {
            if([isApprove isEqualToString:@"true"])
            {
                finalStatus = @"CEOApprove";
            }
            else if([isReject isEqualToString:@"true"])
            {
                finalStatus = @"CEOReject";
            }
        }
      
        [self sendtheStatus:@"/POSTApproveOrRejectNoteToCEO" :isApprove :isReject :isForward :text];
       
    } else {
        NSLog(@"User pressed Cancel button");
        
    }
}

@end
