//
//  NoteToCeoTableViewCell.h
//  test
//
//  Created by ceazeles on 28/09/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NoteToCeoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *bankName;
@property (weak, nonatomic) IBOutlet UILabel *refNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *npaAccountLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *letterDateLabel;

@end

NS_ASSUME_NONNULL_END
