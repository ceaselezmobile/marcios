//
//  NoteToCeoDetailViewController.h
//  test
//
//  Created by ceazeles on 18/10/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utitlity.h"
#import "GlobalURL.h"
#import "WebServices.h"
#import "MBProgressHUD.h"
#import "CustomTableViewCell.h"
#import "HMPopUpView.h"

NS_ASSUME_NONNULL_BEGIN

@interface NoteToCeoDetailViewController : UIViewController <HMPopUpViewDelegate>
{
    NSMutableDictionary *dictionary;
    NSMutableArray *docArray;
    NSString *finalStatus;
    NSString *isApprove;
    NSString *isReject;
    NSString *isForward;
    
}

@property(strong,nonatomic)NSString *theId;
@property(strong,nonatomic)NSMutableDictionary *theDetailArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
