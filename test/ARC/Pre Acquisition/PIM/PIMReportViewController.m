//
//  PIMReportViewController.m
//  test
//
//  Created by ceazeles on 31/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "PIMReportViewController.h"

@interface PIMReportViewController ()

@end

@implementation PIMReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [MBProgressHUD showHUDAddedTo:_webView animated:YES];
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:[BASEURL stringByAppendingString:[NSString stringWithFormat:@"/GetHTMLForPimReport?userid=%@&spnid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"],self.theId]]]];
    [self.webView loadRequest:request];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
    [MBProgressHUD hideHUDForView:webView animated:YES];
}

- (IBAction)backButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)showToast :(NSString *)string
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.text = string;
    hud.label.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
    hud.margin = 10.f;
    [hud setOffset:CGPointMake(0, self.view.frame.size.height/2)];
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:3];
}

@end
