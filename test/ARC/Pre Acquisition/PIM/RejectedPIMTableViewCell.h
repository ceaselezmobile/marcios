//
//  RejectedPIMTableViewCell.h
//  test
//
//  Created by ceazeles on 16/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RejectedPIMTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *accountName;
@property (weak, nonatomic) IBOutlet UILabel *rejectionParameters;

@end
