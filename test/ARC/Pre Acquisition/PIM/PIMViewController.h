//
//  PIMViewController.h
//  test
//
//  Created by ceazeles on 13/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "PIMListTableViewCell.h"
#import "PIMDetailViewController.h"
#import "Utitlity.h"
#import "GlobalURL.h"
#import "WebServices.h"
#import "MBProgressHUD.h"

@protocol SearchDelegate <NSObject>
- (void) selectedString:(NSString*)string selectedID:(NSString *)theId selectFlag:(NSString *)theFlag;
@end

@interface PIMViewController : UIViewController
{
    NSMutableArray *searchArray;
    NSMutableArray *searchResultArray;
    UILabel *noDataLabel;
}

@property (assign)BOOL searchBarActive;
@property(strong,nonatomic)NSString *theURL;
@property(strong,nonatomic)NSString *searchFlag;
@property(strong,nonatomic)NSString *searchScreen;
@property (nonatomic, assign) id<SearchDelegate>delegate;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
