//
//  PIMReportViewController.h
//  test
//
//  Created by ceazeles on 31/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalURL.h"
#import "MBProgressHUD.h"

@interface PIMReportViewController : UIViewController

@property(strong,nonatomic)NSString *theId;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
