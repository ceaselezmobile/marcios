//
//  ForwardedPIMDetailViewController.h
//  test
//
//  Created by ceazeles on 30/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utitlity.h"
#import "GlobalURL.h"
#import "WebServices.h"
#import "MBProgressHUD.h"
#import "CustomTableViewCell.h"
#import "HMPopUpView.h"

@protocol PimStatusDelegate <NSObject>
- (void) selectedString:(NSString*)string selectedID:(NSString *)theId selectFlag:(NSString *)theFlag;
@end

@interface ForwardedPIMDetailViewController : UIViewController <HMPopUpViewDelegate>
{
    NSMutableDictionary *dataDict;
    NSString *isApproved;
    NSString *isRejected;
    NSString *textStatus;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong,nonatomic)NSString *theId;
@property(strong,nonatomic)NSString *account;
@property (weak, nonatomic) IBOutlet UIView *popOverView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *approveButton;
@property (weak, nonatomic) IBOutlet UIButton *rejectButton;
@property (weak, nonatomic) IBOutlet UIButton *reExamineButton;
@property (weak, nonatomic) IBOutlet UIView *acceptRejectView;
@property (weak, nonatomic) IBOutlet UIView *commentView;

@property (nonatomic, assign) id<PimStatusDelegate>delegate;

@end
