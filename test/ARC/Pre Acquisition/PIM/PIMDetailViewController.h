//
//  PIMDetailViewController.h
//  test
//
//  Created by ceazeles on 14/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "PIMViewController.h"
#import "RejectedParametersViewController.h"
#import "ForwardedPIMViewController.h"
#import "PIMReportViewController.h"

@interface PIMDetailViewController : UIViewController
{
    NSString *bankId;
    NSString *spnId;
}
@property (weak, nonatomic) IBOutlet UITextField *bankNameText;
@property (weak, nonatomic) IBOutlet UITextField *refNumberText;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;

@end
