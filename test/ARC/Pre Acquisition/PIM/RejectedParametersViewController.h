//
//  RejectedParametersViewController.h
//  test
//
//  Created by ceazeles on 14/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RejectedParametersTableViewCell.h"
#import "Utitlity.h"
#import "GlobalURL.h"
#import "WebServices.h"
#import "MBProgressHUD.h"
#import "RejectedPIMViewController.h"

@interface RejectedParametersViewController : UIViewController
{
    NSMutableArray *parameterArray;
}


@property(strong,nonatomic)NSString *bankId;
@property(strong,nonatomic)NSString *spnId;
@property(strong,nonatomic)NSString *bankName;
@property(strong,nonatomic)NSString *refNo;
@property (weak, nonatomic) IBOutlet UILabel *bankNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *referenceNoLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
