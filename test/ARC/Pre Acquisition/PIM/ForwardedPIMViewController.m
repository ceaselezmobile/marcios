//
//  ForwardedPIMViewController.m
//  test
//
//  Created by ceazeles on 14/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "ForwardedPIMViewController.h"

@interface ForwardedPIMViewController ()

@end

@implementation ForwardedPIMViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.bankNameLabel.text = self.bankName;
    self.refNumberLabel.text = self.refNo;
    
    noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height/2,self.view.frame.size.width ,50)];
    array = [NSMutableArray array];
    searchResultArray = [NSMutableArray array];
    
    [self loadDataFromApi:[NSString stringWithFormat:@"/GetForwardedList?userid=%@&isreffered=true&spnid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"],self.spnId]];
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
    self.searchBar.hidden = YES;
    self.searchButton.hidden = NO;
    self.titleLabel.hidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.searchBarActive)
    {
        return searchResultArray.count;
    }
    else
    {
        return array.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ForwardedPIMTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"forwardedCell"];
    NSArray *arr = [NSArray array];
    if(self.searchBarActive)
    {
        arr = [searchResultArray mutableCopy];
    }
    else
    {
        arr = [array mutableCopy];
    }
    if(arr.count > 0)
    {
        NSDictionary *dict = arr[indexPath.row];
        cell.accountNameLabel.text = dict[@"AccountName"];
        if([dict[@"IsApproved"] isEqualToString:@"Yes"])
        {
            cell.statusLabel.text = @"Approved";
            cell.statusLabel.backgroundColor = [UIColor colorWithRed:67.0/255.0 green:160.0/255.0 blue:71.0/255.0 alpha:1.0];
        }
        else if([dict[@"IsRejected"] isEqualToString:@"Yes"])
        {
            cell.statusLabel.text = @"Rejected";
            cell.statusLabel.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:95.0/255.0 blue:70.0/255.0 alpha:1.0];
        }
        else if([dict[@"IsReferred"] isEqualToString:@"Yes"])
        {
            cell.statusLabel.text = @"Referred";
            cell.statusLabel.backgroundColor = [UIColor colorWithRed:250.0/255.0 green:186.0/255.0 blue:78.0/255.0 alpha:1.0];
        }
        else
        {
            cell.statusLabel.text = @"N/A";
            cell.statusLabel.backgroundColor = [UIColor grayColor];
        }
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arr = [NSArray array];
    if(self.searchBarActive)
    {
        arr = [searchResultArray mutableCopy];
    }
    else
    {
        arr = [array mutableCopy];
    }
    
    NSDictionary *dict = arr[indexPath.row];
    ForwardedPIMDetailViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ForwardedPIMDetailViewController"];
    vc.theId = dict[@"PIMRefId"];
    vc.account = dict[@"AccountName"];
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)loadDataFromApi :(NSString *)url{
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[WebServices sharedInstance] getWithParameter:nil withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
            NSLog(@"forwarded response izz %@",responseObject);
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self removeNoDataFound:self.view];
                [self->array addObjectsFromArray:responseObject];
                if(self->array.count > 0 )
                {
                    [self.tableView reloadData];
                }
                else{
                    [self showNoDataFound:self.view];
                }
            });
            
        }];
        
        
    }else{
        
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [searchResultArray removeAllObjects];
    NSPredicate *resultPredicate;
    
    resultPredicate = [NSPredicate predicateWithFormat:@"AccountName contains[c] %@", searchText];
    
    searchResultArray  = [NSMutableArray arrayWithArray:[array filteredArrayUsingPredicate:resultPredicate]];
    if(searchResultArray.count > 0)
    {
        self.tableView.hidden = NO;
        [self.tableView reloadData];
        [self removeNoDataFound:self.view];
    }
    else{
        self.tableView.hidden = YES;
        [self showNoDataFound:self.view];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length>0)
    {
        self.searchBarActive = YES;
        [self filterContentForSearchText:searchText scope:[[self.searchBar scopeButtonTitles] objectAtIndex:[self.searchBar selectedScopeButtonIndex]]];
        
    }else{
        [self removeNoDataFound:self.view];
        self.searchBarActive = NO;
        [self.tableView reloadData];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self cancelSearching];
    [self.tableView reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    self.searchBarActive = YES;
    [self.view endEditing:YES];
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:NO animated:YES];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    self.searchBarActive = NO;
    [self.searchBar setShowsCancelButton:NO animated:YES];
}

-(void)cancelSearching
{
    self.searchBarActive = NO;
    [self.searchBar resignFirstResponder];
    self.searchBar.text  = @"";
}

- (IBAction)searchButtonClick:(id)sender {
    self.searchBar.hidden = NO;
    [self.searchBar becomeFirstResponder];
    self.searchButton.hidden = YES;
    self.titleLabel.hidden = YES;
}

-(void)showNoDataFound :(UIView *)view
{
    self.tableView.hidden = YES;
    if(![noDataLabel isDescendantOfView:view])
    {
        [view addSubview:noDataLabel];
    }
    [noDataLabel setFont:[UIFont systemFontOfSize:20]];
    noDataLabel.textAlignment=NSTextAlignmentCenter;
    noDataLabel.layer.masksToBounds  = YES;
    noDataLabel.layer.shadowOpacity  = 2.5;
    noDataLabel.layer.shadowColor    = [[UIColor grayColor] CGColor];
    noDataLabel.layer.shadowOffset   = CGSizeMake(0, 1);
    noDataLabel.layer.shadowRadius   = 2;
    noDataLabel.text=@"No Data Found";
}

-(void)removeNoDataFound :(UIView *)view
{
    self.tableView.hidden = NO;
    [noDataLabel removeFromSuperview];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)backButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
