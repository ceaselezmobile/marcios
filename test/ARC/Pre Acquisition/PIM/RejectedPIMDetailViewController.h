//
//  RejectedPIMDetailViewController.h
//  test
//
//  Created by ceazeles on 30/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utitlity.h"
#import "GlobalURL.h"
#import "WebServices.h"
#import "MBProgressHUD.h"
#import "CustomTableViewCell.h"

@interface RejectedPIMDetailViewController : UIViewController
{
    NSMutableDictionary *dataDict;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong,nonatomic)NSString *theId;
@property(strong,nonatomic)NSString *account;
@property (weak, nonatomic) IBOutlet UIView *popOverView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
