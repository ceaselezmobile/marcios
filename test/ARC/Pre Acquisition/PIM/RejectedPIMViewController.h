//
//  RejectedPIMViewController.h
//  test
//
//  Created by ceazeles on 14/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RejectedPIMTableViewCell.h"
#import "Utitlity.h"
#import "GlobalURL.h"
#import "WebServices.h"
#import "MBProgressHUD.h"
#import "RejectedPIMDetailViewController.h"

@interface RejectedPIMViewController : UIViewController
{
    UILabel *noDataLabel;
    NSMutableArray *array;
    NSMutableArray *searchResultArray;
}

@property (assign)BOOL searchBarActive;
@property(strong,nonatomic)NSString *bankId;
@property(strong,nonatomic)NSString *refId;
@property(strong,nonatomic)NSString *bankName;
@property(strong,nonatomic)NSString *refNo;
@property(strong,nonatomic)NSString *rejId;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
