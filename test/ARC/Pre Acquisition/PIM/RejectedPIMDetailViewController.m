//
//  RejectedPIMDetailViewController.m
//  test
//
//  Created by ceazeles on 30/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "RejectedPIMDetailViewController.h"

@interface RejectedPIMDetailViewController ()<UIDocumentPickerDelegate>

@end

@implementation RejectedPIMDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dataDict = [NSMutableDictionary dictionary];
    
    [self loadDataFromApi:[NSString stringWithFormat:@"/GetTranchedAccountdetails?userid=%@&pimreferenceid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"],self.theId]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 20 ;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomTableViewCell *cell;
    if(indexPath.section == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"accountNameCell"];
        //  cell.accountName.text = dataDict[@"AccountName"];
        cell.accountName.text = self.account;
        cell.accountName .layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
    }
    if(indexPath.section == 1)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"arrangementCell"];
        if(dataDict.count>0)
        {
            cell.bankArrangement.text = dataDict[@"BankArrangement"];
            cell.bankArrangement .layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
        
    }
    if(indexPath.section == 222)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"locationCell"];
        if(dataDict.count>0)
        {
            cell.unitLocation.text = dataDict[@"BankArrangement"];
            cell.unitLocation.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
        
    }
    if(indexPath.section == 2)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"ourShareCell"];
        if(dataDict.count>0)
        {
            cell.ourShare.text = [dataDict[@"OurBankShare"]stringValue];
            cell.ourShare.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
        
    }
    if(indexPath.section == 3)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"otherShareCell"];
        if(dataDict.count>0)
        {
            cell.otherShare.text = [dataDict[@"OtherBankShare"]stringValue];
            cell.otherShare.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    if(indexPath.section == 4)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"landBankCell"];
        if(dataDict.count>0)
        {
            cell.landBankShare.text = [dataDict[@"LandBankShare"]stringValue];
            cell.landBankShare.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    
    if(indexPath.section == 5)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"lnBCell"];
        if(dataDict.count>0)
        {
            cell.lnbShare.text = [dataDict[@"LNBBankshare"]stringValue];
            cell.lnbShare.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    if(indexPath.section == 6)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"pnmCell"];
        if(dataDict.count>0)
        {
            cell.pnmShare.text = [dataDict[@"PAndMBankShare"]stringValue];
            cell.pnmShare.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    if(indexPath.section == 7)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"realBalanceCell"];
        if(dataDict.count>0)
        {
            cell.realBalance.text = [dataDict[@"PAndMBankShare"]stringValue];
            cell.realBalance.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    if(indexPath.section == 8)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"unchargedInterestCell"];
        if(dataDict.count>0)
        {
            cell.unchargedInterest.text = [dataDict[@"PAndMBankShare"]stringValue];
            cell.unchargedInterest.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    if(indexPath.section == 9)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"totalBalanceCell"];
        if(dataDict.count>0)
        {
            cell.totalBalance.text = [dataDict[@"TotalBalance"]stringValue];
            cell.totalBalance.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    if(indexPath.section == 10)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"fmvCell"];
        if(dataDict.count>0)
        {
            cell.fmvText.text = [dataDict[@"FMV"]stringValue];
            cell.fmvText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    if(indexPath.section == 11)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"dsvCell"];
        if(dataDict.count>0)
        {
            cell.dsvText.text = [dataDict[@"DSV"]stringValue];
            cell.dsvText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }if(indexPath.section == 12)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"reservePriceCell"];
        if(dataDict.count>0)
        {
            cell.reservePriceText.text = [dataDict[@"ReservePrice"]stringValue];
            cell.reservePriceText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    if(indexPath.section == 13)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"rejectionParamCell"];
        if(dataDict.count>0)
        {
            cell.rejectionParameterTextView.text = dataDict[@"RejectionParameters"];
            cell.rejectionParameterTextView.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    if(indexPath.section == 14)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"statusCell"];
        if(dataDict.count>0)
        {
            cell.statusText.text = dataDict[@"Status"];
            cell.statusText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    if(indexPath.section == 15)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"remarksCell"];
        if(dataDict.count>0)
        {
            cell.remarksTextView.text = dataDict[@"Remarks"];
            cell.remarksTextView.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    if(indexPath.section == 16)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"realisableValueCell"];
        if(dataDict.count>0)
        {
            cell.realisableValueText.text = [dataDict[@"RealisableValue"]stringValue];
            cell.realisableValueText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    if(indexPath.section == 17)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"reservePriceCashCell"];
        if(dataDict.count>0)
        {
            cell.reservePriceCashText.text = [dataDict[@"ReservePrice"]stringValue];
            cell.reservePriceCashText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    if(indexPath.section == 18)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"docHeaderCell"];
        if(dataDict.count>0)
        {
            cell.pimDocText.text = dataDict[@"FileName"];
            cell.pimDocText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    if(indexPath.section == 19)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"docCell"];
        if(dataDict.count>0)
        {
            cell.docName.text = dataDict[@"FileName"];
            
            cell.downloadBtn.tag = indexPath.row;
            [cell.downloadBtn addTarget:self action:@selector(download:) forControlEvents:UIControlEventTouchUpInside];
            cell.docName.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        }
    }
    return cell;
}

-(void)download:(UIButton*)sender
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Do you want to dowload or view the file?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *Download = [UIAlertAction actionWithTitle:@"Download" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                               {
                                   [self downloadFiles:sender.tag];
                               }];
    UIAlertAction *viewFile = [UIAlertAction actionWithTitle:@"View" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                               {
                                   [self viewFile:sender.tag];
                               }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:Download];
    [alert addAction:viewFile];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)downloadFiles :(NSInteger)indexPath{
    [MBProgressHUD showHUDAddedTo:_webView animated:YES];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlToDownload ;
        NSString *type;
        
          urlToDownload = dataDict[@"VirtualPath"];
       // urlToDownload = [[DOCUMENTURL stringByAppendingString:@"documents/pim/"] stringByAppendingString:dataDict[@"FileName"]];
        type = dataDict[@"FileName"];
        if(!([urlToDownload isEqual:[NSNull null]] || urlToDownload == nil)){
            
            NSLog(@"urlToDownload--------%@",urlToDownload);
            urlToDownload = [urlToDownload stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            //        urlToDownload = [urlToDownload stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            NSURL  *url = [NSURL URLWithString:urlToDownload];
            NSData *urlData = [NSData dataWithContentsOfURL:url];
            if ( urlData )
            {
                NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString  *documentsDirectory = [paths objectAtIndex:0];
                NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,type];
                
                //saving is done on main thread
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:_webView animated:YES];
                    [urlData writeToFile:filePath atomically:YES];
                    NSLog(@"filepath test n---------%@",filePath);
                    
                    NSLog(@"File Saved !");
                    
                    //Create the file path of the document to upload
                    NSURL *filePathToUpload = [NSURL fileURLWithPath:filePath]  ;
                    UIDocumentPickerViewController *docPicker = [[UIDocumentPickerViewController alloc] initWithURL:filePathToUpload inMode:UIDocumentPickerModeExportToService];
                    NSLog(@"filepath---------%@",filePathToUpload);
                    docPicker.delegate = self;
                    [self presentViewController:docPicker animated:YES completion:nil];
                });
            }
        }
        else{
            [[Utitlity sharedInstance] showAlertViewWithMessage:@"FilePath Not Found" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
             {
                 
             }];
        }
    });
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
{
    [MBProgressHUD hideHUDForView:_webView animated:YES];
    
    // Called when user uploaded the file - Display success alert
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"nsurl===%@",url);
        NSString *alertMessage = [NSString stringWithFormat:@"Successfully uploaded file %@", [url lastPathComponent]];
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"UIDocumentView"
                                              message:alertMessage
                                              preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alertController animated:YES completion:nil];
        
        
    });
    
}


-(void)viewFile:(NSInteger)indexPath{
    
    if([Utitlity isConnectedTointernet]){
        _popOverView.hidden = NO;
        
        [MBProgressHUD showHUDAddedTo:_webView animated:YES];
        NSString *pdfUrl ;
        
        pdfUrl = dataDict[@"VirtualPath"];
       // pdfUrl = [[DOCUMENTURL stringByAppendingString:@"documents/pim/"] stringByAppendingString:dataDict[@"FileName"]];
        if(!([pdfUrl isEqual:[NSNull null]] || pdfUrl == nil)){
            
            pdfUrl = [pdfUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:pdfUrl]];
            [_webView loadRequest:request];
        }
        else{
            [[Utitlity sharedInstance] showAlertViewWithMessage:@"FilePath Not Found" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
             {
                 
             }];
        }
    }else{
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [MBProgressHUD hideHUDForView:_webView animated:YES];
    NSLog(@"finish");
}


-(void)loadDataFromApi :(NSString *)url{
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[WebServices sharedInstance] getWithParameter:nil withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
            NSLog(@"response izz %@",responseObject);
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                self->dataDict = responseObject[@"GetPIMRefDocumentResult"];
                if(self->dataDict.count > 0)
                {
                    [self.tableView reloadData];
                }
                
                
                
            });
            
        }];
        
        
    }else{
        
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}

- (IBAction)backButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)closeButtonClick:(id)sender {
     _popOverView.hidden = YES;
}

@end
