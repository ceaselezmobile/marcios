//
//  PIMDetailViewController.m
//  test
//
//  Created by ceazeles on 14/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "PIMDetailViewController.h"

@interface PIMDetailViewController ()<SearchDelegate>

@end

@implementation PIMDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

-(void) viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    PIMViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PIMViewController"];
    vc.delegate = self;
    if(textField == self.bankNameText)
    {
        vc.searchFlag = @"BankName";
        
    }
    else if(textField == self.refNumberText)
    {
        if([self.bankNameText.text isEqualToString:@""])
        {
            [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please select the bank" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
             {
                 
             }];
        }
        else{
        vc.searchFlag = @"RefNo";
        vc.theURL = [NSString stringWithFormat:@"/GetAllSPNReferenceNoByBankId?userid=%@&bankid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"],bankId];
        }
    }
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) selectedString:(NSString*)string selectedID:(NSString *)theId selectFlag:(NSString *)theFlag
{
    if([theFlag isEqualToString:@"BankName"])
    {
        self.bankNameText.text = string;
        self.refNumberText.text = @"";
        bankId = theId;
    }
    else{
        self.refNumberText.text = string;
        spnId = theId;
    }
}
- (IBAction)rejectedButtonClick:(id)sender {
    if([self.bankNameText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please select the bank" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else if([self.refNumberText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please select the reference number for the bank" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else{
    RejectedParametersViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"RejectedParametersViewController"];
    vc.bankId = bankId;
    vc.spnId = spnId;
    vc.bankName = self.bankNameText.text;
    vc.refNo = self.refNumberText.text;
    [self.navigationController pushViewController:vc animated:YES];
    }
}

- (IBAction)forwardedButtonClick:(id)sender {
    if([self.bankNameText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please select the bank" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else if([self.refNumberText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please select the reference number for the bank" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else{
    ForwardedPIMViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ForwardedPIMViewController"];
    vc.bankId = bankId;
    vc.spnId = spnId;
    vc.bankName = self.bankNameText.text;
    vc.refNo = self.refNumberText.text;
    [self.navigationController pushViewController:vc animated:YES];
    }
}

- (IBAction)PIMreportButtonClick:(id)sender {
    if([self.bankNameText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please select the bank" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else if([self.refNumberText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please select the reference number for the bank" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else{
        PIMReportViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PIMReportViewController"];
        vc.theId = spnId;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
