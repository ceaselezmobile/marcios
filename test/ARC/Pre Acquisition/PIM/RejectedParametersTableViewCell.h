//
//  RejectedParametersTableViewCell.h
//  test
//
//  Created by ceazeles on 14/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RejectedParametersTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *parameterButton;

@end
