//
//  RejectedParametersViewController.m
//  test
//
//  Created by ceazeles on 14/08/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "RejectedParametersViewController.h"

@interface RejectedParametersViewController ()

@end

@implementation RejectedParametersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.bankNameLabel.text = self.bankName;
    self.referenceNoLabel.text = self.refNo;
    parameterArray = [NSMutableArray array];
    
    [self loadDataFromApi:[NSString stringWithFormat:@"/GetRejectionParameterList?userid=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return parameterArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    RejectedParametersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"parameterCell"];
    
    if(parameterArray.count > 0)
    {
        NSDictionary *dict = parameterArray[indexPath.row];
        [cell.parameterButton setTitle:dict[@"Description"] forState:UIControlStateNormal];
        cell.parameterButton.tag = indexPath.row;
        [cell.parameterButton addTarget:self action:@selector(goToDetail:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(void)goToDetail:(UIButton*)sender
{
    NSDictionary *dict = parameterArray[sender.tag];
    RejectedPIMViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"RejectedPIMViewController"];
    vc.refId = self.spnId;
    vc.rejId = dict[@"RejectionId"];
    vc.bankId = self.bankId;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)showToast
{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.label.text = @"In Progress";
        hud.label.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
        hud.margin = 10.f;
        [hud setOffset:CGPointMake(0, self.view.frame.size.height)];
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:0.5];

}

-(void)loadDataFromApi :(NSString *)url{
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[WebServices sharedInstance] getWithParameter:nil withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
            NSLog(@"response izz %@",responseObject);
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                [self->parameterArray addObjectsFromArray:responseObject];
                if(self->parameterArray.count > 0)
                {
                    [self.tableView reloadData];
                }
                
                
            });
            
        }];
        
        
    }else{
        
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}

- (IBAction)backButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
