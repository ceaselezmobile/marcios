//
//  LegalDueDiligenceTableViewCell.h
//  test
//
//  Created by ceazeles on 27/09/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LegalDueDiligenceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *bankName;
@property (weak, nonatomic) IBOutlet UILabel *refNumber;
@property (weak, nonatomic) IBOutlet UILabel *borrowerName;

@end

NS_ASSUME_NONNULL_END
