//
//  LegalDueDiligenceDetailViewController.m
//  test
//
//  Created by ceazeles on 08/10/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "LegalDueDiligenceDetailViewController.h"

@interface LegalDueDiligenceDetailViewController ()

@end

@implementation LegalDueDiligenceDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
