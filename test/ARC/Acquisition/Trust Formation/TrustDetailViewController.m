//
//  TrustDetailViewController.m
//  test
//
//  Created by ceazeles on 19/09/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "TrustDetailViewController.h"

@interface TrustDetailViewController ()<MFMailComposeViewControllerDelegate>

@end

@implementation TrustDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.trustView.hidden = NO;
    self.documentView.hidden = YES;
    dict = [NSMutableDictionary dictionary];
    docArray = [NSMutableArray array];
    accountArray = [NSMutableArray array];
    
    [self loadDataFromApi:[NSString stringWithFormat:@"/GetTrustDetailsBytrustid?userid=%@&TrustID=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"],self.theId]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if(tableView == self.tableView){
        
        return 15 ;
    }
    else{
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView == self.tableView){
        
            if([dict[@"PurchasedType"] isEqualToString:@"Cash"])
            {
              if(section == 6)
              {
                  return 0;
              }
              if(section == 13)
              {
                  return accountArray.count;
              }
              if(section == 14)
                {
                    return 0;
                }
              else{
                  return 1;
              }
            }
            else{
                if(section == 5)
                {
                    return 0;
                }
                if(section == 13)
                {
                    return 0;
                }
                if(section == 14)
                {
                    return accountArray.count;
                }
                else{
                    return 1;
                }
            }
        
    }
    else{
        return docArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.tableView){
        CustomTableViewCell *cell;
        if(indexPath.section == 0)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"bankNameCell"];
            // if(array.count>0){
            cell.bankNameText.text = dict[@"BankName"];
            cell.bankNameText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            // }
            
        }
        else if(indexPath.section == 1)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"refNoCell"];
            //  if(array.count>0){
            cell.refNoText.text = dict[@"ProjectCode"];
            cell.refNoText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            //  }
        }
        else if(indexPath.section == 2)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"trustNameCell"];
            //  if(array.count>0){
            cell.trustNameText.text = dict[@"TrustName"];
            cell.trustNameText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            //  }
        }
        else if(indexPath.section == 3)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"purchaseTypeCell"];
            //  if(array.count>0){
            cell.purchaseTypeText.text = dict[@"PurchasedType"];
            cell.purchaseTypeText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            //  }
        }
        else if(indexPath.section == 4)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"purchaseValueCell"];
            //  if(array.count>0){
            cell.purchaseValueText.text = [dict[@"PurchasedValue"]stringValue];
            cell.purchaseValueText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            //  }
        }
        else if(indexPath.section == 5)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"cashPaidCell"];
            //  if(array.count>0){
            cell.cashPaidText.text = [dict[@"CashPaid"]stringValue];
            cell.cashPaidText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            //  }
        }
        else if(indexPath.section == 6)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"purchasePercCell"];
            //  if(array.count>0){
            cell.arcPercText.text = [dict[@"ARCPercentage"]stringValue];
            cell.arcPercText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            
            cell.arcAmountText.text = [dict[@"ARCAmount"]stringValue];
            cell.arcAmountText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            
            cell.bankPercText.text = [dict[@"BankPercentage"]stringValue];
            cell.bankPercText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            
            cell.bankAmountText.text = [dict[@"BankAmount"]stringValue];
            cell.bankAmountText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            //  }
        }
        else if(indexPath.section == 7)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"accountNameCell"];
            //  if(array.count>0){
            cell.accountName.text = dict[@"PurchasedAccountName"];
            cell.accountName.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            //  }
        }
        else if(indexPath.section == 8)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"accountNumberCell"];
            //  if(array.count>0){
            cell.accountNumberText.text = dict[@"PurchasedAccountNo"];
            cell.accountNumberText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            //  }
        }
        else if(indexPath.section == 9)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"branchNameCell"];
            //  if(array.count>0){
            cell.branchNameText.text = dict[@"PurchasedBranchName"];
            cell.branchNameText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            //  }
        }
        else if(indexPath.section == 10)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"ifscCodeCell"];
            //  if(array.count>0){
            cell.ifscCodeText.text = dict[@"PurchasedIFSCCode"];
            cell.ifscCodeText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            //  }
        }
        else if(indexPath.section == 11)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"trustDateCell"];
            //  if(array.count>0){
            cell.trustCreatedDateText.text = dict[@"TrustCreatedDate"];
            cell.trustCreatedDateText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            //  }
        }
        
        else if(indexPath.section == 12)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"docHeaderCell"];
        }
        else if(indexPath.section == 13)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"npaAccountDataCell"];
            if(accountArray.count > 0)
            {
                NSDictionary *dict = accountArray[indexPath.row];
                cell.accountNameText.text = dict[@"AccountName"];
                cell.purchaseValueText.text = [dict[@"PurchaseValue"]stringValue];
            }
        }
        else if(indexPath.section == 14)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"npaAccountCell"];
            if(accountArray.count > 0)
            {
                NSDictionary *dict = accountArray[indexPath.row];
                cell.accountNameText.text = dict[@"AccountName"];
                cell.purchaseTypeText.text = [dict[@"PurchaseValue"]stringValue];
            }
        }
        return cell;
    }
    else
    {
       AdvocateDetailTableViewCell *cell = cell = [tableView dequeueReusableCellWithIdentifier:@"documentCell"];
        if(docArray.count > 0)
        {
            NSDictionary *dict = docArray[indexPath.row];
            cell.advocateNameLabel.text = dict[@"AdvocateName"];
            cell.contactNumberLabel.text = dict[@"ContactPhone"];
            cell.contactEmailLabel.text = dict[@"ContactEmailID"];
            cell.purposeLabel.text = dict[@"Purpose"];
            cell.descriptionLabel.text = dict[@"Discription"];
            cell.caseDetailLabel.text = dict[@"CaseDetails"];
            
            cell.callButton.tag = indexPath.row;
            [cell.callButton addTarget:self action:@selector(call:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.mailButton.tag = indexPath.row;
            [cell.mailButton addTarget:self action:@selector(mail:) forControlEvents:UIControlEventTouchUpInside];
        }
        
       return cell;
    }
}

-(void)call:(UIButton*)sender
{
    
    [self makeCall:[docArray[sender.tag]valueForKey:@"ContactPhone"]];
}

-(void)mail:(UIButton*)sender
{
    
    [self sendMail:[docArray[sender.tag]valueForKey:@"ContactEmailID"]];
}

-(void)makeCall :(NSString *)number
{
    number = [@"telprompt://" stringByAppendingString:number];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:number]];
}

-(void)sendMail :(NSString *)mailId
{
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    mail.mailComposeDelegate = self;
    [mail setSubject:@""];
    [mail setMessageBody:@"" isHTML:NO];
    [mail setToRecipients:@[[NSString stringWithFormat:@"%@",mailId]]];
    
    [self presentViewController:mail animated:YES completion:NULL];
}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == self.tableView){
        if(section == 13)
        {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
            
            UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width/2, 40)];
            [label1 setFont:[UIFont boldSystemFontOfSize:13]];
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Account Name"];
            [attributeString addAttribute:NSUnderlineStyleAttributeName
                                    value:[NSNumber numberWithInt:1]
                                    range:(NSRange){0,[attributeString length]}];
            label1.attributedText = attributeString;
            label1.textAlignment = NSTextAlignmentCenter;
            
            
            UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(view.frame.size.width/2, 0, view.frame.size.width/2, 40)];
            [label2 setFont:[UIFont boldSystemFontOfSize:13]];
            NSMutableAttributedString *attributeString2 = [[NSMutableAttributedString alloc] initWithString:@"Purchase Value (Cr)"];
            [attributeString2 addAttribute:NSUnderlineStyleAttributeName
                                     value:[NSNumber numberWithInt:1]
                                     range:(NSRange){0,[attributeString2 length]}];
            label2.attributedText = attributeString2;
            label2.textAlignment = NSTextAlignmentCenter;
            
            
            [view addSubview:label1];
            [view addSubview:label2];
            [view setBackgroundColor:[UIColor whiteColor]];
            return view;
        }
        else if(section == 14)
        {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
            
            UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width/3, 40)];
            [label1 setFont:[UIFont boldSystemFontOfSize:13]];
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Account Name"];
            [attributeString addAttribute:NSUnderlineStyleAttributeName
                                    value:[NSNumber numberWithInt:1]
                                    range:(NSRange){0,[attributeString length]}];
            label1.attributedText = attributeString;
            label1.textAlignment = NSTextAlignmentCenter;
            
            
            UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(view.frame.size.width/3, 0, view.frame.size.width/3, 40)];
            [label2 setFont:[UIFont boldSystemFontOfSize:13]];
            NSMutableAttributedString *attributeString2 = [[NSMutableAttributedString alloc] initWithString:@"Purchase Value (Cr)"];
            [attributeString2 addAttribute:NSUnderlineStyleAttributeName
                                     value:[NSNumber numberWithInt:1]
                                     range:(NSRange){0,[attributeString2 length]}];
            label2.attributedText = attributeString2;
            label2.textAlignment = NSTextAlignmentCenter;
            
            UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake((view.frame.size.width/3)*2, 0, view.frame.size.width/3, 40)];
            [label3 setFont:[UIFont boldSystemFontOfSize:13]];
            NSMutableAttributedString *attributeString3 = [[NSMutableAttributedString alloc] initWithString:@"Purchase Ratio"];
            [attributeString3 addAttribute:NSUnderlineStyleAttributeName
                                     value:[NSNumber numberWithInt:1]
                                     range:(NSRange){0,[attributeString3 length]}];
            label3.attributedText = attributeString3;
            label3.textAlignment = NSTextAlignmentCenter;
            
            
            [view addSubview:label1];
            [view addSubview:label2];
            [view addSubview:label3];
            [view setBackgroundColor:[UIColor whiteColor]];
            return view;
        }
        
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(tableView == self.tableView)
    {
        if([dict[@"PurchasedType"] isEqualToString:@"Cash"])
        {
          if(section == 13)
          {
            return 40;
          }
        }
        else
        {
            if(section == 14)
            {
                return 40;
            }
        }
    }
    return 0;
}


-(void)loadDataFromApi :(NSString *)url{
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[WebServices sharedInstance] getWithParameter:nil withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
            NSLog(@"response izz %@",responseObject);
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                self->dict = responseObject;
                self->docArray = [responseObject objectForKey:@"lstTrustAdvocateDetails"];
                self->accountArray = [responseObject objectForKey:@"lstTrustNPAAccount"];
                
                [self.tableView reloadData];
                [self.documentTable reloadData];
                
            });
            
        }];
        
        
    }else{
        
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}

- (IBAction)trustDetailButtonClick:(id)sender {
    self.trustView.hidden = NO;
    self.tableView.hidden = NO;
    self.documentTable.hidden = YES;
    self.documentView.hidden = YES;
}

- (IBAction)documentDetailButtonClick:(id)sender {
    self.trustView.hidden = YES;
    self.tableView.hidden = YES;
    self.documentTable.hidden = NO;
    self.documentView.hidden = NO;
    
}

- (IBAction)backButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
