//
//  TrustListTableViewCell.h
//  test
//
//  Created by ceazeles on 18/09/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrustListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *trustNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *refNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *trustBankLabel;
@property (weak, nonatomic) IBOutlet UILabel *purchasedBankLabel;
@property (weak, nonatomic) IBOutlet UILabel *purchasedAccountLabel;



@end
