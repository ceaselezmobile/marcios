//
//  TrustDetailViewController.h
//  test
//
//  Created by ceazeles on 19/09/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utitlity.h"
#import "GlobalURL.h"
#import "WebServices.h"
#import "MBProgressHUD.h"
#import "CustomTableViewCell.h"
#import "AdvocateDetailTableViewCell.h"
#import <MessageUI/MessageUI.h>

@interface TrustDetailViewController : UIViewController
{
    NSMutableDictionary *dict;
    NSMutableArray *docArray;
    NSMutableArray *accountArray;
}

@property(strong,nonatomic)NSString *theId;
@property(strong,nonatomic)NSMutableDictionary *theDetailArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITableView *documentTable;
@property (weak, nonatomic) IBOutlet UIView *trustView;
@property (weak, nonatomic) IBOutlet UIView *documentView;

@end
