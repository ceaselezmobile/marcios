//
//  DocsDetailsViewController.h
//  test
//
//  Created by ceazeles on 24/09/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utitlity.h"
#import "GlobalURL.h"
#import "WebServices.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "DocsDetailTableViewCell.h"
#import "DocsDetailsDetailViewController.h"

@interface DocsDetailsViewController : UIViewController
{
    UILabel *noDataLabel;
    NSMutableArray *array;
    NSMutableArray *searchResultArray;
}

@property (assign)BOOL searchBarActive;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
