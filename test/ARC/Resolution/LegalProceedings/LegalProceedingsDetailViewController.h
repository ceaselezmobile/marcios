//
//  LegalProceedingsDetailViewController.h
//  test
//
//  Created by ceazeles on 09/10/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface LegalProceedingsDetailViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
