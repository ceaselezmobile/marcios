//
//  LegalProceedingsDetailViewController.m
//  test
//
//  Created by ceazeles on 09/10/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "LegalProceedingsDetailViewController.h"

@interface LegalProceedingsDetailViewController ()

@end

@implementation LegalProceedingsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 3 ;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomTableViewCell *cell;
    if(indexPath.section == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"bankNameCell"];
        
        //  cell.bankNameText.text = [array[0]valueForKey:@"BankName"];
        //  cell.bankNameText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        
    }
    if(indexPath.section == 1)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"referenceNoCell"];
        
        //  cell.bankNameText.text = [array[0]valueForKey:@"BankName"];
        //  cell.bankNameText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        
    }
    if(indexPath.section == 2)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"accountNameCell"];
        
        //  cell.bankNameText.text = [array[0]valueForKey:@"BankName"];
        //  cell.bankNameText.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        
    }
    
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
