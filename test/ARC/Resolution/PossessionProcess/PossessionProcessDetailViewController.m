//
//  PossessionProcessDetailViewController.m
//  test
//
//  Created by ceazeles on 22/11/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "PossessionProcessDetailViewController.h"

@interface PossessionProcessDetailViewController ()

@end

@implementation PossessionProcessDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backButtonOfClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
