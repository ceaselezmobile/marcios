//
//  AssignStakeholdersTableViewCell.h
//  test
//
//  Created by ceazeles on 08/10/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AssignStakeholdersTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *bankNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *refNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *trustNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *raCategoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *caseLabel;

@end

NS_ASSUME_NONNULL_END
