//
//  AssignStakeholdersViewController.m
//  test
//
//  Created by ceazeles on 08/10/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "AssignStakeholdersViewController.h"

@interface AssignStakeholdersViewController ()

@end

@implementation AssignStakeholdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height/2,self.view.frame.size.width ,50)];
    array = [NSMutableArray array];
    searchResultArray = [NSMutableArray array];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

-(void) viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    self.searchBar.hidden = YES;
    self.searchButton.hidden = NO;
    self.titleLabel.hidden = NO;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.searchBarActive)
    {
        return searchResultArray.count;
    }
    else
    {
        return 10;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AssignStakeholdersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"assignStakeholderCell"];
    
    NSArray *arr = [NSArray array];
    if(self.searchBarActive)
    {
        arr = [searchResultArray mutableCopy];
    }
    else
    {
        arr = [array mutableCopy];
    }
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 155;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arr = [NSArray array];
    if(self.searchBarActive)
    {
        arr = [searchResultArray mutableCopy];
    }
    else
    {
        arr = [array mutableCopy];
    }
    
    // NSDictionary *dict = arr[indexPath.row];
    AssignStakeholderDetailViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AssignStakeholderDetailViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [searchResultArray removeAllObjects];
    NSPredicate *resultPredicate;
    
    resultPredicate = [NSPredicate predicateWithFormat:@"BankName contains[c] %@", searchText];
    
    searchResultArray  = [NSMutableArray arrayWithArray:[array filteredArrayUsingPredicate:resultPredicate]];
    if(searchResultArray.count > 0)
    {
        self.tableView.hidden = NO;
        [self.tableView reloadData];
        [self removeNoDataFound:self.view];
    }
    else{
        self.tableView.hidden = YES;
        [self showNoDataFound:self.view];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length>0)
    {
        self.searchBarActive = YES;
        [self filterContentForSearchText:searchText scope:[[self.searchBar scopeButtonTitles] objectAtIndex:[self.searchBar selectedScopeButtonIndex]]];
        
    }else{
        [self removeNoDataFound:self.view];
        self.searchBarActive = NO;
        [self.tableView reloadData];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self cancelSearching];
    [self.tableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    self.searchBarActive = YES;
    [self.view endEditing:YES];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    self.searchBarActive = NO;
    [self.searchBar setShowsCancelButton:NO animated:YES];
}

-(void)cancelSearching
{
    self.searchBarActive = NO;
    [self.searchBar resignFirstResponder];
    self.searchBar.text  = @"";
}

-(void)loadDataFromApi :(NSString *)url{
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[WebServices sharedInstance] getWithParameter:nil withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
            NSLog(@"response izz %@",responseObject);
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self removeNoDataFound:self.view];
                [self->array addObjectsFromArray:responseObject];
                if(self->array.count > 0 )
                {
                    [self.tableView reloadData];
                }
                else{
                    [self showNoDataFound:self.view];
                }
            });
            
        }];
        
        
    }else{
        
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}

-(void)showNoDataFound :(UIView *)view
{
    self.tableView.hidden = YES;
    if(![noDataLabel isDescendantOfView:view])
    {
        [view addSubview:noDataLabel];
    }
    [noDataLabel setFont:[UIFont systemFontOfSize:20]];
    noDataLabel.textAlignment=NSTextAlignmentCenter;
    noDataLabel.layer.masksToBounds  = YES;
    noDataLabel.layer.shadowOpacity  = 2.5;
    noDataLabel.layer.shadowColor    = [[UIColor grayColor] CGColor];
    noDataLabel.layer.shadowOffset   = CGSizeMake(0, 1);
    noDataLabel.layer.shadowRadius   = 2;
    noDataLabel.text=@"No Data Found";
}

-(void)removeNoDataFound :(UIView *)view
{
    self.tableView.hidden = NO;
    [noDataLabel removeFromSuperview];
}

- (IBAction)searchButtonClick:(id)sender {
    self.searchBar.hidden = NO;
    [self.searchBar becomeFirstResponder];
    self.searchButton.hidden = YES;
    self.titleLabel.hidden = YES;
}


@end
