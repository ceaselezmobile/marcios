//
//  AlertVC.m
//  test
//
//  Created by ceaselez on 26/06/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "AlertVC.h"

@interface AlertVC ()
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerBtn1;
@property (weak, nonatomic) IBOutlet UIButton *headerBtn2;
@property (weak, nonatomic) IBOutlet UIButton *headerBtn3;
@property (weak, nonatomic) IBOutlet UIView *headerBtn1View;
@property (weak, nonatomic) IBOutlet UIView *headerBtn2View;
@property (weak, nonatomic) IBOutlet UIView *headerBtn3View;
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;

@end

@implementation AlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

@end
